/************************************************************************************************************************
@Name: APTPS_TriggerUtil
@Author: Avinash Bamane
@CreateDate: 24/08/2020
@Description: 
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/

public with sharing class APTPS_TriggerUtil {

    Map<Id, List<Apttus_Proposal__Proposal_Line_Item__c>> quoteProposalLineMap = new Map<Id, List<Apttus_Proposal__Proposal_Line_Item__c>>();

    public Map<Id, Apttus_Proposal__Proposal_Line_Item__c> quoteNewBundleMap = new Map<Id, Apttus_Proposal__Proposal_Line_Item__c>();
    public Map<Id, List<Apttus_Proposal__Proposal_Line_Item__c>> quoteNewAcMap = new Map<Id, List<Apttus_Proposal__Proposal_Line_Item__c>>();
    public Map<Id, Apttus_Proposal__Proposal_Line_Item__c> quoteNewTermMap = new Map<Id, Apttus_Proposal__Proposal_Line_Item__c>();
    public Map<Id, Apttus_Proposal__Proposal_Line_Item__c> quoteNewEnhancementMap = new Map<Id, Apttus_Proposal__Proposal_Line_Item__c>();
    public Map<Id, Apttus_Proposal__Proposal_Line_Item__c> quoteSalesMarginMap = new Map<Id, Apttus_Proposal__Proposal_Line_Item__c>();
    public Map<Id, Apttus_Proposal__Proposal_Line_Item__c> quoteNewProductTypeMap = new Map<Id, Apttus_Proposal__Proposal_Line_Item__c>();

    public Map<Id, Apttus_Proposal__Proposal_Line_Item__c> quoteAssetBundleMap = new Map<Id, Apttus_Proposal__Proposal_Line_Item__c>();
    public Map<Id, List<Apttus_Proposal__Proposal_Line_Item__c>> quoteAssetAcMap = new Map<Id, List<Apttus_Proposal__Proposal_Line_Item__c>>();
    public Map<Id, Apttus_Proposal__Proposal_Line_Item__c> quoteAssetTermMap = new Map<Id, Apttus_Proposal__Proposal_Line_Item__c>();
    public Map<Id, Apttus_Proposal__Proposal_Line_Item__c> quoteAssetEnhancementMap = new Map<Id, Apttus_Proposal__Proposal_Line_Item__c>();
    public Map<Id, Apttus_Proposal__Proposal_Line_Item__c> quoteAssetProductTypeMap = new Map<Id, Apttus_Proposal__Proposal_Line_Item__c>();
    
    public APTPS_TriggerUtil(Map<Id, List<Apttus_Proposal__Proposal_Line_Item__c>> quoteProposalLineMap) {
        this.quoteProposalLineMap = quoteProposalLineMap;
        getLineItemDetails(quoteProposalLineMap);
    }

    private void getLineItemDetails(Map<Id, List<Apttus_Proposal__Proposal_Line_Item__c>> quoteProposalLineMap) {
        try {
            for(Id propId : quoteProposalLineMap.keySet()) {
                List<Apttus_Proposal__Proposal_Line_Item__c> proposalLineItemList = quoteProposalLineMap.get(propId);
                for(Apttus_Proposal__Proposal_Line_Item__c pli : proposalLineItemList) {
                    // Bundle Product
                    if('Product/Service'.equalsIgnoreCase(pli.Apttus_QPConfig__LineType__c) && pli.Apttus_QPConfig__IsPrimaryLine__c 
                    && !'Sales Margin'.equalsIgnoreCase(pli.Apttus_QPConfig__ChargeType__c)) {
                        if(isNewProposalLine(pli))
                            quoteNewBundleMap.put(propId, pli);
                        else
                            quoteAssetBundleMap.put(propId, pli);
                    }

                    // Product Type
                    if('Product Type'.equalsIgnoreCase(pli.Option_Product_Family_System__c) && pli.Apttus_QPConfig__IsPrimaryLine__c) {
                        if(isNewProposalLine(pli))
                            quoteNewProductTypeMap.put(propId, pli);
                        else
                            quoteAssetProductTypeMap.put(propId, pli);
                    }

                    // Aircraft
                    if('Aircraft'.equalsIgnoreCase(pli.Option_Product_Family_System__c) && pli.Apttus_QPConfig__IsPrimaryLine__c) {
                        if(isNewProposalLine(pli)) {
                            /*
                            if(quoteNewAcMap.containsKey(propId))
                                quoteNewAcMap.get(propId).add(pli);
                            else
                                quoteNewAcMap.put(propId, new List<Apttus_Proposal__Proposal_Line_Item__c>{pli});*/
                            quoteNewAcMap = setProposalLineItemMap(quoteNewAcMap, propId, pli);
                        } else {
                            quoteAssetAcMap = setProposalLineItemMap(quoteAssetAcMap, propId, pli);
                        }
                    }

                    // Term
                    if('Term'.equalsIgnoreCase(pli.Option_Product_Name__c) && pli.Apttus_QPConfig__IsPrimaryLine__c) {
                        if(isNewProposalLine(pli))
                            quoteNewTermMap.put(propId, pli);
                        else
                            quoteAssetTermMap.put(propId, pli);
                    }

                    // Enhancements
                    if('Enhancements'.equalsIgnoreCase(pli.Option_Product_Name__c) && pli.Apttus_QPConfig__IsPrimaryLine__c) {
                        if(isNewProposalLine(pli))
                            quoteNewEnhancementMap.put(propId, pli);
                        else
                            quoteAssetEnhancementMap.put(propId, pli);
                    }

                    // TODO: Do we need to check Line Status
                    if(pli.Apttus_QPConfig__IsPrimaryLine__c && 'Sales Margin'.equalsIgnoreCase(pli.Apttus_QPConfig__ChargeType__c))
                        quoteSalesMarginMap.put(propId, pli);
                }
            }
        } catch(Exception e) {
            System.debug('Error while forming proposal line item maps. Exception --> ' + e.getStackTraceString());
        }
    }

    private static boolean isNewProposalLine(Apttus_Proposal__Proposal_Line_Item__c pli) {
        boolean result = false;
        result = 'New'.equalsIgnoreCase(pli.Apttus_QPConfig__LineStatus__c) ? true : false;
        return result;
    }

    private static Map<Id, List<Apttus_Proposal__Proposal_Line_Item__c>> setProposalLineItemMap(Map<Id, List<Apttus_Proposal__Proposal_Line_Item__c>> quoteLineItemMap, 
    Id key, Apttus_Proposal__Proposal_Line_Item__c obj) {
        if(quoteLineItemMap.containsKey(key)) {
            quoteLineItemMap.get(key).add(obj);
        } else {
            quoteLineItemMap.put(key, new List<Apttus_Proposal__Proposal_Line_Item__c>{obj});
        }
        
        return quoteLineItemMap;
    }
    
    //Get record types
    public static Map<String, Id> getRecordTypes(String objectName) {
        Map<String, Id> recTypeMap = new Map<String, Id>();
        try {
            if(objectName != null && !String.isEmpty(objectName)) {
                for(RecordType rec : [SELECT Id, DeveloperName FROM RecordType WHERE SobjectType =: objectName]){
                    recTypeMap.put(rec.DeveloperName, rec.Id);
                }
            } else
                system.debug('Null or empty string is provided instead of Object API name.');
        } catch(Exception e) {
            System.debug('Exception in Class-APTPS_TriggerUtil: Method- getRecordTypes() at line' + e.getLineNumber()+' Message->' + e.getMessage());
        }
        
        return recTypeMap;
    }
}