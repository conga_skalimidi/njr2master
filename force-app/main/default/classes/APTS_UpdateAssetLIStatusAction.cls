/*************************************************************
@Name: APTS_UpdateAssetLIStatusAction
@Author: Siva Kumar
@CreateDate: 23 December 2019
@Description : This Action class is called when Agreement is activated
******************************************************************/
global class APTS_UpdateAssetLIStatusAction {
    @InvocableMethod
    public static void UpdateAssetLIStatus(List<Id> assetIds)
    {
        System.debug(assetIds);
        System.enqueueJob(new APTS_UpdateAssetLIQueueble(assetIds));
    }   
}