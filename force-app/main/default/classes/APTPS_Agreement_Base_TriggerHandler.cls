/************************************************************************************************************************
 @Name: APTPS_Agreement_Base_TriggerHandler
 @Author: Conga PS Dev Team
 @CreateDate:
 @Description: Agreement Trigger Handler. As per trigger event, execute respective business logic from APTPS_Agreement_Helper
 helper class.
 ************************************************************************************************************************
 @ModifiedBy: Conga PS Dev Team
 @ModifiedDate: 17/11/2020
 @ChangeDescription: Added logic for Shares and Leases.
 ************************************************************************************************************************/
public class APTPS_Agreement_Base_TriggerHandler extends TriggerHandler{
    public override void afterInsert(){
        APTPS_Agreement_Helper.setAgreementExtensionFields(Trigger.new);
        APTPS_Agreement_Helper.createDocusignRecipients(Trigger.new);
        //START: S&L logic
        //Comment out this logic - not required
        //APTPS_Agreement_Helper.afterInsertHelper(Trigger.new);
        //END: S&L logic
        
        //CLM Automation
        APTPS_Agreement_Helper.handleCLMAutomation(Trigger.new);
    }

    public override void beforeInsert(){
        List<Apttus__APTS_Agreement__c> allAgList = Trigger.new;
        APTPS_Agreement_Helper.setAgreementFields(Trigger.new);
        APTPS_Agreement_Helper.createAgreementExtensionRecord(Trigger.new);
        //START: S&L logic
        //Comment out this logic - not required
        //APTPS_Agreement_Helper.populateAgreementFields(Trigger.new);
        //APTPS_Agreement_Helper.updateChevronStatus(Trigger.new);
        //END: S&L logic
        
        List<Apttus__APTS_Agreement__c> demoAgList = new List<Apttus__APTS_Agreement__c>();
        List<Apttus__APTS_Agreement__c> ctAgList = new List<Apttus__APTS_Agreement__c>();
        for(Apttus__APTS_Agreement__c ag : allAgList) {
            //Demo product - Agreement
            if(APTS_ConstantUtil.DEMO.equalsIgnoreCase(ag.APTPS_Program_Type__c)) {
                demoAgList.add(ag);
            }
            
            //Corporate Trial - Agreement
            if(APTS_ConstantUtil.CORP_TRIAL.equalsIgnoreCase(ag.APTPS_Program_Type__c)) {
                ctAgList.add(ag);
            }
        }
        
        system.debug('ctAgList-->'+ctAgList+' allAgList-->'+allAgList);
        
       if(!demoAgList.isEmpty()) {
           system.debug('demoAgList-->'+demoAgList);
           APTPS_Agreement_Helper.stampDemoAgreementFields(demoAgList);
       }
            
        
        if(!ctAgList.isEmpty()) {
            system.debug('ctAgList-->'+ctAgList);
           APTPS_Agreement_Helper.stampCTAgreementFields(ctAgList); 
        }
            
    }

    public override void afterUpdate(){
        //APTPS_Agreement_Helper.setAgreementExtensionFields(Trigger.new);
        //START: S&L logic
        //Comment out this logic - not required
        Map<Id, Apttus__APTS_Agreement__c> oldMap = (Map<Id, Apttus__APTS_Agreement__c>)Trigger.OldMap;
        List<Apttus__APTS_Agreement__c> newList = (List<Apttus__APTS_Agreement__c>)Trigger.new;
        //APTPS_Agreement_Helper.afterUpdateHelper(oldMap, Trigger.New);
         String oldAgStatus, oldAgStatusCat, newAgStatus,oldAgFundStatus,newAgFundStatus;
         APTPS_EmailNotification enObj = new APTPS_EmailNotification();
        for (Apttus__APTS_Agreement__c ag : newList){ 
            Apttus__APTS_Agreement__c oldAgObj = oldMap.get(ag.Id);
            //START: Compare Agreement Prior details
            oldAgStatus = oldAgObj.Apttus__Status__c;
            oldAgStatusCat = oldAgObj.Apttus__Status_Category__c;
            newAgStatus = ag.Agreement_Status__c;
            oldAgFundStatus = oldAgObj.Funding_Status__c;
            newAgFundStatus = ag.Funding_Status__c;
             //Agreement Funded Contract 
            if (!APTS_ConstantUtil.FUNDED_CONTRACT.equalsIgnoreCase(oldAgFundStatus) && APTS_ConstantUtil.FUNDED_CONTRACT.equalsIgnoreCase(ag.Funding_Status__c)){
                if(APTS_ConstantUtil.CORP_TRIAL.equalsIgnoreCase(ag.APTPS_Program_Type__c)) {
                    try{
                        String Business_Object = APTS_ConstantUtil.AGREEMENT_OBJ;
                        String Product_Code = APTS_ConstantUtil.CT_PRODUCT_CODE;
                        String Status = ag.Funding_Status__c;
                        String objId = ag.id;
                        String contactId = ag.Apttus__Primary_Contact__c;
                        enObj.call(Product_Code, new Map<String, Object>{'Id' => objId,'Business_Object'=>Business_Object,'Product_Code'=>Product_Code,'Status'=>Status,'ContactId'=>contactId});
                       
                    }catch(APTPS_EmailNotification.ExtensionMalformedCallException e) {
                        system.debug('Error while sending Email Notification for Agreement Id --> '+ag.id+' Error details --> '+e.errMsg);
                    }
                }
            }
             //Agreement Activaiton
            if (!APTS_ConstantUtil.ACTIVATED.equalsIgnoreCase(oldAgStatus) && !APTS_ConstantUtil.IN_EFFECT.equalsIgnoreCase(oldAgStatusCat) && APTS_ConstantUtil.ACTIVATED.equalsIgnoreCase(ag.Apttus__Status__c) && APTS_ConstantUtil.IN_EFFECT.equalsIgnoreCase(ag.Apttus__Status_Category__c)){
                if(APTS_ConstantUtil.CORP_TRIAL.equalsIgnoreCase(ag.APTPS_Program_Type__c) || APTS_ConstantUtil.DEMO.equalsIgnoreCase(ag.APTPS_Program_Type__c)) {
                    try{
                        String Business_Object = APTS_ConstantUtil.AGREEMENT_OBJ;
                        String Product_Code = '';
                        if(APTS_ConstantUtil.CORP_TRIAL.equalsIgnoreCase(ag.APTPS_Program_Type__c)) {
                            Product_Code = APTS_ConstantUtil.CT_PRODUCT_CODE;
                        } else if(APTS_ConstantUtil.DEMO.equalsIgnoreCase(ag.APTPS_Program_Type__c)) {
                            if(APTS_ConstantUtil.CUR_USD.equalsIgnoreCase(ag.CurrencyIsoCode) ) {
                                Product_Code = APTS_ConstantUtil.NJUS_DEMO;
                            } else if(APTS_ConstantUtil.CUR_EUR.equalsIgnoreCase(ag.CurrencyIsoCode) ) {
                                Product_Code = APTS_ConstantUtil.NJE_DEMO;
                            }
                            
                        }               
                        String Status = ag.Apttus__Status__c;
                        String objId = ag.id;
                        String contactId = ag.Apttus__Primary_Contact__c;
                        enObj.call(Product_Code, new Map<String, Object>{'Id' => objId,'Business_Object'=>Business_Object,'Product_Code'=>Product_Code,'Status'=>Status,'ContactId'=>contactId});
                       
                    }catch(APTPS_EmailNotification.ExtensionMalformedCallException e) {
                        system.debug('Error while sending Email Notification for Agreement Id --> '+ag.id+' Error details --> '+e.errMsg);
                    }
                }
            }
        }
        //END: S&L logic
    }

    //START: S&L logic
    public override void beforeUpdate() {
        System.debug('in Before update APTPS_Agreement_Base_TriggerHandler');
        //Comment out this logic - not required
        //Map<Id, Apttus__APTS_Agreement__c> oldMap = (Map<Id, Apttus__APTS_Agreement__c>)Trigger.OldMap;
        //APTPS_Agreement_Helper.beforeUpdateHelper(oldMap, Trigger.New);
        //APTPS_Agreement_Helper.updateChevronStatusWithOldMap(oldMap, Trigger.New);
        //APTPS_Agreement_Helper.updateChevronStatus(Trigger.new);
        
        //START: SNL Agreement lifecycle management
        List<Apttus__APTS_Agreement__c> newAg = Trigger.New;
        List<Apttus__APTS_Agreement__c> demoNewAgList =  new List<Apttus__APTS_Agreement__c>();
        List<Apttus__APTS_Agreement__c> demoNJANewAgList =  new List<Apttus__APTS_Agreement__c>();
        List<Apttus__APTS_Agreement__c> ctNewAgList = new List<Apttus__APTS_Agreement__c>();
        List<Apttus__APTS_Agreement__c> ctAgList = new List<Apttus__APTS_Agreement__c>();
        Map<Id,SObject> oldAgMap = Trigger.OldMap;
        Map<Id,SObject> demoOldAgMap = new Map<Id,SObject>();
        Map<Id,SObject> demoNJAOldAgMap = new Map<Id,SObject>();
        Map<Id,SObject> ctOldAgMap = new Map<Id,SObject>();
        for(Apttus__APTS_Agreement__c ag : newAg) {
            if(APTPS_SNL_Util.isSNLProgramType(ag.APTPS_Program_Type__c)) {
                //NJE Demo product - Agreement
                if(APTS_ConstantUtil.DEMO.equalsIgnoreCase(ag.APTPS_Program_Type__c)){
                    if(APTS_ConstantUtil.CUR_EUR.equalsIgnoreCase(ag.CurrencyIsoCode)) {
                        demoNewAgList.add(ag);
                        demoOldAgMap.put(ag.id,(Apttus__APTS_Agreement__c)oldAgMap.get(ag.Id));
                    } else {
                        demoNJANewAgList.add(ag);
                        demoNJAOldAgMap.put(ag.id,(Apttus__APTS_Agreement__c)oldAgMap.get(ag.Id));
                    }                   
                }               
                //Corporate Trial - Agreement
                if(APTS_ConstantUtil.CORP_TRIAL.equalsIgnoreCase(ag.APTPS_Program_Type__c)) {
                    ctNewAgList.add(ag);
                    ctOldAgMap.put(ag.id,(Apttus__APTS_Agreement__c)oldAgMap.get(ag.Id));
                }
                
            }
        }
        //NJE Demo product - Agreement Status update
        if(!demoNewAgList.isEmpty()) {
           system.debug('demoNewAgList-->'+demoNewAgList);
            system.debug('demoOldAgMap-->'+demoOldAgMap);
           try{
                APTPS_CLMLifeCycle clmFlowObj = new APTPS_CLMLifeCycle();
                clmFlowObj.call(APTS_ConstantUtil.NJE_DEMO  ,new Map<String, Object>{'newAgrList'=>demoNewAgList,'oldAgrList'=>demoOldAgMap});
            } catch(APTPS_CLMLifeCycle.ExtensionMalformedCallException e) {
                system.debug('Error while Populating CLM Status for Demo-->'+e.errMsg);
            }
           
       }
       //NJA Demo product - Agreement Status update
        if(!demoNJANewAgList.isEmpty()) {
           system.debug('demoNJANewAgList-->'+demoNJANewAgList);
           try{
                APTPS_CLMLifeCycle clmFlowObj = new APTPS_CLMLifeCycle();
                clmFlowObj.call(APTS_ConstantUtil.NJUS_DEMO   ,new Map<String, Object>{'newAgrList'=>demoNJANewAgList,'oldAgrList'=>demoNJAOldAgMap});
            } catch(APTPS_CLMLifeCycle.ExtensionMalformedCallException e) {
                system.debug('Error while Populating CLM Status for Demo-->'+e.errMsg);
            }
           
       }
            
        //Corporate Trial - Agreement Status update
        if(!ctNewAgList.isEmpty()) {
           system.debug('ctNewAgList-->'+ctNewAgList);
            try{
                APTPS_CLMLifeCycle clmFlowObj = new APTPS_CLMLifeCycle();
                clmFlowObj.call(APTS_ConstantUtil.CT_PRODUCT_CODE ,new Map<String, Object>{'newAgrList'=>ctNewAgList,'oldAgrList'=>ctOldAgMap});
            } catch(APTPS_CLMLifeCycle.ExtensionMalformedCallException e) {
                system.debug('Error while Populating CLM Status for Corporate Trail-->'+e.errMsg);
            }
           
        }
        
        
        
        
        //END: SNL Agreement lifecycle
    }

    //END: S&L logic
}