/*
 * Created by bbellanca on 5/16/2018.
   Modified By Dev Team Apttus on 12/23/2019
 */
global class ActivateAgreementAction {

        public class ActivateAgreementRequest{
            @InvocableVariable(required=true)
            public Id agreementId;
            @InvocableVariable(required=false)
            public String documentName;
            @InvocableVariable(required=false)
            public String documentBody;
            @InvocableVariable(required=false)
            public String documentId;
        }
        
        @InvocableMethod(label='Activates an Agreement' description='Activates an Agreement')
        public static void activateAgreements (ActivateAgreementRequest[] activateAgreementRequests) {
            List<Id> agreementIdsList = new List<Id>();
            Apttus__APTS_Agreement__c[] agreements = new Apttus__APTS_Agreement__c[]{};
            List<Apttus__APTS_Agreement__c> updateAgreementList = new List<Apttus__APTS_Agreement__c>();
            for(ActivateAgreementRequest request : ActivateAgreementRequests) {
                system.debug('request-->'+request.agreementId);
               
                //if(request.agreementId!=null && request.agreementId!='') {
                    agreementIdsList.add(request.agreementId);   
                //} 
                 List<String> activateDocIds = new List<String>();
                
                
                if(request.documentId != null && request.documentId != '') {
                
                   activateDocIds.add(request.documentId);

                   
                }
                else if(request.documentName != null && request.documentBody != null) {
                
                    Attachment dummyAttachment = new Attachment();
                    dummyAttachment.Name = request.documentName;
                    dummyAttachment.Body = Blob.valueOf(request.documentBody);
                    dummyAttachment.ContentType = 'text/plain';
                    dummyAttachment.ParentId = request.agreementId;
                    insert dummyAttachment;
                    activateDocIds.add(dummyAttachment.Id);
                
                }

                     
                String[] remDocIds = new String[]{};
                Boolean response = Apttus.AgreementWebService.activateAgreement(request.agreementId, activateDocIds, remDocIds);


            }
            
            for(Apttus__APTS_Agreement__c agreementObj : [SELECT Id,Active_Until_Date__c,Apttus__Contract_End_Date__c,Delayed_Start_End_Date__c,Funding_Date__c,RecordType.Name,Grace_Period_End_Date__c,Initial_Term_Expiration_Date__c,Delayed_Start_Amount_Months__c,Initial_Term_Amount_Months__c,Grace_Period_Amount_Months__c,APTS_Referrer_Referee_Agreement__c,APTS_Referrer_Referee_Agreement__r.Funding_Date__c, APTS_Modification_Type__c FROM Apttus__APTS_Agreement__c WHERE Id IN :agreementIdsList ]) {
                if((agreementObj.RecordType.Name == 'Referee' || agreementObj.RecordType.Name=='Referrer') &&  agreementObj.APTS_Referrer_Referee_Agreement__c!=null) {
                    Apttus__APTS_Agreement__c updateAgreement = new Apttus__APTS_Agreement__c();
                    updateAgreement.Id = agreementObj.Id;
                    date fundingDate = agreementObj.APTS_Referrer_Referee_Agreement__r.Funding_Date__c;
                    updateAgreement.Funding_Date__c = fundingDate;
                    
                    //date delayedStartEndDate = fundingDate.addMonths(Integer.valueOf(agreementObj.Delayed_Start_Amount_Months__c));
                    date delayedStartEndDate = fundingDate.addMonths(0);
                    updateAgreement.Delayed_Start_End_Date__c = delayedStartEndDate;
                    //date intialTermExpirationDate = delayedStartEndDate.addMonths(Integer.valueOf(agreementObj.Initial_Term_Amount_Months__c));
                    date intialTermExpirationDate = delayedStartEndDate.addMonths(12);
                    updateAgreement.Initial_Term_Expiration_Date__c = intialTermExpirationDate;
                    //updateAgreement.Grace_Period_End_Date__c = intialTermExpirationDate.addMonths(Integer.valueOf(agreementObj.Grace_Period_Amount_Months__c));
                    updateAgreement.Grace_Period_End_Date__c = intialTermExpirationDate.addMonths(0);
                    updateAgreement.Active_Until_Date__c = intialTermExpirationDate.addMonths(0);
                    updateAgreement.Apttus__Contract_End_Date__c = intialTermExpirationDate.addMonths(0);
                    updateAgreement.Apttus__Contract_Start_Date__c = fundingDate;
                    updateAgreement.Delayed_Start_Amount_Months__c = '0';
                    updateAgreement.Initial_Term_Amount_Months__c = '12';
                    updateAgreement.Grace_Period_Amount_Months__c = '0';
                    
                    updateAgreementList.add(updateAgreement);
                    agreementIdsList.add(updateAgreement.Id);
                    
                }

            }
            try{
                if(!updateAgreementList.isEmpty()) {
                    update updateAgreementList;
                }
            } catch(Exception e){
                System.debug('[ActivateAgreementAction].activateAgreements Message: ' + e.getMessage());
                System.debug('[ActivateAgreementAction].activateAgreements StackTrace: ' + e.getStackTraceString());
                
            }


            
            for(Apttus__APTS_Agreement__c agreementObj : [SELECT Id,Agreement_Status__c, APTS_Modification_Type__c, APTS_Additional_Modification_Type__c, Xfer_to_EDR__c  FROM Apttus__APTS_Agreement__c WHERE Id IN :agreementIdsList ]) {
               
                //Cross Account Assignment - Update Assets and terminate original agreement
                if(agreementObj.APTS_Modification_Type__c == 'Assignment'
                && agreementObj.APTS_Additional_Modification_Type__c == APTS_ConstantUtil.CROSS_ACCOUNT_ASSIGNMENT) {
                    //system.debug('ActivateAgreement - into CA Assignment');
                    System.enqueueJob(new NJ_ProcessAssetLIQueueble(agreementObj.Id));
                }
               
               //This job is to enable the Xfer_to_EDR flag, which will executed at agreement activation
                if(agreementObj.Agreement_Status__c == 'Active' && (agreementObj).Xfer_to_EDR__c == false){
                    NJ_ExecuteXferToEDRUpdateBatch batch = new NJ_ExecuteXferToEDRUpdateBatch(agreementObj.Id);
                    Database.executeBatch( batch, 1 );

                }
            }
  
        }

    
}