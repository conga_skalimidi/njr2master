/**
 * @description       : Test class for APTPS_Agreement_Helper
 * @author            : Sagar Solanki
 * @group             : Apttus(Conga) Dev
 * @last modified on  : 10-29-2020
 * @last modified by  : Sagar Solanki
 *
 * Modifications Log
 * Ver   Date         Author          Modification
 * 1.0   10-26-2020   Sagar Solanki   Added test class : GCM-9780
 * 2.0	 03-25-2021	  Conga DEV Team  Do cleanup for testsetup
 **/
@isTest
public with sharing class APTPS_Agreement_HelperTest{
    @testsetup
    static void setup(){
        Account accToInsert = new Account();
		accToInsert.Name = 'APTS Test Account';
		accToInsert.APTS_Has_Active_NJUS_Agreement__c = true;
		accToInsert.APTS_Has_Active_NJE_Agreement__c = true;
		insert accToInsert;

        Opportunity testOpportunity = APTS_CPQTestUtility.createOpportunity('test Apttus Opportunity', 'New Customer', accToInsert.Id, 'Propose');
		insert testOpportunity;

        Contact contactToInsert = APTS_CPQTestUtility.createContact('ContactFN', 'Solanki', accToInsert.Id);
		insert contactToInsert;
    }
    
	@isTest
	static void setAgreementFields(){
		Id recordTypeIdNJUS = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('NJUS New Card Sale').getRecordTypeId();
		Id recordTypeIdNJE = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('NJE New Card Sale').getRecordTypeId();

        Account accToInsert = [SELECT Id, Name, APTS_Has_Active_NJUS_Agreement__c, APTS_Has_Active_NJE_Agreement__c 
                              FROM Account WHERE Name = 'APTS Test Account' LIMIT 1];
        
        Opportunity testOpportunity = [SELECT Id FROM Opportunity WHERE Name = 'test Apttus Opportunity' LIMIT 1];

		Apttus_Config2__PriceList__c testPriceList = APTS_CPQTestUtility.createPriceList('test Apttus Price List', true);
		insert testPriceList;

		Apttus_Proposal__Proposal__c testProposal = APTS_CPQTestUtility.createProposal('test Apttus Proposal', accToInsert.Id, testOpportunity.Id, 'Proposal', testPriceList.Id);
		//populate custom fields value if required
		insert testProposal;

		Product2 testProduct = APTS_CPQTestUtility.createProduct('Card', 'APTS', 'Apttus', 'Bundle', true, true, true, true);
		//populate custom fields value if required
		insert testProduct;

		//Populate Proposal Line item data
		List<Apttus_Proposal__Proposal_Line_Item__c> lineItemList = new List<Apttus_Proposal__Proposal_Line_Item__c>();
		Apttus_Proposal__Proposal_Line_Item__c quoteli = new Apttus_Proposal__Proposal_Line_Item__c();
		quoteli.APTS_Modification_Type__c = 'Term Extension';
		quoteli.Apttus_Proposal__Product__c = testProduct.Id;
		quoteli.Apttus_QPConfig__LineStatus__c = 'New';
		quoteli.Apttus_Proposal__Proposal__c = testProposal.Id;
		quoteli.Option_Product_Name__c = 'Enhancements';
		lineItemList.add(quoteli);

		Apttus_Proposal__Proposal_Line_Item__c quoteli1 = new Apttus_Proposal__Proposal_Line_Item__c();
		quoteli1.APTS_Modification_Type__c = 'Collective Service Area';
		quoteli1.Apttus_Proposal__Product__c = testProduct.Id;
		quoteli1.Apttus_QPConfig__LineStatus__c = 'New';
		quoteli1.Apttus_Proposal__Proposal__c = testProposal.Id;
		quoteli1.Option_Product_Name__c = 'Collective Service Area';
		lineItemList.add(quoteli1);

		Apttus_Proposal__Proposal_Line_Item__c quoteli2 = new Apttus_Proposal__Proposal_Line_Item__c();
		quoteli2.APTS_Modification_Type__c = 'Collective Service Area';
		quoteli2.Apttus_Proposal__Product__c = testProduct.Id;
		quoteli2.Apttus_QPConfig__LineStatus__c = 'New';
		quoteli2.Apttus_Proposal__Proposal__c = testProposal.Id;
		quoteli2.Option_Product_Name__c = APTS_Constants.PPD_EXCLUSIONS;
		lineItemList.add(quoteli2);

		Apttus_Proposal__Proposal_Line_Item__c quoteli3 = new Apttus_Proposal__Proposal_Line_Item__c();
		quoteli3.APTS_Modification_Type__c = 'Collective Service Area';
		quoteli3.Apttus_Proposal__Product__c = testProduct.Id;
		quoteli3.Apttus_QPConfig__LineStatus__c = 'New';
		quoteli3.Apttus_Proposal__Proposal__c = testProposal.Id;
		quoteli3.Option_Product_Name__c = 'Flight Enhancements';
		lineItemList.add(quoteli3);

		Apttus_Proposal__Proposal_Line_Item__c quoteli4 = new Apttus_Proposal__Proposal_Line_Item__c();
		quoteli4.APTS_Modification_Type__c = 'Collective Service Area';
		quoteli4.Apttus_Proposal__Product__c = testProduct.Id;
		quoteli4.Apttus_QPConfig__LineStatus__c = 'New';
		quoteli4.Apttus_Proposal__Proposal__c = testProposal.Id;
		quoteli4.Option_Product_Name__c = 'Upgrade';
		lineItemList.add(quoteli4);

		Apttus_Proposal__Proposal_Line_Item__c quoteli5 = new Apttus_Proposal__Proposal_Line_Item__c();
		quoteli5.APTS_Modification_Type__c = 'Collective Service Area';
		quoteli5.Apttus_Proposal__Product__c = testProduct.Id;
		quoteli5.Apttus_QPConfig__LineStatus__c = 'New';
		quoteli5.Apttus_Proposal__Proposal__c = testProposal.Id;
		quoteli5.Option_Product_Name__c = 'Short Leg Waiver';
		lineItemList.add(quoteli5);

		Apttus_Proposal__Proposal_Line_Item__c quoteli6 = new Apttus_Proposal__Proposal_Line_Item__c();
		quoteli6.APTS_Modification_Type__c = 'Direct Conversion';
		quoteli6.Apttus_Proposal__Product__c = testProduct.Id;
		quoteli6.Apttus_QPConfig__LineStatus__c = 'Mofification';
		quoteli6.Apttus_Proposal__Proposal__c = testProposal.Id;
		quoteli6.Option_Product_Name__c = 'Short Leg Waiver';
		quoteli6.Option_Product_Family_System__c = 'Aircraft';
		lineItemList.add(quoteli6);

		insert lineItemList;

		ID accID = accToInsert.Id;
		//Prepare agreement data
		List<Apttus__APTS_Agreement__c> aggList = new List<Apttus__APTS_Agreement__c>();

		Apttus__APTS_Agreement__c aggToInsert = new Apttus__APTS_Agreement__c();
		aggToInsert.Name = 'APTS Test Agreement 1';
		aggToInsert.Program__c = 'NetJets U.S.';
		aggToInsert.Apttus__Account__c = accID;
		aggToInsert.RecordTypeId = recordTypeIdNJUS;
		aggToInsert.Apttus__Status_Category__c = 'Request';
		aggToInsert.Apttus__Status__c = 'Request';
		aggToInsert.APTPS_Number_of_Conversion_Aircraft_Line__c = 5;
		System.debug('PROPOSAL ID:' + testProposal.Id);
		aggToInsert.Apttus_QPComply__RelatedProposalId__c = testProposal.Id;
		aggToInsert.APTS_Modification_Type__c = 'Assignment';
		insert aggToInsert;
		aggList.add(aggToInsert);
        
        Test.startTest();
		APTPS_Agreement_Helper.setAgreementFields(aggList);

		Apttus__APTS_Agreement__c agreementDetails = [SELECT Id, APTPS_Number_of_Conversion_Aircraft_Line__c

		                                              FROM Apttus__APTS_Agreement__c
		                                              WHERE Id = :aggToInsert.Id
		                                              LIMIT 1];

		System.assertEquals(6, agreementDetails.APTPS_Number_of_Conversion_Aircraft_Line__c, 'Number of conversionofaircraftline');

		Test.stopTest();
	}

	@isTest
	static void setAgreementExtensionFields(){
		Id recordTypeIdNJUS = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('NJUS New Card Sale').getRecordTypeId();
		Id recordTypeIdNJE = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('NJE New Card Sale').getRecordTypeId();

		Account accToInsert = [SELECT Id, Name, APTS_Has_Active_NJUS_Agreement__c, APTS_Has_Active_NJE_Agreement__c 
                              FROM Account WHERE Name = 'APTS Test Account' LIMIT 1];
        
        Opportunity testOpportunity = [SELECT Id FROM Opportunity WHERE Name = 'test Apttus Opportunity' LIMIT 1];

        Contact contactToInsert = [SELECT Id FROM Contact WHERE FirstName = 'ContactFN' LIMIT 1];

		Apttus_Config2__PriceList__c testPriceList = APTS_CPQTestUtility.createPriceList('test Apttus Price List', true);
		insert testPriceList;

		Apttus_Proposal__Proposal__c testProposal = APTS_CPQTestUtility.createProposal('test Apttus Proposal', accToInsert.Id, testOpportunity.Id, 'Proposal', testPriceList.Id);
		insert testProposal;

		APTPS_Agreement_Extension__c extAgreement = new APTPS_Agreement_Extension__c();
		extAgreement.APTPS_Assignor_Card_Number__c = '';
		extAgreement.APTPS_Previous_Account__c = '';
		extAgreement.APTPS_Previous_Primary_Contact__c = '';
		extAgreement.APTPS_Previous_Legal_Entity__c = '';
		extAgreement.APTPS_Aircraft_1_Hours_Text__c = 'One hundred';
		extAgreement.APTPS_Aircraft_2_Hours_Text__c = 'One hundred Twenty Five';
		extAgreement.APTPS_Aircraft_1_Hours__c = 10;
		extAgreement.APTPS_Aircraft_2_Hours__c = 10;

		insert extAgreement;

		Product2 testProduct = APTS_CPQTestUtility.createProduct('Card', 'APTS', 'Apttus', 'Bundle', true, true, true, true);
		//populate custom fields value if required
		insert testProduct;

		//Populate Proposal Line item data
		List<Apttus_Proposal__Proposal_Line_Item__c> lineItemList = new List<Apttus_Proposal__Proposal_Line_Item__c>();
		Apttus_Proposal__Proposal_Line_Item__c quoteli = new Apttus_Proposal__Proposal_Line_Item__c();
		quoteli.APTS_Modification_Type__c = 'Term Extension';
		quoteli.Apttus_Proposal__Product__c = testProduct.Id;
		quoteli.Apttus_QPConfig__LineStatus__c = 'New';
		quoteli.Apttus_Proposal__Proposal__c = testProposal.Id;
		quoteli.Option_Product_Name__c = 'Enhancements';
		quoteli.Apttus_QPConfig__ChargeType__c = 'Enhancements';
		quoteli.Option_Product_Family_System__c = 'Aircraft';
		lineItemList.add(quoteli);

		Apttus_Proposal__Proposal_Line_Item__c quoteli1 = new Apttus_Proposal__Proposal_Line_Item__c();
		quoteli1.APTS_Modification_Type__c = 'Collective Service Area';
		quoteli1.Apttus_Proposal__Product__c = testProduct.Id;
		quoteli1.Apttus_QPConfig__LineStatus__c = 'Modification';
		quoteli1.Apttus_Proposal__Proposal__c = testProposal.Id;
		quoteli1.Option_Product_Name__c = 'Collective Service Area';
		quoteli1.Apttus_QPConfig__ChargeType__c = 'Product Type';
		quoteli1.Aircraft_Line_Number__c = 2;
		quoteli1.Option_Product_Family_System__c = 'Aircraft';
		lineItemList.add(quoteli1);

		Apttus_Proposal__Proposal_Line_Item__c quoteli2 = new Apttus_Proposal__Proposal_Line_Item__c();
		quoteli2.APTS_Modification_Type__c = 'Collective Service Area';
		quoteli2.Apttus_Proposal__Product__c = testProduct.Id;
		quoteli2.Apttus_QPConfig__LineStatus__c = 'New';
		quoteli2.Apttus_Proposal__Proposal__c = testProposal.Id;
		quoteli2.Option_Product_Name__c = APTS_Constants.PPD_EXCLUSIONS;
		quoteli2.Apttus_QPConfig__ChargeType__c = 'Enhancements';
		quoteli2.Aircraft_Line_Number__c = 1;
		quoteli2.Option_Product_Family_System__c = 'Aircraft';
		lineItemList.add(quoteli2);

		Apttus_Proposal__Proposal_Line_Item__c quoteli3 = new Apttus_Proposal__Proposal_Line_Item__c();
		quoteli3.APTS_Modification_Type__c = 'Collective Service Area';
		quoteli3.Apttus_Proposal__Product__c = testProduct.Id;
		quoteli3.Apttus_QPConfig__LineStatus__c = 'New';
		quoteli3.Apttus_Proposal__Proposal__c = testProposal.Id;
		quoteli3.Option_Product_Name__c = 'Enhancements';
		quoteli3.Apttus_QPConfig__ChargeType__c = 'Enhancements';
		quoteli3.Aircraft_Line_Number__c = 2;
		quoteli3.Option_Product_Family_System__c = 'Enhancement';
		lineItemList.add(quoteli3);

		Apttus_Proposal__Proposal_Line_Item__c quoteli4 = new Apttus_Proposal__Proposal_Line_Item__c();
		quoteli4.APTS_Modification_Type__c = 'Collective Service Area';
		quoteli4.Apttus_Proposal__Product__c = testProduct.Id;
		quoteli4.Apttus_QPConfig__LineStatus__c = 'New';
		quoteli4.Apttus_Proposal__Proposal__c = testProposal.Id;
		quoteli4.Option_Product_Name__c = 'Upgrade';
		quoteli4.Apttus_QPConfig__ChargeType__c = 'Purchase Price';
		quoteli4.Aircraft_Line_Number__c = 2;
		quoteli4.Option_Product_Family_System__c = 'Aircraft';
		lineItemList.add(quoteli4);

		Apttus_Proposal__Proposal_Line_Item__c quoteli5 = new Apttus_Proposal__Proposal_Line_Item__c();
		quoteli5.APTS_Modification_Type__c = 'Collective Service Area';
		quoteli5.Apttus_Proposal__Product__c = testProduct.Id;
		quoteli5.Apttus_QPConfig__LineStatus__c = 'New';
		quoteli5.Apttus_Proposal__Proposal__c = testProposal.Id;
		quoteli5.Option_Product_Name__c = 'Short Leg Waiver';
		quoteli5.Aircraft_Line_Number__c = 1;
		quoteli5.Option_Product_Family_System__c = 'Aircraft';
		lineItemList.add(quoteli5);

		Apttus_Proposal__Proposal_Line_Item__c quoteli6 = new Apttus_Proposal__Proposal_Line_Item__c();
		quoteli6.APTS_Modification_Type__c = 'Direct Conversion';
		quoteli6.Apttus_Proposal__Product__c = testProduct.Id;
		quoteli6.Apttus_QPConfig__LineStatus__c = 'Mofification';
		quoteli6.Apttus_Proposal__Proposal__c = testProposal.Id;
		quoteli6.Option_Product_Name__c = 'Short Leg Waiver';
		quoteli6.Option_Product_Family_System__c = 'Aircraft';
		quoteli6.Aircraft_Line_Number__c = 1;
		lineItemList.add(quoteli6);

		Apttus_Proposal__Proposal_Line_Item__c quoteli7 = new Apttus_Proposal__Proposal_Line_Item__c();
		quoteli7.APTS_Modification_Type__c = 'Assignment';
		quoteli7.Apttus_Proposal__Product__c = testProduct.Id;
		quoteli7.Apttus_QPConfig__LineStatus__c = 'Mofification';
		quoteli7.Apttus_Proposal__Proposal__c = testProposal.Id;
		quoteli7.Option_Product_Name__c = 'QS Executive';
		quoteli7.Apttus_QPConfig__ChargeType__c = 'Purchase Price';
		quoteli7.Aircraft_Line_Number__c = 2;
		quoteli7.Option_Product_Family_System__c = 'Aircraft';
		lineItemList.add(quoteli7);

		insert lineItemList;


		ID accID = accToInsert.Id;
		//Prepare agreement data
		List<Apttus__APTS_Agreement__c> aggList = new List<Apttus__APTS_Agreement__c>();

		Apttus__APTS_Agreement__c aggToInsert = new Apttus__APTS_Agreement__c();
		aggToInsert.Name = 'APTS Test Agreement 1';
		aggToInsert.Program__c = 'NetJets U.S.';
		aggToInsert.Apttus__Account__c = accID;
		aggToInsert.RecordTypeId = recordTypeIdNJUS;
		aggToInsert.Apttus__Status_Category__c = 'Request';
		aggToInsert.Apttus__Status__c = 'Request';
		aggToInsert.APTPS_Number_of_Conversion_Aircraft_Line__c = 5;
		System.debug('PROPOSAL ID:' + testProposal.Id);
		aggToInsert.Apttus_QPComply__RelatedProposalId__c = testProposal.Id;
		aggToInsert.APTPS_Agreement_Extension__c = extAgreement.Id;
		aggToInsert.Apttus__Primary_Contact__c = contactToInsert.Id;
		insert aggToInsert;
		aggList.add(aggToInsert);

		Test.startTest();
		APTPS_Agreement_Helper.setAgreementFields(aggList);

		Decimal decNo = 10.50;
		APTPS_Agreement_Helper.convertNumberToWord(decNo);
		Decimal decNo1 =.5;
		APTPS_Agreement_Helper.convertNumberToWord(decNo1);

		Apttus__APTS_Agreement__c agreementDetails = [SELECT Id, APTPS_Number_of_Conversion_Aircraft_Line__c
		                                              FROM Apttus__APTS_Agreement__c
		                                              WHERE Id = :aggToInsert.Id
		                                              LIMIT 1];

		System.assertEquals(6, agreementDetails.APTPS_Number_of_Conversion_Aircraft_Line__c, 'Number of conversionofaircraftline');

		Test.stopTest();
	}
}