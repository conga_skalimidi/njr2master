/***********************************************************************************************************************
@Name: APTS_UpdateAssetLIQueuebleTest
@Author: Siva Kumar
@CreateDate: 27 October 2020
@Description: APEX class to test classes APTS_UpdateAssetLIQueueble, APTS_UpdateAssetLIStatusAction
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/
@isTest
public with sharing class APTS_UpdateAssetLIQueuebleTest {
    Public static Account accToInsert;
    Public static Opportunity testOpportunity;
    @testsetup
    static void setup(){
        accToInsert = new Account();
        accToInsert.Name = 'APTS Test Account';
        accToInsert.APTS_Has_Active_NJUS_Agreement__c = true;
        accToInsert.APTS_Has_Active_NJE_Agreement__c = true;
        insert accToInsert;
        
        testOpportunity = APTS_CPQTestUtility.createOpportunity('test Apttus Opportunity', 'New Customer', accToInsert.Id, 'Propose');
        insert testOpportunity;
    }
    
    @isTest
    static void updateAssetLIModificationAETest(){
        Id recordTypeIdNJUS = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('NJUS New Card Sale').getRecordTypeId();
        Id recordTypeIdNJE = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('NJE New Card Sale').getRecordTypeId();

        Account accToInsert = [SELECT Id, Name, APTS_Has_Active_NJUS_Agreement__c, APTS_Has_Active_NJE_Agreement__c 
                              FROM Account WHERE Name = 'APTS Test Account' LIMIT 1];
        
        Opportunity testOpportunity = [SELECT Id FROM Opportunity WHERE Name = 'test Apttus Opportunity' LIMIT 1];

        Apttus_Config2__PriceList__c testPriceList = APTS_CPQTestUtility.createPriceList('test Apttus Price List', true);
        insert testPriceList;
		List<Apttus_Proposal__Proposal__c> proposalList = new List<Apttus_Proposal__Proposal__c>();
        Apttus_Proposal__Proposal__c testProposal = APTS_CPQTestUtility.createProposal('test Apttus Proposal', accToInsert.Id, testOpportunity.Id, 'Proposal', testPriceList.Id);
        //populate custom fields value if required
        testProposal.APTS_New_Sale_or_Modification__c = 'Modification';
        testProposal.APTS_Modification_Type__c = 'Add Enhancements';
        proposalList.add(testProposal);
        
        Apttus_Proposal__Proposal__c testProposal1 = APTS_CPQTestUtility.createProposal('test Apttus Proposal', accToInsert.Id, testOpportunity.Id, 'Proposal', testPriceList.Id);
        //populate custom fields value if required
        testProposal1.APTS_New_Sale_or_Modification__c = 'Modification';
        testProposal1.APTS_Modification_Type__c = 'New Card Sale Conversion';
        proposalList.add(testProposal1);
        
        Apttus_Proposal__Proposal__c testProposal2 = APTS_CPQTestUtility.createProposal('test Apttus Proposal', accToInsert.Id, testOpportunity.Id, 'Proposal', testPriceList.Id);
        //populate custom fields value if required
        testProposal2.APTS_New_Sale_or_Modification__c = 'Modification';
        testProposal2.APTS_Modification_Type__c = 'Assignment';
        proposalList.add(testProposal2);
        
        insert proposalList;
        
        /*Apttus_Proposal__Proposal__c testProposal3 = APTS_CPQTestUtility.createProposal('test Apttus Proposal', accToInsert.Id, testOpportunity.Id, 'Proposal', testPriceList.Id);
        //populate custom fields value if required
        testProposal3.APTS_New_Sale_or_Modification__c = 'Modification';
        testProposal3.APTS_Modification_Type__c = 'Termination';
        insert testProposal3;*/
        
        ID accID = accToInsert.Id;
        
        List<Apttus__APTS_Agreement__c> aggList = new List<Apttus__APTS_Agreement__c>();
        
        //New Sale      
        Apttus__APTS_Agreement__c newSaleAgg = new Apttus__APTS_Agreement__c();
        newSaleAgg.Name = 'APTS Test Agreement 1111';
        newSaleAgg.Program__c = 'NetJets U.S.';
        newSaleAgg.Apttus__Account__c = accToInsert.Id;
        newSaleAgg.Card_Number__c = String.valueof((Math.random() * 100));
        newSaleAgg.APTS_Modification_Type__c = 'New Sale';
        newSaleAgg.RecordTypeId = recordTypeIdNJUS;
        newSaleAgg.Apttus_QPComply__RelatedProposalId__c = testProposal.Id;
        newSaleAgg.Agreement_Status__c = 'In Modification';
        aggList.add(newSaleAgg);
        
        //New Card Sale Conversion
        Apttus__APTS_Agreement__c NCSCAgg = new Apttus__APTS_Agreement__c();
        NCSCAgg.Name = 'APTS Test Agreement 1111';
        NCSCAgg.Program__c = 'NetJets U.S.';
        NCSCAgg.Apttus__Account__c = accID;
        NCSCAgg.Card_Number__c = String.valueof((Math.random() * 100));
        NCSCAgg.APTS_Modification_Type__c = 'New Card Sale Conversion';
        NCSCAgg.RecordTypeId = recordTypeIdNJUS;
        NCSCAgg.Apttus_QPComply__RelatedProposalId__c = testProposal1.Id;
        aggList.add(NCSCAgg);
        
        //Termination
        /*Apttus__APTS_Agreement__c terminationAgg = new Apttus__APTS_Agreement__c();
        terminationAgg.Name = 'APTS Test Agreement 1111';
        terminationAgg.Program__c = 'NetJets U.S.';
        terminationAgg.Apttus__Account__c = accToInsert.Id;
        terminationAgg.Card_Number__c = String.valueof((Math.random() * 100));
        terminationAgg.APTS_Modification_Type__c = 'Termination';
        terminationAgg.RecordTypeId = recordTypeIdNJUS;
        terminationAgg.Apttus_QPComply__RelatedProposalId__c = testProposal3.Id;
        aggList.add(terminationAgg);    
        //insert terminationAgg;    */      
        
        //Add Enhancements
        Apttus__APTS_Agreement__c AEAgg = new Apttus__APTS_Agreement__c();
        AEAgg.Name = 'APTS Test Agreement 1111';
        AEAgg.Program__c = 'NetJets U.S.';
        AEAgg.Apttus__Account__c = accID;
        AEAgg.Card_Number__c = String.valueof((Math.random() * 100));
        AEAgg.APTS_Modification_Type__c = 'Add Enhancements';
        AEAgg.RecordTypeId = recordTypeIdNJUS;
        AEAgg.Apttus_QPComply__RelatedProposalId__c = testProposal.Id;
        aggList.add(AEAgg);
        
        //Assignment
        Apttus__APTS_Agreement__c  assignmentAgg = new Apttus__APTS_Agreement__c();
        assignmentAgg.Name = 'APTS Test Agreement 1111';
        assignmentAgg.Program__c = 'NetJets U.S.';
        assignmentAgg.Apttus__Account__c = accID;
        assignmentAgg.Card_Number__c = String.valueof((Math.random() * 100));
        assignmentAgg.APTS_Modification_Type__c = 'Assignment';
        assignmentAgg.RecordTypeId = recordTypeIdNJUS;
        assignmentAgg.Apttus_QPComply__RelatedProposalId__c = testProposal2.Id;
        aggList.add(assignmentAgg);
        
        insert aggList;
        
        List<id> assetLIIds = new list<id>();
        
        //Create Asset Line Item for New Card Sale Conversion       
        List<Apttus_Config2__AssetLineItem__c> assetLIList = new List<Apttus_Config2__AssetLineItem__c>();
        Apttus_Config2__AssetLineItem__c  NCSCAseetLI = new Apttus_Config2__AssetLineItem__c ();
        NCSCAseetLI.Apttus_CMConfig__AgreementId__c = aggList[1].Id;
        NCSCAseetLI.Apttus_Config2__ChargeType__c = 'Purchase Price';
        NCSCAseetLI.Apttus_Config2__LineType__c = 'Product/Service';
        NCSCAseetLI.Apttus_Config2__IsPrimaryLine__c = TRUE;
        NCSCAseetLI.APTS_Original_Agreement__c = aggList[0].Id;
        assetLIList.add(NCSCAseetLI);
        
        Apttus_Config2__AssetLineItem__c  NCSCAseetLI1 = new Apttus_Config2__AssetLineItem__c ();
        NCSCAseetLI1.Apttus_CMConfig__AgreementId__c = aggList[1].Id;
        NCSCAseetLI1.Apttus_Config2__ChargeType__c = 'Purchase Price';
        NCSCAseetLI1.Apttus_Config2__LineType__c = 'Product/Service';
        NCSCAseetLI1.Apttus_Config2__IsPrimaryLine__c = TRUE;
        NCSCAseetLI1.APTS_Original_Agreement__c = aggList[0].Id;
        assetLIList.add(NCSCAseetLI1);
        
        //Create Asset Line Item for Assignment         
        Apttus_Config2__AssetLineItem__c  assignmentAssetLI = new Apttus_Config2__AssetLineItem__c ();
        assignmentAssetLI.Apttus_CMConfig__AgreementId__c = aggList[3].Id;
        assignmentAssetLI.Apttus_Config2__ChargeType__c = 'Purchase Price';
        assignmentAssetLI.Apttus_Config2__LineType__c = 'Product/Service';
        assignmentAssetLI.Apttus_Config2__IsPrimaryLine__c = TRUE;
        assignmentAssetLI.APTS_Original_Agreement__c = aggList[0].Id;
        assetLIList.add(assignmentAssetLI);
        
        Apttus_Config2__AssetLineItem__c  assignmentAssetLI1 = new Apttus_Config2__AssetLineItem__c ();
        assignmentAssetLI1.Apttus_CMConfig__AgreementId__c = aggList[3].Id;
        assignmentAssetLI1.Apttus_Config2__ChargeType__c = 'Purchase Price';
        assignmentAssetLI1.Apttus_Config2__LineType__c = 'Product/Service';
        assignmentAssetLI1.Apttus_Config2__IsPrimaryLine__c = TRUE;
        assignmentAssetLI1.APTS_Original_Agreement__c = aggList[0].Id;
        assetLIList.add(assignmentAssetLI1);
        
        //Create Asset Line Item for Add Enhancements           
        Apttus_Config2__AssetLineItem__c  AEALI = new Apttus_Config2__AssetLineItem__c ();
        AEALI.Apttus_CMConfig__AgreementId__c = aggList[2].Id;
        AEALI.Apttus_Config2__ChargeType__c = 'Purchase Price';
        AEALI.Apttus_Config2__LineType__c = 'Product/Service';
        AEALI.Apttus_Config2__IsPrimaryLine__c = TRUE;
        AEALI.APTS_Original_Agreement__c = aggList[0].Id;
        assetLIList.add(AEALI);
        
        Apttus_Config2__AssetLineItem__c  AEALI1 = new Apttus_Config2__AssetLineItem__c ();
        AEALI1.Apttus_CMConfig__AgreementId__c = aggList[2].Id;
        AEALI1.Apttus_Config2__ChargeType__c = 'Purchase Price';
        AEALI1.Apttus_Config2__LineType__c = 'Option';
        AEALI1.Apttus_Config2__IsPrimaryLine__c = TRUE;
        AEALI1.APTS_Original_Agreement__c = aggList[0].Id;
        assetLIList.add(AEALI1);
        insert assetLIList;
        
        //Create Asset Line Item Attribute for New Card Sale Conversion
        Apttus_Config2__AssetAttributeValue__c NCSCAseetAV = new Apttus_Config2__AssetAttributeValue__c();
        NCSCAseetAV.Apttus_Config2__AssetLineItemId__c = assetLIList[0].Id;
        NCSCAseetAV.Aircraft_Remaining_Hours__c = 20;
        NCSCAseetAV.Requested_Hours__c = 10;
        insert NCSCAseetAV;
        NCSCAseetLI.Apttus_Config2__AttributeValueId__c = NCSCAseetAV.id;
        update NCSCAseetLI;
        Apttus_Config2__AssetAttributeValue__c NCSCAseetAV1 = new Apttus_Config2__AssetAttributeValue__c();
        NCSCAseetAV1.Apttus_Config2__AssetLineItemId__c = assetLIList[1].Id;
        NCSCAseetAV1.Aircraft_Remaining_Hours__c = 20;
        NCSCAseetAV1.Requested_Hours__c = 25;
        insert NCSCAseetAV1;
        NCSCAseetLI1.Apttus_Config2__AttributeValueId__c = NCSCAseetAV1.id;
        update NCSCAseetLI1;
        
        assetLIIds.add(assetLIList[0].Id);
        assetLIIds.add(assetLIList[1].Id);
        assetLIIds.add(assetLIList[2].Id);
        assetLIIds.add(assetLIList[3].Id);
        assetLIIds.add(assetLIList[4].Id);
        assetLIIds.add(assetLIList[5].Id);
        
        //Start Test
        Test.startTest();
        APTS_UpdateAssetLIStatusAction.UpdateAssetLIStatus(assetLIIds);
        //Stop Test
        Test.stopTest();
                
    }
    
    @isTest
    static void updateAssetLITerminationTest(){
        List<Account> accList = new List<Account>();
        Id recordTypeIdNJUS = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('NJUS New Card Sale').getRecordTypeId();
        Id recordTypeIdNJE = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('NJE New Card Sale').getRecordTypeId();

        Account accToInsert = [SELECT Id, Name, APTS_Has_Active_NJUS_Agreement__c, APTS_Has_Active_NJE_Agreement__c 
                              FROM Account WHERE Name = 'APTS Test Account' LIMIT 1];
        
        Opportunity testOpportunity = [SELECT Id FROM Opportunity WHERE Name = 'test Apttus Opportunity' LIMIT 1];

        Apttus_Config2__PriceList__c testPriceList = APTS_CPQTestUtility.createPriceList('test Apttus Price List', true);
        insert testPriceList;

        Apttus_Proposal__Proposal__c testProposal = APTS_CPQTestUtility.createProposal('test Apttus Proposal', accToInsert.Id, testOpportunity.Id, 'Proposal', testPriceList.Id);
        //populate custom fields value if required
        testProposal.APTS_New_Sale_or_Modification__c = 'Modification';
        testProposal.APTS_Modification_Type__c = 'Termination';
        insert testProposal;            
        
        List<Apttus__APTS_Agreement__c> aggList = new List<Apttus__APTS_Agreement__c>();
        Apttus__APTS_Agreement__c aggToInsert = new Apttus__APTS_Agreement__c();
        aggToInsert.Name = 'APTS Test Agreement 1111';
        aggToInsert.Program__c = 'NetJets U.S.';
        aggToInsert.Apttus__Account__c = accToInsert.Id;
        aggToInsert.Card_Number__c = '123456789';
        aggToInsert.APTS_Modification_Type__c = 'Termination';
        aggToInsert.RecordTypeId = recordTypeIdNJUS;
        aggToInsert.Apttus_QPComply__RelatedProposalId__c = testProposal.Id;
        aggList.add(aggToInsert);
        
        Apttus__APTS_Agreement__c aggToInsert1 = new Apttus__APTS_Agreement__c();
        aggToInsert1.Name = 'APTS Test Agreement 1111';
        aggToInsert1.Program__c = 'NetJets U.S.';
        aggToInsert1.Apttus__Account__c = accToInsert.Id;
        aggToInsert1.Card_Number__c = '132456789';
        aggToInsert1.APTS_Modification_Type__c = 'New Sale';
        aggToInsert1.RecordTypeId = recordTypeIdNJUS;
        aggToInsert1.Apttus_QPComply__RelatedProposalId__c = testProposal.Id;
        aggList.add(aggToInsert1);
        insert aggList;
        
		List<Apttus_Config2__AssetLineItem__c> assetLIs = new List<Apttus_Config2__AssetLineItem__c>();
        Apttus_Config2__AssetLineItem__c  assetLi = new Apttus_Config2__AssetLineItem__c ();
        assetLi.Apttus_CMConfig__AgreementId__c = aggList[0].Id;
        assetLi.Apttus_Config2__ChargeType__c = 'Purchase Price';
        assetLi.Apttus_Config2__LineType__c = 'Product/Service';
        assetLi.Apttus_Config2__IsPrimaryLine__c = TRUE;
        assetLi.APTS_Original_Agreement__c = aggList[1].Id;
        assetLIs.add(assetLi);
        
        //Create New Asset Line Item
        Apttus_Config2__AssetLineItem__c  assetLi1 = new Apttus_Config2__AssetLineItem__c ();
        assetLi1.Apttus_CMConfig__AgreementId__c = aggList[0].Id;
        assetLi1.Apttus_Config2__ChargeType__c = 'Purchase Price';
        assetLi1.Apttus_Config2__LineType__c = 'Product/Service';
        assetLi1.Apttus_Config2__IsPrimaryLine__c = TRUE;
        assetLi1.APTS_Original_Agreement__c = aggList[1].Id;
        assetLis.add(assetLi1);

        Apttus_Config2__AssetLineItem__c  assetLi2 = new Apttus_Config2__AssetLineItem__c ();
        assetLi2.Apttus_CMConfig__AgreementId__c = aggList[0].Id;
        assetLi2.Apttus_Config2__ChargeType__c = 'Purchase Price';
        assetLi2.Apttus_Config2__LineType__c = 'Option';
        assetLi2.Apttus_Config2__IsPrimaryLine__c = TRUE;
        assetLi2.APTS_Original_Agreement__c = aggList[1].Id;
        assetLis.add(assetLi2);

        insert assetLis;
        
        Apttus__APTS_Related_Agreement__c relatedAgg = new Apttus__APTS_Related_Agreement__c();
        relatedAgg.Apttus__APTS_Contract_From__c = aggList[0].Id;
        relatedAgg.Apttus__APTS_Contract_To__c = aggList[1].Id;
        insert relatedAgg;
        
        //Start Test
        Test.startTest();
        APTS_UpdateAssetLIStatusAction.UpdateAssetLIStatus(new list<id>{assetLis[0].id,assetLis[1].id,assetLis[2].id});
        //Stop Test
        Test.stopTest();
                
    }

    @isTest
    static void updateAssetLIModificationAssignmentTest(){
        Id recordTypeIdNJUS = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('NJUS New Card Sale').getRecordTypeId();
        Id recordTypeIdNJE = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('NJE New Card Sale').getRecordTypeId();

        Account accToInsert = [SELECT Id, Name, APTS_Has_Active_NJUS_Agreement__c, APTS_Has_Active_NJE_Agreement__c 
                              FROM Account WHERE Name = 'APTS Test Account' LIMIT 1];
        
        Opportunity testOpportunity = [SELECT Id FROM Opportunity WHERE Name = 'test Apttus Opportunity' LIMIT 1];

        Apttus_Config2__PriceList__c testPriceList = APTS_CPQTestUtility.createPriceList('test Apttus Price List', true);
        insert testPriceList;
		List<Apttus_Proposal__Proposal__c> proposalList = new List<Apttus_Proposal__Proposal__c>();
        
        Apttus_Proposal__Proposal__c testProposal = APTS_CPQTestUtility.createProposal('test Apttus Proposal', accToInsert.Id, testOpportunity.Id, 'Proposal', testPriceList.Id);
        //populate custom fields value if required
        testProposal.APTS_New_Sale_or_Modification__c = 'New Sale';
        testProposal.APTS_Modification_Type__c = 'In Modification';
        proposalList.add(testProposal);

        Apttus_Proposal__Proposal__c testProposal1 = APTS_CPQTestUtility.createProposal('test Apttus Proposal', accToInsert.Id, testOpportunity.Id, 'Proposal', testPriceList.Id);
        //populate custom fields value if required
        testProposal1.APTS_New_Sale_or_Modification__c = 'Modification';
        testProposal1.APTS_Modification_Type__c = 'Assignment';
        proposalList.add(testProposal1);
        
        insert proposalList;
        
       
        ID accID = accToInsert.Id;
        
        List<Apttus__APTS_Agreement__c> aggList = new List<Apttus__APTS_Agreement__c>();
        
        //New Sale      
        Apttus__APTS_Agreement__c newSaleAgg = new Apttus__APTS_Agreement__c();
        newSaleAgg.Name = 'APTS Test Agreement 1111';
        newSaleAgg.Program__c = 'NetJets U.S.';
        newSaleAgg.Apttus__Account__c = accToInsert.Id;
        newSaleAgg.Card_Number__c = String.valueof((Math.random() * 100));
        newSaleAgg.APTS_Modification_Type__c = 'New Sale';
        newSaleAgg.RecordTypeId = recordTypeIdNJUS;
        newSaleAgg.Apttus_QPComply__RelatedProposalId__c = testProposal.Id;
        newSaleAgg.Agreement_Status__c = 'In Modification';
        aggList.add(newSaleAgg);
        
          
        //Assignment
        Apttus__APTS_Agreement__c  assignmentAgg = new Apttus__APTS_Agreement__c();
        assignmentAgg.Name = 'APTS Test Agreement 1111';
        assignmentAgg.Program__c = 'NetJets U.S.';
        assignmentAgg.Apttus__Account__c = accID;
        assignmentAgg.Card_Number__c = String.valueof((Math.random() * 100));
        assignmentAgg.APTS_Modification_Type__c = 'Assignment';
        assignmentAgg.RecordTypeId = recordTypeIdNJUS;
        assignmentAgg.Apttus_QPComply__RelatedProposalId__c = testProposal1.Id;
        aggList.add(assignmentAgg);
        
        insert aggList;
        
        List<id> assetLIIds = new list<id>();
        
        //Create Asset Line Item for New Card Sale Conversion       
        List<Apttus_Config2__AssetLineItem__c> assetLIList = new List<Apttus_Config2__AssetLineItem__c>();
        Apttus_Config2__AssetLineItem__c  NCSCAseetLI = new Apttus_Config2__AssetLineItem__c ();
        NCSCAseetLI.Apttus_CMConfig__AgreementId__c = aggList[1].Id;
        NCSCAseetLI.Apttus_Config2__ChargeType__c = 'Purchase Price';
        NCSCAseetLI.Apttus_Config2__LineType__c = 'Product/Service';
        NCSCAseetLI.Apttus_Config2__IsPrimaryLine__c = TRUE;
        NCSCAseetLI.APTS_Original_Agreement__c = aggList[0].Id;
        assetLIList.add(NCSCAseetLI);
        
  
        Apttus_Config2__AssetLineItem__c  AEALI1 = new Apttus_Config2__AssetLineItem__c ();
        AEALI1.Apttus_CMConfig__AgreementId__c = aggList[1].Id;
        AEALI1.Apttus_Config2__ChargeType__c = 'Purchase Price';
        AEALI1.Apttus_Config2__LineType__c = 'Option';
        AEALI1.Apttus_Config2__IsPrimaryLine__c = TRUE;
        AEALI1.APTS_Original_Agreement__c = aggList[0].Id;
        assetLIList.add(AEALI1);
        insert assetLIList;
        
        //Create Asset Line Item Attribute for New Card Sale Conversion
        Apttus_Config2__AssetAttributeValue__c NCSCAseetAV = new Apttus_Config2__AssetAttributeValue__c();
        NCSCAseetAV.Apttus_Config2__AssetLineItemId__c = assetLIList[0].Id;
        NCSCAseetAV.Aircraft_Remaining_Hours__c = 20;
        NCSCAseetAV.APTS_Override_Requested_Hours__c = 5;
        NCSCAseetAV.Requested_Hours__c = 10;
        insert NCSCAseetAV;
        NCSCAseetLI.Apttus_Config2__AttributeValueId__c = NCSCAseetAV.id;
        update NCSCAseetLI;
        Apttus_Config2__AssetAttributeValue__c NCSCAseetAV1 = new Apttus_Config2__AssetAttributeValue__c();
        NCSCAseetAV1.Apttus_Config2__AssetLineItemId__c = assetLIList[1].Id;
        NCSCAseetAV1.Aircraft_Remaining_Hours__c = 20;
        NCSCAseetAV1.Requested_Hours__c = 25;
        insert NCSCAseetAV1;

        
        assetLIIds.add(assetLIList[0].Id);
        assetLIIds.add(assetLIList[1].Id);
            
        //Start Test
        Test.startTest();
        APTS_UpdateAssetLIStatusAction.UpdateAssetLIStatus(assetLIIds);
        //Stop Test
        Test.stopTest();
                
    }

    @IsTest
    static void updateAssetLIModificationEHCTest(){
        List<Account> accList = new List<Account>();
        Id recordTypeIdNJUS = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('NJUS New Card Sale').getRecordTypeId();
       
        Account accToInsert = new Account();
        accToInsert.Name = 'APTS Test Account';
        accToInsert.APTS_Has_Active_NJUS_Agreement__c = true;
        accToInsert.APTS_Has_Active_NJE_Agreement__c = true;
        insert accToInsert;
  
        Opportunity testOpportunity = APTS_CPQTestUtility.createOpportunity('test Apttus Opportunity', 'New Customer', accToInsert.Id, 'Propose');
        insert testOpportunity;
  
        Apttus_Config2__PriceList__c testPriceList = APTS_CPQTestUtility.createPriceList('test Apttus Price List', true);
        insert testPriceList;
  
        Apttus_Proposal__Proposal__c testProposal = APTS_CPQTestUtility.createProposal('test Apttus Proposal', accToInsert.Id, testOpportunity.Id, 'Proposal', testPriceList.Id);
        //populate custom fields value if required
        testProposal.APTS_New_Sale_or_Modification__c = 'Modification';
        testProposal.APTS_Modification_Type__c = 'Expired Hour Card';
        insert testProposal;
        
              
        ID accID = accToInsert.Id;
        
         //New Sale      
        Apttus__APTS_Agreement__c newSaleAgg = new Apttus__APTS_Agreement__c();
        newSaleAgg.Name = 'APTS Test Agreement 1111';
        newSaleAgg.Program__c = 'NetJets U.S.';
        newSaleAgg.Apttus__Account__c = accToInsert.Id;
        newSaleAgg.Card_Number__c = String.valueof((Math.random() * 100));
        newSaleAgg.APTS_Modification_Type__c = 'Expired Hour Card';
        newSaleAgg.RecordTypeId = recordTypeIdNJUS;
        newSaleAgg.Apttus_QPComply__RelatedProposalId__c = testProposal.Id;
        newSaleAgg.Agreement_Status__c = 'Active';
      
        insert newSaleAgg;    
        
        
        
        List<Apttus_Config2__AssetLineItem__c> assetLIIds = new list<Apttus_Config2__AssetLineItem__c>();
        
        //Create Asset Line Item for New Card Sale Conversion       
        
        Apttus_Config2__AssetLineItem__c  NCSCAseetLI = new Apttus_Config2__AssetLineItem__c ();
        NCSCAseetLI.Apttus_CMConfig__AgreementId__c = newSaleAgg.Id;
        NCSCAseetLI.Apttus_Config2__ChargeType__c = 'Expired Hours Value';
        NCSCAseetLI.Apttus_Config2__LineType__c = 'Option';
        NCSCAseetLI.APTS_Original_Agreement__c = newSaleAgg.Id;
        insert NCSCAseetLI; 
       
        Apttus_Config2__AssetAttributeValue__c termAssetAttri = new Apttus_Config2__AssetAttributeValue__c();
        termAssetAttri.Apttus_Config2__AssetLineItemId__c = NCSCAseetLI.Id;
        termAssetAttri.APTPS_Is_Expired_Hours_Reduced__c = false;
        termAssetAttri.APTS_Aircraft_Expired_Hours__c = 25;
        termAssetAttri.APTS_Overridden_Hours_To_Unexpire__c = 10;
        termAssetAttri.APTS_Hours_To_Unexpire__c = 0;
        termAssetAttri.APTS_Override_Hours_To_Unexpire__c = true;
        termAssetAttri.APTPS_Stop_Integration_Updates__c = false;
        insert termAssetAttri;
        
        NCSCAseetLI.Apttus_Config2__AttributeValueId__c = termAssetAttri.Id;
        update NCSCAseetLI;
  
        //assetLIIds.add(NCSCAseetLI);
        
        //Start Test
        Test.startTest();
        APTS_UpdateAssetLIStatusAction.UpdateAssetLIStatus(new list<id>{NCSCAseetLI.id});
        //Stop Test
        Test.stopTest();
                
    }
        
}