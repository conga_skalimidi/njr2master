/*******************************************************************************************************************************************
@Name: APTS_AgreementDeletionBatch
@Author: Avinash Bamane
@CreateDate: 05/03/2021
@Description: GCM-9961: Any agreement that is past 90 days after last modified date and has not been activated will need to be deleted.


Modification Log
 * ------------------------------------------------------------------------------------------------------------------------------------------------------------
 * Developer               Date           US/Defect        Description
 * ------------------------------------------------------------------------------------------------------------------------------------------------------------
 * Venu Bairoju           09/15/21        GCM-9961          Updated
*******************************************************************************************************************************************/

public with sharing class APTS_AgreementDeletionBatch implements Database.Batchable<sObject>, Database.Stateful {
    // Read batch related Custom settings.
    static APTPS_Agreement_Deletion_Batch__c batchDetails = APTPS_Agreement_Deletion_Batch__c.getOrgDefaults();
    static String numberOfDays = !String.isEmpty(batchDetails.Number_of_days__c) ? batchDetails.Number_of_days__c : 'LAST_N_DAYS:260';
    //static String numberOfDays = 'LAST_N_DAYS:225';
    static String agStatus = !String.isEmpty(batchDetails.Agreement_Status__c) ? batchDetails.Agreement_Status__c : '\'Contract Generated\', \'Pending Funding\', \'Contract Pending\', \'Draft\'';
    static String recTypes = '\'Referrer\', \'Referee\'';
    static string agDelStatus = !string.isEmpty(batchDetails.APTS_Do_Not_Delete__c) ? batchDetails.APTS_Do_Not_Delete__c : 'false';
    //static string agProposalDelStatus = !string.isEmpty(batchDetails.APTS_Proposal_Do_Not_Delete__c) ? batchDetails.APTS_Proposal_Do_Not_Delete__c : 'false';
    static string agProgram = !string.isEmpty(batchDetails.Program__c) ? batchDetails.Program__c : 'NetJets U.S.';
    List<Apttus__APTS_Agreement__c> globalAgreementList = new List<Apttus__APTS_Agreement__c>();
    //static String proposalStatus = !String.isEmpty(batchDetails.Proposal_Status__c) ? batchDetails.Proposal_Status__c : 'Cancelled';
    //static String proposalComment = !String.isEmpty(batchDetails.Proposal_Comment__c) ? batchDetails.Proposal_Comment__c : '';
    
    public Database.QueryLocator start(Database.BatchableContext BC) {
        system.debug('SOQL query parameters are, numberOfDays --> '+numberOfDays+' agStatus --> '+agStatus+' agDelStatus --> '+agDelStatus+'');
        String agQuery = 'SELECT Id,Name,Apttus__Related_Opportunity__c,Program__c,Primary_Sales_Executive__r.Email,Account_Executive__r.Email,RVP__r.Email,APTS_Associate_Sales_Executive__r.Email,Sales_Consultant__r.Email, Agreement_Status__c, APTS_Path_Chevron_Status__c, Apttus_QPComply__RelatedProposalId__c, Apttus_QPComply__RelatedProposalId__r.Proposal_Chevron_Status__c, APTS_Modification_Type__c, APTS_New_Sale_or_Modification__c,APTS_Referee_Agreement__c,APTS_Referrer_Agreement__c,APTS_Referrer_Referee_Agreement__c,APTS_Referrer_Agreement__r.Agreement_Status__c,APTS_Referee_Agreement__r.Agreement_Status__c FROM Apttus__APTS_Agreement__c WHERE CreatedDate < '+numberOfDays+ ' AND Program__c ='+agProgram+' AND Agreement_Status__c IN ('+agStatus+') AND RecordType.name NOT IN ('+recTypes +') AND APTS_Do_Not_Delete__c ='+agDelStatus ; 
        //String agQuery = 'SELECT Id,CreatedDate, Created_Date__c, Name,Apttus__Related_Opportunity__c,Program__c,Primary_Sales_Executive__r.Email,Account_Executive__r.Email,RVP__r.Email,APTS_Associate_Sales_Executive__r.Email,Sales_Consultant__r.Email, Agreement_Status__c, APTS_Path_Chevron_Status__c, Apttus_QPComply__RelatedProposalId__c, Apttus_QPComply__RelatedProposalId__r.Proposal_Chevron_Status__c, APTS_Modification_Type__c, APTS_New_Sale_or_Modification__c,APTS_Referee_Agreement__c, APTS_Referrer_Agreement__c,APTS_Referrer_Referee_Agreement__c,APTS_Referrer_Agreement__r.Agreement_Status__c,APTS_Referee_Agreement__r.Agreement_Status__c FROM Apttus__APTS_Agreement__c WHERE Program__c ='+agProgram+' AND Agreement_Status__c IN ('+agStatus+') AND RecordType.name NOT IN ('+recTypes +') AND APTS_Do_Not_Delete__c ='+agDelStatus+' AND  Apttus_QPComply__RelatedProposalId__r.APTS_Do_Not_Delete__c='+agProposalDelStatus+' AND Created_Date__c=\'2/25/2021\'';
        system.debug('APTS_AgreementDeletionBatch SOQL query is --> '+agQuery+'');
        return Database.getQueryLocator(agQuery);
    }
    
    public void execute(Database.BatchableContext BC, List<Apttus__APTS_Agreement__c> agList) {
        try{
            system.debug('Total Number of agreements to process --> '+agList.size());
            Set<Id> proposalIdSet = new Set<Id>();
            Set<Id> ehcProp = new Set<Id>();
            Set<Id> otherModProp = new Set<Id>();
            for(Apttus__APTS_Agreement__c ag : agList) {
                if(ag.Apttus_QPComply__RelatedProposalId__c != null)
                    proposalIdSet.add(ag.Apttus_QPComply__RelatedProposalId__c);
                //Sort out EHC and Other Modification Proposals
                if(ag.APTS_Modification_Type__c != null 
                   && APTS_Constants.EHC.equalsIgnoreCase(ag.APTS_Modification_Type__c))
                    ehcProp.add(ag.Apttus_QPComply__RelatedProposalId__c);
                else if(ag.APTS_New_Sale_or_Modification__c != null 
                        && APTS_Constants.MODIFICATION.equalsIgnoreCase(ag.APTS_New_Sale_or_Modification__c) 
                        && !String.isEmpty(ag.APTS_Modification_Type__c))
                    otherModProp.add(ag.Apttus_QPComply__RelatedProposalId__c);
            }
            
            system.debug('Number of Quote/Proposals to be updated --> '+proposalIdSet.size());
            system.debug('Number of EHC Proposals --> '+ehcProp.size()+' Data --> '+ehcProp);
            system.debug('Number of Proposals except EHC --> '+otherModProp.size()+' Data --> '+otherModProp);
            
            //Reuse Helper method to reset Parent Agreement Status from In Modification to Active or Inactive
            List<Apttus__APTS_Agreement__c> updateInModAgList = APTS_QuoteProposalBaseTriggerHelper.resetParentAgreementStatus(proposalIdSet, ehcProp, otherModProp);
            
            system.debug('Total number of In Modification agreements to update --> '+updateInModAgList.size());
            system.debug('Total number of agreements to delete --> '+agList.size());
            system.debug('agreements to delete --> '+agList);
            
            if(!proposalIdSet.isEmpty())
                System.enqueueJob(new APTS_AgreementDeleteQueueable(proposalIdSet));
            
            if(!updateInModAgList.isEmpty())
                update updateInModAgList;
            //---------------------------------------------------------------------------------------------
            Map<Id,Apttus__APTS_Agreement__c> agreementMap = new Map<Id,Apttus__APTS_Agreement__c>(agList);
            
            Set<Id> referAgreementIdSet = new Set<Id>();
            for(Apttus__APTS_Agreement__c agre : agreementMap.values()) {
                if(agre.APTS_Referrer_Agreement__c != null && agre.APTS_Referrer_Agreement__r.Agreement_Status__c != 'On Hold') {
                    agreementMap.remove(agre.Id);
                } else if(agre.APTS_Referee_Agreement__c != null && agre.APTS_Referee_Agreement__r.Agreement_Status__c != 'On Hold') {
                    agreementMap.remove(agre.Id);
                }
                
                if(agre.APTS_Referrer_Agreement__c != null)
                    referAgreementIdSet.add(agre.APTS_Referrer_Agreement__c);
                
                if(agre.APTS_Referee_Agreement__c != null)
                    referAgreementIdSet.add(agre.APTS_Referee_Agreement__c);
                
            }
            system.debug('referAgreementIdSet --> '+referAgreementIdSet);
			system.debug('agreements to delete 2--> '+agreementMap);
            for(Apttus__APTS_Agreement__c referAgree : [SELECT Id,Name,Apttus__Related_Opportunity__c,Program__c,Primary_Sales_Executive__r.Email,Account_Executive__r.Email,RVP__r.Email,APTS_Associate_Sales_Executive__r.Email,Sales_Consultant__r.Email,APTS_Referrer_Referee_Agreement__c,Agreement_Status__c
                                                        FROM Apttus__APTS_Agreement__c 
                                                        WHERE Id IN :referAgreementIdSet AND Agreement_Status__c = 'On Hold']) {
                agreementMap.put(referAgree.Id,referAgree);
            }
            
            system.debug('agreements to delete 3--> '+agreementMap);
            
            globalAgreementList.addAll(agreementMap.values());
            
            //----------------------------------------------------------------------------------------------
            if(!agList.isEmpty())
                delete agreementMap.values();
        } catch(DMLException e){
            system.debug('===ERROR: APTS_AgreementDeletionBatch==='+e.getStackTraceString());
        }
    }
    
    public void finish(Database.BatchableContext BC) {
        system.debug('===FINISH=== APTS_AgreementDeletionBatch Batch execution has been completed');
        System.debug('globalAgreementList=====>'+globalAgreementList);
        List<Opportunity> opptyList = new List<Opportunity>();
        Set<Id> opptyId = new Set<Id>();
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        for(Apttus__APTS_Agreement__c agrre : globalAgreementList) {
            
            if(!opptyId.contains(agrre.Apttus__Related_Opportunity__c)) {
               Opportunity op = new Opportunity();
                op.Id = agrre.Apttus__Related_Opportunity__c;
                op.StageName = 'Proposal';
                opptyId.add(agrre.Apttus__Related_Opportunity__c);
                opptyList.add(op);  
            }
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            List<String> sendTo = new List<String>();
            sendTo.add('vbairoju@netjets.com'); // for testing purpose added. can remove after testing.
            
            if(agrre.Primary_Sales_Executive__c != null) {
                sendTo.add(agrre.Primary_Sales_Executive__r.Email);
            }
            if(agrre.Account_Executive__c != null) {
                sendTo.add(agrre.Account_Executive__r.Email);
            }
            if(agrre.RVP__c != null) {
                sendTo.add(agrre.RVP__r.Email);
            }
            if(agrre.APTS_Associate_Sales_Executive__c != null) {
                sendTo.add(agrre.APTS_Associate_Sales_Executive__r.Email);
            }
            if(agrre.Sales_Consultant__c != null) {
                sendTo.add(agrre.Sales_Consultant__r.Email);
            }
            mail.setToAddresses(sendTo);
            
            mail.setReplyTo('vbairoju@netjets.com');
            mail.setSenderDisplayName('Venu Bairoju');
            
            
            // Step 4. Set email contents - you can use variables!
            mail.setSubject(agrre.name +' has been canceled');
            String body = 'This email is notification that your ' +agrre.name ;
            body += ' and proposal on ' + system.URL.getSalesforceBaseUrl().toExternalForm() + '/' +agrre.Apttus__Related_Opportunity__c ; 
            body += ' has been canceled as the terms of the Agreement have expired and are now void.';
            body += ' Please notify your Prospect/Owner and if they are still interested,';
            body += ' please create and submit a new proposal through Apttus and a new Agreement will be sent via DocuSign.';
            mail.setHtmlBody(body);
            
            mails.add(mail);
        }
        Messaging.sendEmail(mails);
        if(!opptyList.isEmpty() && !Test.isRunningTest())
            update opptyList;
        
    }
}