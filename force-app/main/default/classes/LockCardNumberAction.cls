/*
 * Created by tfoor on 6/11/2018.
 */
global class LockCardNumberAction {

     public class LockCardNumberRequest{
      @InvocableVariable(required=true)
      public Id agreementId;
    }
    @InvocableMethod(label='Locks card number to prevent duplication' description='Locks card number to prevent duplication')
    public static void setCardNumber(LockCardNumberRequest[] lockCardNumberRequests) {
        integer i = 0;
        while(i<5){
            try{
              Card_Number__c cardNum = [SELECT Next_Card_Number__c FROM Card_Number__c LIMIT 1 FOR UPDATE];
              system.debug('cardNum -->'+i+'--'+cardNum);
              
            } catch(exception ex){
                system.debug('Exception at LockCardNumberAction-->'+ex);
                 system.debug('i -->'+i);
               i++;
            }
            i=5;
        }
        
    }
}