public with sharing class APTS_ProposalDeletionBatch implements Database.Batchable<sObject>, Database.Stateful {
    // Read batch related Custom settings.
    static APTPS_Proposal_Deletion_Batch__c batchDetails = APTPS_Proposal_Deletion_Batch__c.getOrgDefaults();
    static String numberOfDays = !String.isEmpty(batchDetails.Number_of_days__c) ? batchDetails.Number_of_days__c : 'LAST_N_DAYS:30';
    static string agDelStatus = !string.isEmpty(batchDetails.APTS_Do_Not_Delete__c) ? batchDetails.APTS_Do_Not_Delete__c : 'false';
    static string agAgreementDelStatus = !string.isEmpty(batchDetails.APTS_Agreement_Do_Not_Delete__c) ? batchDetails.APTS_Agreement_Do_Not_Delete__c : 'false';
    static string agProgram = !string.isEmpty(batchDetails.Program__c) ? batchDetails.Program__c : 'NetJets U.S.';
    
    List<Apttus_Proposal__Proposal__c> globalProposalList = new List<Apttus_Proposal__Proposal__c>();
    public Database.QueryLocator start(Database.BatchableContext BCP) {
        system.debug('SOQL query parameters are, numberOfDays --> '+numberOfDays+' prStatus');
        String prQuery = 'SELECT Id,Apttus_QPComply__MasterAgreementId__c,APTS_Modification_Type__c,APTS_New_Sale_or_Modification__c FROM Apttus_Proposal__Proposal__c WHERE CreatedDate < '+numberOfDays+ ' AND Program__c ='+agProgram+' AND APTS_Do_Not_Delete__c ='+agDelStatus+' and Apttus_QPComply__MasterAgreementId__r.APTS_Do_Not_Delete__c ='+agAgreementDelStatus;
        system.debug('APTS_ProposalDeletionBatch SOQL query is --> '+prQuery);
        return Database.getQueryLocator(prQuery);
    }
    
    public void execute(Database.BatchableContext BCP, List<Apttus_Proposal__Proposal__c> prList) {
        try{
            system.debug('Total Number of Proposals to process --> '+prList.size());
            Set<Id> proposalIdSet1 = new Set<Id>();
            Set<Id> ehcProp1 = new Set<Id>();
            Set<Id> otherModProp1 = new Set<Id>();

            for(Apttus_Proposal__Proposal__c pr : prList) {
                proposalIdSet1.add(pr.Apttus_QPComply__MasterAgreementId__c);
                //Sort out EHC and Other Modification Proposals
                if(pr.APTS_Modification_Type__c != null 
                   && APTS_Constants.EHC.equalsIgnoreCase(pr.APTS_Modification_Type__c))
                    ehcProp1.add(pr.Apttus_QPComply__MasterAgreementId__c);
                else if(pr.APTS_New_Sale_or_Modification__c != null 
                        && APTS_Constants.MODIFICATION.equalsIgnoreCase(pr.APTS_New_Sale_or_Modification__c) 
                        && !String.isEmpty(pr.APTS_Modification_Type__c))
                    otherModProp1.add(pr.Apttus_QPComply__MasterAgreementId__c);
            }
            
            system.debug('Number of Quote/Proposals to be updated --> '+proposalIdSet1.size());
            system.debug('Number of EHC Proposals --> '+ehcProp1.size()+' Data --> '+ehcProp1);
            system.debug('Number of Proposals except EHC --> '+otherModProp1.size()+' Data --> '+otherModProp1);

            if(!prList.isEmpty())
                delete prList;
        } catch(DMLException e){
            system.debug('===ERROR: APTS_ProposalDeletionBatch==='+e.getStackTraceString());
        }
    }
    
    public void finish(Database.BatchableContext BCP) {
        system.debug('===FINISH=== APTS_ProposalDeletionBatch Batch execution has been completed');
        system.debug('globalProposalList=====>'+globalProposalList);
         List<Opportunity> opptyList = new List<Opportunity>();
        for(Apttus_Proposal__Proposal__c prop : globalProposalList) {
            
            Opportunity op = new Opportunity();
            op.Id = prop.Apttus_Proposal__Opportunity__c;
            op.StageName = 'Proposal';
            opptyList.add(op);
    }
        if(!opptyList.isEmpty())
            update opptyList;
}
}