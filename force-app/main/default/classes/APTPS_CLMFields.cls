/************************************************************************************************************************
@Name: APTPS_CLMFields
@Author: Conga PS Dev Team
@CreateDate: 28 May 2021
@Description: 
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/

public class APTPS_CLMFields implements APTPS_CLMFieldsCallable {
    private static String dsMetadataName = 'APTPS_CLMFields_Registry__mdt';
    public static Map<String, String> prodCodeClassNameMap = new Map<String, String>();
    
    /** 
    @description: Find out the APEX class implementation for the given productCode
    @param: Product Code
    @return: APEX Class name
    */
    private String findCLMFields_Implementation(String productCode) {
        String className = null;
        if(productCode != null && !prodCodeClassNameMap.isEmpty() 
           && prodCodeClassNameMap.containsKey(productCode)) {
               className = prodCodeClassNameMap.get(productCode);
           }
        system.debug('APEX Class Details --> '+className);
        return className;
    }
    
    /** 
    @description: Execute product specific Deal Summary logic.
    @param: Product code and arguments
    @return: 
    */
    public Object call(String productCode, Map<String, Object> args) {
        system.debug('Product Code to update deal summary --> '+productCode);
        system.debug('Arguments to call method --> '+args);
        String retMsg = null;
        if(args == null || args.get('agreementList') == null) 
            throw new ExtensionMalformedCallException('Agreement data is missing.');
        
        if(!String.isEmpty(productCode)) {
            if(prodCodeClassNameMap.isEmpty()) {
                setProdCodeClassNameMap();
            }
            
            String className = findCLMFields_Implementation(productCode);
            if(className != null) {
                APTPS_CLMFieldsInterface dsObj = (APTPS_CLMFieldsInterface)Type.forName(className).newInstance();
                dsObj.populateCLMFields(args);
                retMsg = 'SUCCESS';
            }
        }
        return retMsg;
    }
    
    /** 
    @description: Custom Exception implementation
    @param:
    @return: 
    */
    public class ExtensionMalformedCallException extends Exception {
        public String errMsg = '';
        public void ExtensionMalformedCallException(String errMsg) {
            this.errMsg = errMsg;
        }
    }
    
    /** 
    @description: Read CLM Fields Registry metadata and prepare the Map
    @param:
    @return: 
    */
    private void setProdCodeClassNameMap() {
        List<APTPS_CLMFields_Registry__mdt> fieldsSummaryHandlers = [SELECT Product_Code__c, Class_Name__c FROM APTPS_CLMFields_Registry__mdt];
        for(APTPS_CLMFields_Registry__mdt handler : fieldsSummaryHandlers) {
            if(handler.Product_Code__c != null && handler.Class_Name__c != null) 
                prodCodeClassNameMap.put(handler.Product_Code__c, handler.Class_Name__c);
        }
    }
    
}