/************************************************************************************************************************
@Name: APTPS_ApprovalsTest
@Author: Conga PS Dev Team
@CreateDate: 28 June 2021
@Description: Test class coverage for Approvals implementation
************************************************************************************************************************/
@isTest
public class APTPS_ApprovalsTest {
    @testsetup
    public static void makeData() {
        Test.startTest();
        //Create SNL Custom settings
        APTPS_SNL_Products__c snlProducts = new APTPS_SNL_Products__c();
        snlProducts.Products__c = 'Demo,Corporate Trial';
        insert snlProducts;
        
        Account accToInsert = new Account();
        accToInsert.Name = 'APTS Test Account';
        insert accToInsert;

        Service_Account__c serviceAcc = new Service_Account__c();
        serviceAcc.Account__c = accToInsert.Id;
        serviceAcc.NetJets_Company__c = 'NJA';
        serviceAcc.Status__c = 'Active';
        insert serviceAcc;

        Apttus_Config2__AccountLocation__c testAccLocation = APTS_CPQTestUtility.createAccountLocation('Loc1', accToInsert.Id);
        insert testAccLocation;
        
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator' LIMIT 1];
        USer newUser = new User(lastname = 'Test',Alias = 'Test',TimeZoneSidKey = 'GMT',
                                LocaleSidKey = 'eu_ES',EmailEncodingKey = 'ISO-8859-1',
                                ProfileId = p.id,LanguageLocaleKey = 'en_US',
                                userName='Test@sdkksddhfhdsjhf.com',email='Test@asjgdddgsad.com');
        insert newUser;
        
        List<AccountTeamMember> acctTeamMembersList = new List<AccountTeamMember>();
        AccountTeamMember acctTeamMember = new AccountTeamMember();
        acctTeamMember.UserId = newUser.Id;
        acctTeamMember.TeamMemberRole = APTS_Constants.SALES_EXEC_PRIMARY;
        acctTeamMember.AccountId  = accToInsert.Id;
        acctTeamMember.NetJets_Company__c = 'NJA';
        acctTeamMembersList.add(acctTeamMember);
        AccountTeamMember acctTeamMember1 = new AccountTeamMember();
        acctTeamMember1.UserId = newUser.Id;
        acctTeamMember1.TeamMemberRole = APTS_Constants.SALES_EXECUTIVE;
        acctTeamMember1.AccountId  = accToInsert.Id;
        acctTeamMember1.NetJets_Company__c = 'NJA';
        acctTeamMembersList.add(acctTeamMember1);
        AccountTeamMember acctTeamMember2 = new AccountTeamMember();
        acctTeamMember2.UserId = newUser.Id;
        acctTeamMember2.TeamMemberRole = APTS_Constants.ACCOUNT_EXEC_PRIMARY;
        acctTeamMember2.AccountId  = accToInsert.Id;
        acctTeamMember2.NetJets_Company__c = 'NJE';
        acctTeamMembersList.add(acctTeamMember2);
        
        AccountTeamMember acctTeamMember3 = new AccountTeamMember();
        acctTeamMember3.UserId = newUser.Id;
        acctTeamMember3.TeamMemberRole = APTS_Constants.REGIONAL_VP;
        acctTeamMember3.AccountId  = accToInsert.Id;
        acctTeamMember3.NetJets_Company__c = 'NJE';
        acctTeamMembersList.add(acctTeamMember3);
        
        AccountTeamMember acctTeamMember4 = new AccountTeamMember();
        acctTeamMember4.UserId = newUser.Id;
        acctTeamMember4.TeamMemberRole = APTS_Constants.ASSOC_SALES_EXECUTIVE ;
        acctTeamMember4.AccountId  = accToInsert.Id;
        acctTeamMember4.NetJets_Company__c = 'NJE';
        acctTeamMembersList.add(acctTeamMember4);
        
        
        
        insert acctTeamMembersList;
        
        
        Opportunity testOpportunity = APTS_CPQTestUtility.createOpportunity('test Apttus Opportunity', 'New Customer', accToInsert.Id, 'Propose');
        insert testOpportunity;
        
        Apttus_Config2__PriceList__c testPriceList = APTS_CPQTestUtility.createPriceList('test Apttus Price List', true);
        insert testPriceList;
        
        //Create Products
        List<Product2> prodList = new List<Product2>();
        Product2 NJUS_DEMO = APTS_CPQTestUtility.createProduct('Demo', 'NJUS_DEMO', 'Apttus', 'Bundle', true, true, true, true);
        prodList.add(NJUS_DEMO);
        
        Product2 NJE_DEMO = APTS_CPQTestUtility.createProduct('Demo', 'NJE_DEMO', 'Apttus', 'Bundle', true, true, true, true);
        prodList.add(NJE_DEMO);
        
        Product2 NJE_CORP = APTS_CPQTestUtility.createProduct('Corporate Trial', 'NJE_Corporate_Trial', 'Apttus', 'Standalone', true, true, true, true);
        prodList.add(NJE_CORP);

        insert prodList;
        
        //Create Quote/Proposals
        List<Apttus_Proposal__Proposal__c> proposalList = new List<Apttus_Proposal__Proposal__c>();
        Apttus_Proposal__Proposal__c demoNJUSQuote = APTS_CPQTestUtility.createProposal('DEMO NJUS Quote', accToInsert.Id, testOpportunity.Id, 'Proposal', testPriceList.Id);
        demoNJUSQuote.APTS_New_Sale_or_Modification__c = APTS_Constants.NEW_SALE;
        demoNJUSQuote.APTPS_Program_Type__c = 'Demo';
        demoNJUSQuote.CurrencyIsoCode = 'USD';
        proposalList.add(demoNJUSQuote);
        
        Apttus_Proposal__Proposal__c demoNJEQuote = APTS_CPQTestUtility.createProposal('DEMO NJE Quote', accToInsert.Id, testOpportunity.Id, 'Proposal', testPriceList.Id);
        demoNJEQuote.APTS_New_Sale_or_Modification__c = APTS_Constants.NEW_SALE;
        demoNJEQuote.APTPS_Program_Type__c = 'Demo';
        //demoNJEQuote.CurrencyIsoCode = 'EUR';
        proposalList.add(demoNJEQuote);
        
        Apttus_Proposal__Proposal__c corpNJEQuote = APTS_CPQTestUtility.createProposal('Corporate NJE Quote', accToInsert.Id, testOpportunity.Id, 'Proposal', testPriceList.Id);
        corpNJEQuote.APTS_New_Sale_or_Modification__c = APTS_Constants.NEW_SALE;
        corpNJEQuote.APTPS_Program_Type__c = 'Corporate Trial';
        //corpNJEQuote.CurrencyIsoCode = 'EUR';
        proposalList.add(corpNJEQuote);

        
        insert proposalList;
        
        //Create Quote/Proposal Line Items
        List<Apttus_Proposal__Proposal_Line_Item__c> pliList = new List<Apttus_Proposal__Proposal_Line_Item__c>();
        Apttus_Proposal__Proposal_Line_Item__c demoUS = APTS_CPQTestUtility.getPropLI(NJUS_DEMO.Id, 'New', demoNJUSQuote.Id, '', null);
        demoUS.Apttus_QPConfig__LineType__c = 'Product/Service';
        demoUS.Apttus_QPConfig__IsPrimaryLine__c = TRUE;
        demoUS.Apttus_QPConfig__NetPrice__c = 123;
        pliList.add(demoUS);
        
        Apttus_Proposal__Proposal_Line_Item__c demoNJE = APTS_CPQTestUtility.getPropLI(NJE_DEMO.Id, 'New', demoNJEQuote.Id, '', null);
        demoNJE.Apttus_QPConfig__LineType__c = 'Product/Service';
        demoNJE.Apttus_QPConfig__IsPrimaryLine__c = TRUE;
        demoNJE.Apttus_QPConfig__NetPrice__c = 123;
        pliList.add(demoNJE);
        
        Apttus_Proposal__Proposal_Line_Item__c corporateNJE = APTS_CPQTestUtility.getPropLI(NJE_CORP.Id, 'New', corpNJEQuote.Id, '', null);
        corporateNJE.Apttus_QPConfig__LineType__c = 'Product/Service';
        corporateNJE.Apttus_QPConfig__IsPrimaryLine__c = TRUE;
        corporateNJE.Apttus_QPConfig__NetPrice__c = 100;
        pliList.add(corporateNJE);
        
        
        insert pliList;
        
        //Create Proposal Attribute Value
        List<Apttus_QPConfig__ProposalProductAttributeValue__c> pavList = new List<Apttus_QPConfig__ProposalProductAttributeValue__c>();
        Apttus_QPConfig__ProposalProductAttributeValue__c demoNJUSPav = new Apttus_QPConfig__ProposalProductAttributeValue__c();
        demoNJUSPav.Apttus_QPConfig__LineItemId__c = demoUS.Id;
        demoNJUSPav.APTPS_Type_of_Demo__c = 'Card';
        demoNJUSPav.APTPS_Demo_Aircraft__c = 'Citation XLS';
        demoNJUSPav.APTPS_Rate_Type__c = APTS_Constants.SPECIALRATE;
        demoNJUSPav.APTPS_Waive_Credit_Card_Authorization__c = true;
        demoNJUSPav.APTPS_Waive_Positioning_Fees__c = true;
        pavList.add(demoNJUSPav);
        
        Apttus_QPConfig__ProposalProductAttributeValue__c demoNJEPav = new Apttus_QPConfig__ProposalProductAttributeValue__c();
        demoNJEPav.Apttus_QPConfig__LineItemId__c = demoNJE.Id;
        demoNJEPav.APTPS_Type_of_Demo__c = 'Card';
        demoNJEPav.APTPS_Demo_Aircraft__c = 'Phenom 300';
        demoNJEPav.APTPS_NJE_Rate_Type__c = APTS_Constants.PREMIUM;
        demoNJEPav.APTPS_Waive_Positioning_Fees__c = true;
        pavList.add(demoNJEPav);
        
        Apttus_QPConfig__ProposalProductAttributeValue__c corporateNJEPav = new Apttus_QPConfig__ProposalProductAttributeValue__c();
        corporateNJEPav.Apttus_QPConfig__LineItemId__c = corporateNJE.Id;
        corporateNJEPav.APTPS_Corporate_Trial_Aircraft__c = 'Citation Latitude';
        corporateNJEPav.APTPS_Term_Months__c = String.valueOf(3);
        corporateNJEPav.APTPS_Term_End_Date__c = Date.today();
        corporateNJEPav.APTPS_Maximum_Number_of_Trials__c = 2.0;
        corporateNJEPav.APTPS_Override_Deposit__c = 100.00;
        pavList.add(corporateNJEPav);

        
        insert pavList;
        
        demoUS.Apttus_QPConfig__AttributeValueId__c = pavList[0].id;
        update demoUS;
        demoNJE.Apttus_QPConfig__AttributeValueId__c = pavList[1].id;
        update demoNJE; 

        Test.stopTest();
    }
    
    @isTest
    public static void testDealSummary() {
        Test.startTest();
        List<Apttus_Proposal__Proposal__c> quotesToUpdate = new List<Apttus_Proposal__Proposal__c>();
        Apttus_Proposal__Proposal__c demoNJUSQuote = [SELECT Id, Apttus_QPConfig__ConfigurationFinalizedDate__c FROM Apttus_Proposal__Proposal__c 
                                                      WHERE APTPS_Program_Type__c = 'Demo' AND CurrencyIsoCode = 'USD' LIMIT 1];
        demoNJUSQuote.Apttus_QPConfig__ConfigurationFinalizedDate__c = Date.today();
        quotesToUpdate.add(demoNJUSQuote);
        
        Apttus_Proposal__Proposal__c demoNJEQuote = [SELECT Id, Apttus_QPConfig__ConfigurationFinalizedDate__c FROM Apttus_Proposal__Proposal__c 
                                                     WHERE APTPS_Program_Type__c = 'Demo' AND Apttus_Proposal__Proposal_Name__c ='DEMO NJE Quote' LIMIT 1];
        demoNJEQuote.Apttus_QPConfig__ConfigurationFinalizedDate__c = Date.today();
        quotesToUpdate.add(demoNJEQuote);
        
        Apttus_Proposal__Proposal__c corporateNJEQuote = [SELECT Id, Apttus_QPConfig__ConfigurationFinalizedDate__c FROM Apttus_Proposal__Proposal__c 
                                                          WHERE APTPS_Program_Type__c = 'Corporate Trial' LIMIT 1];
        corporateNJEQuote.Apttus_QPConfig__ConfigurationFinalizedDate__c = Date.today();
        quotesToUpdate.add(corporateNJEQuote);

        
        update quotesToUpdate;
        APTPS_Approvals_Corporate ctApprovals = new APTPS_Approvals_Corporate();
        ctApprovals.checkSalesOpsApprovalRequired(null,null);
        ctApprovals.checkSalesDirectorApprovalRequired(null,null);
        ctApprovals.checkDemoTeamApprovalRequired(null,null);
        ctApprovals.checkDirectorARApprovalRequired(null,null);
        ctApprovals.checkRVPApprovalRequired(null,null);
        ctApprovals.checkSalesOpsTeamApprovalRequired(null,null);
        
        APTPS_Approvals_Demo demoApprovals = new APTPS_Approvals_Demo();
        demoApprovals.checkLegalApprovedRequired(null,null);

        
        Test.stopTest();
    }
}