/************************************************************************************************************************
@Name: APTS_MassUpdateLegalEntityControllerTest
@Author: Avinash Bamane
@CreateDate: 23 Oct 2020
@Description: Test Class for APTS_MassUpdateLegalEntityController
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/

@isTest
public class APTS_MassUpdateLegalEntityControllerTest {
    @isTest
    public static void testMassUpdateController() {
        Account acc = new Account();
        acc.Name = 'Test Account';
        insert acc;
        
        List<Apttus_Config2__AccountLocation__c> locList = new List<Apttus_Config2__AccountLocation__c>();
        for(Integer i=0; i < 5; i++) {
        	Apttus_Config2__AccountLocation__c accLoc = new Apttus_Config2__AccountLocation__c();
            accLoc.Name = 'Test '+i;
            accLoc.Apttus_Config2__Type__c = 'Individual';
            accLoc.Apttus_Config2__Street__c = 'Test';
            accLoc.Apttus_Config2__Country__c = 'USA';
            accLoc.Apttus_Config2__AccountId__c = acc.Id;
            locList.add(accLoc);
        }
        
        if(!locList.isEmpty())
            insert locList;
        
        Opportunity oppObj = new Opportunity();
        oppObj.Name = 'Test Opportunity';
        oppObj.Account = acc;
        oppObj.CloseDate = Date.today();
        oppObj.StageName = 'Proposal/Solution';
        insert oppObj;
        
        Apttus_Proposal__Proposal__c quote = new Apttus_Proposal__Proposal__c();
        quote.Apttus_Proposal__Account__c = acc.Id;
        quote.Apttus_Proposal__Opportunity__c = oppObj.Id;
        quote.APTS_New_Sale_or_Modification__c = 'Modification';
        quote.APTS_Modification_Type__c = 'Assignment';
        quote.APTS_Additional_Modification_Type__c = 'Intra-account assignment';
        insert quote;
        
        Apttus_Config2__ProductConfiguration__c config = new Apttus_Config2__ProductConfiguration__c();
        config.Name = 'Test';
        insert config;
        
        List<Apttus_Config2__LineItem__c> liToInsert = new List<Apttus_Config2__LineItem__c>();
        Apttus_Config2__AccountLocation__c assignedLoc = new Apttus_Config2__AccountLocation__c();
        for(Integer i=0; i<5; i++) {
            Apttus_Config2__LineItem__c li = new Apttus_Config2__LineItem__c();
            li.Apttus_Config2__ConfigurationId__c = config.Id;
            assignedLoc = locList[Integer.valueof((Math.random() * locList.size()))];
            li.Apttus_Config2__LocationId__c = assignedLoc.Id;
            li.Apttus_Config2__ItemSequence__c = i+1;
            li.Apttus_Config2__LineNumber__c = i+1; 
            liToInsert.add(li);
        }
        if(!liToInsert.isEmpty())
            insert liToInsert;
        
        PageReference pageRef = System.Page.Apttus_Config2__Cart;
        Test.setCurrentPage(pageRef);
        System.currentPageReference().getParameters().put('id', config.Id);
        System.currentPageReference().getParameters().put('businessObjectId', quote.id);
        
        test.startTest();
        ApexPages.StandardController sc = new ApexPages.StandardController(config);
        APTS_MassUpdateLegalEntityController controller = new APTS_MassUpdateLegalEntityController(sc);
        controller.updateLegalEntity();
        controller.returnToCartPage();
        Apttus_Config2__LineItem__c liResult = [SELECT Id, Apttus_Config2__LocationId__r.Id 
                                               FROM Apttus_Config2__LineItem__c 
                                               WHERE Apttus_Config2__ConfigurationId__c = :config.Id LIMIT 1];
        System.assertNotEquals(liResult.Id, assignedLoc.Id);
        test.stopTest();
    }
}