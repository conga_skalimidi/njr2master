/***********************************************************************************************************************
@Name: NJ_ProcessAssetLIQueuebleTest
@Author: Ramesh Kumar
@CreateDate: 03 Nov 2020
@Description: APEX class to test classes NJ_ProcessAssetLIQueuebleTest
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/
@isTest
public with sharing class NJ_ProcessAssetLIQueuebleTest {
    @isTest
    static void processCrossAccountAssetLITest(){
        List<Account> accList = new List<Account>();
        Id recordTypeIdNJUS = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('NJUS New Card Sale').getRecordTypeId();
        Id recordTypeIdNJE = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('NJE New Card Sale').getRecordTypeId();

        Account accToInsert = new Account();
        accToInsert.Name = 'APTS Test Account';
        accToInsert.APTS_Has_Active_NJUS_Agreement__c = true;
        accToInsert.APTS_Has_Active_NJE_Agreement__c = true;
        insert accToInsert;

        Opportunity testOpportunity = APTS_CPQTestUtility.createOpportunity('test Apttus Opportunity', 'New Customer', accToInsert.Id, 'Propose');
        insert testOpportunity;

        Apttus_Config2__PriceList__c testPriceList = APTS_CPQTestUtility.createPriceList('test Apttus Price List', true);
        insert testPriceList;

        Apttus_Proposal__Proposal__c testProposal = APTS_CPQTestUtility.createProposal('test Apttus Proposal', accToInsert.Id, testOpportunity.Id, 'Proposal', testPriceList.Id);
        //populate custom fields value if required
        testProposal.APTS_New_Sale_or_Modification__c = 'Modification';
        testProposal.APTS_Modification_Type__c = 'Termination';
        insert testProposal;            
        
        List<Apttus__APTS_Agreement__c> aggList = new List<Apttus__APTS_Agreement__c>();
        Apttus__APTS_Agreement__c aggToInsert = new Apttus__APTS_Agreement__c();
        aggToInsert.Name = 'APTS Test Agreement 1111';
        aggToInsert.Program__c = 'NetJets U.S.';
        aggToInsert.Apttus__Account__c = accToInsert.Id;
        aggToInsert.Card_Number__c = '123456789';
        aggToInsert.APTS_Modification_Type__c = 'Termination';
        aggToInsert.RecordTypeId = recordTypeIdNJUS;
        aggToInsert.Apttus_QPComply__RelatedProposalId__c = testProposal.Id;
        aggList.add(aggToInsert);

        Apttus_Config2__AssetLineItem__c  terminationAssetLi1 = new Apttus_Config2__AssetLineItem__c ();
        terminationAssetLi1.Apttus_CMConfig__AgreementId__c = aggList[0].Id;
        terminationAssetLi1.Apttus_Config2__ChargeType__c = 'Purchase Price';
        terminationAssetLi1.Apttus_Config2__LineType__c = 'Product/Service';
        terminationAssetLi1.Apttus_Config2__IsPrimaryLine__c = TRUE;
        //terminationAssetLi1.APTS_Original_Agreement__c = aggList[0].Id;
        insert terminationAssetLi1;
        
        //Create New Asset Line Item
        Apttus_Config2__AssetLineItem__c  terminationAssetLi2 = new Apttus_Config2__AssetLineItem__c ();
        terminationAssetLi2.Apttus_CMConfig__AgreementId__c = aggList[0].Id;
        terminationAssetLi2.Apttus_Config2__ChargeType__c = 'Purchase Price';
        terminationAssetLi2.Apttus_Config2__LineType__c = 'Product/Service';
        terminationAssetLi2.Apttus_Config2__IsPrimaryLine__c = TRUE;
        //terminationAssetLi2.APTS_Original_Agreement__c = aggList[0].Id;
        insert terminationAssetLi2;
        
        Apttus__APTS_Agreement__c aggToInsert1 = new Apttus__APTS_Agreement__c();
        aggToInsert1.Name = 'APTS Test Agreement 1111';
        aggToInsert1.Program__c = 'NetJets U.S.';
        aggToInsert1.Apttus__Account__c = accToInsert.Id;
        aggToInsert1.Card_Number__c = '132456789';
        aggToInsert1.APTS_Modification_Type__c = 'New Sale';
        aggToInsert1.RecordTypeId = recordTypeIdNJUS;
        aggToInsert1.Apttus_QPComply__RelatedProposalId__c = testProposal.Id;
        aggList.add(aggToInsert1);
        insert aggList;
                
        Apttus_Config2__AssetLineItem__c  assetLi = new Apttus_Config2__AssetLineItem__c ();
        assetLi.Apttus_CMConfig__AgreementId__c = aggList[1].Id;
        assetLi.Apttus_Config2__ChargeType__c = 'Purchase Price';
        assetLi.Apttus_Config2__LineType__c = 'Product/Service';
        assetLi.Apttus_Config2__IsPrimaryLine__c = TRUE;
        assetLi.APTS_Original_Agreement__c = aggList[0].Id;
        insert assetLi;
        
        //Create New Asset Line Item
        Apttus_Config2__AssetLineItem__c  assetLi1 = new Apttus_Config2__AssetLineItem__c ();
        assetLi1.Apttus_CMConfig__AgreementId__c = aggList[1].Id;
        assetLi1.Apttus_Config2__ChargeType__c = 'Purchase Price';
        assetLi1.Apttus_Config2__LineType__c = 'Product/Service';
        assetLi1.Apttus_Config2__IsPrimaryLine__c = TRUE;
        assetLi1.APTS_Original_Agreement__c = aggList[0].Id;
        insert assetLi1;
        
        Apttus__APTS_Related_Agreement__c relatedAgg = new Apttus__APTS_Related_Agreement__c();
        relatedAgg.Apttus__APTS_Contract_From__c = aggList[1].Id;
        relatedAgg.Apttus__APTS_Contract_To__c = aggList[0].Id;
        relatedAgg.APTS_Business_Object_Type__c='Agreement Life Cycle Flow';
        insert relatedAgg;

       // Apttus__APTS_Related_Agreement__c srcAgreeObj = [SELECT Id,Apttus__APTS_Contract_From__c,Apttus__APTS_Contract_To__c,APTS_Business_Object_Type__c 
       // FROM Apttus__APTS_Related_Agreement__c WHERE Apttus__APTS_Contract_From__c =:agreementId AND APTS_Business_Object_Type__c='Agreement Life Cycle Flow' Limit 1];
           
       System.debug('Termination Agreement ID -->'+ aggList[0].Id);
       System.debug('Regular Agreement ID -->'+ aggList[1].Id);
      
       for(Apttus_Config2__AssetLineItem__c updateALI :[SELECT Id,Apttus_CMConfig__AgreementId__c,Apttus_Config2__AssetStatus__c FROM Apttus_Config2__AssetLineItem__c WHERE Apttus_CMConfig__AgreementId__c =:aggList[0].Id  FOR UPDATE] ) {
        System.debug('Into for loop -->');
    
        }
        //Start Test
        Test.startTest();

          System.enqueueJob(new NJ_ProcessAssetLIQueueble(aggList[1].Id));
        
          //Stop Test
        Test.stopTest();
                
    }
    
    
    
}
