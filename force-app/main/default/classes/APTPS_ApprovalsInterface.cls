/************************************************************************************************************************
@Name: APTPS_ApprovalsInterface
@Author: Conga PS Dev Team
@CreateDate: 25 June 2021
@Description: 
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/

public interface APTPS_ApprovalsInterface {
   void updateApprovals(Map<String, Object> args, List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines);
}