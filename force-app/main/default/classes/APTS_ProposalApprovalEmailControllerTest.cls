/**
 * @description       : Test class for APTS_ProposalApprovalEmailController
 * @author            : Sagar Solanki
 * @group             :
 * @last modified on  : 10-29-2020
 * @last modified by  : Sagar Solanki
 * Modifications Log
 * Ver   Date         Author          Modification
 * 1.0   10-28-2020   Sagar Solanki   Initial Version
 **/
@isTest
public with sharing class APTS_ProposalApprovalEmailControllerTest{
	@isTest
	static void constructorTest(){

		Account testAccount = APTS_CPQTestUtility.createAccount('Test VCB', 'Other');
		insert testAccount;


		Opportunity testOpportunity = APTS_CPQTestUtility.createOpportunity('New Sale Opportunity', 'New', testAccount.Id, 'Closed Won');
		insert testOpportunity;

		Apttus_Config2__PriceList__c testPriceList = APTS_CPQTestUtility.createPriceList('USA', true);
		insert testPriceList;

		//Create proposal
		Apttus_Proposal__Proposal__c nsProposal = APTS_CPQTestUtility.createProposal('New Sale Test Quote', testAccount.Id, testOpportunity.Id, 'Proposal NJUS', testPriceList.Id);
		nsProposal.Deal_Description__c = 'desc';
		insert nsProposal;

		Test.startTest();

		APTS_ProposalApprovalEmailController controller = new APTS_ProposalApprovalEmailController();
		controller.ProposalId = nsProposal.Id;
		controller.getProposal();
		controller.getApprovalRequest();
		System.assert(true, true);
		Test.stopTest();
	}
}