/*************************************************************
@Name: APTPS_UpdateContractRequestCtrl
@Author: Siva Kumar
@CreateDate: 04 December 2020
@Description : This  class is called to update Contract request checkbox
******************************************************************/
public class APTPS_UpdateContractRequestCtrl {
@AuraEnabled
    public static string updateContractRequest(string recordId)
    {
        String msg = '';
        Apttus_Proposal__Proposal__c updateProposal = new Apttus_Proposal__Proposal__c(id=recordId,Contract_Request__c=true);
         try {
                system.debug('updateProposal-->'+updateProposal);
                update updateProposal;
                msg = 'Success';
            } catch(Exception e) {
                msg = 'Fail';
            }
        return msg;
    }
}