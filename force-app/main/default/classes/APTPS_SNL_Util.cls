/************************************************************************************************************************
 @Name: APTPS_SNL_Util
 @Author: Conga PS Dev Team
 @CreateDate: 04 June 2021
 @Description: Util class for SNL business logic
 ************************************************************************************************************************
 @ModifiedBy:
 @ModifiedDate:
 @ChangeDescription:
 ************************************************************************************************************************/

public class APTPS_SNL_Util {
    /**
     @description: Check if provided Program Type is of SNL type or not
     @param: progType - Program Type
     @return: true in case of SNL products
     */
    public static boolean isSNLProgramType(String progType) {
        boolean isSNL = false;
        APTPS_SNL_Products__c snlProducts = APTPS_SNL_Products__c.getOrgDefaults();
        String prodNames = snlProducts.Products__c;
        if(progType != null && prodNames.contains(progType))
            isSNL = true;
        system.debug('Is SNL program type --> '+isSNL);
        return isSNL;
    }
}