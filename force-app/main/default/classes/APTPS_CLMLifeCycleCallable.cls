/************************************************************************************************************************
@Name: APTPS_CLMLifeCycleCallable
@Author: Conga PS Dev Team
@CreateDate: 4 June 2021
@Description: 
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/

public interface APTPS_CLMLifeCycleCallable {
    Object call(String productCode, Map<String, Object> args);
}