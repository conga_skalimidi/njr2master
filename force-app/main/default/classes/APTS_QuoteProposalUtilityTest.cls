/***********************************************************************************************************************
@Name: APTS_QuoteProposalUtilityTest
@Author: Siva Kumar
@CreateDate: 29 October 2020
@Description: APEX class to test classes APTS_QuoteProposalUtility
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/
@isTest
public with sharing class APTS_QuoteProposalUtilityTest{
    @isTest
    static void QuoteProposalUtilityTe(){

         Account testAccount = APTS_CPQTestUtility.createAccount('Test VCB', 'Other');
    insert testAccount;


    Opportunity testOpportunity = APTS_CPQTestUtility.createOpportunity('New Sale Opportunity', 'New', testAccount.Id, 'Closed Won');
    insert testOpportunity;

    Apttus_Config2__AccountLocation__c testAccLocation = APTS_CPQTestUtility.createAccountLocation('Loc1', testAccount.Id);
    insert testAccLocation;
    
    Apttus_Config2__ClassificationName__c testClassification = APTS_CPQTestUtility.createCategory('test Apttus Classification', 'Aircraft Type', 'Apttus', null, true);
    //populate custom fields value if required
    insert testClassification;
    //system.assert(testClassification.Name == 'test Apttus Classification');
    
    Apttus_Config2__ClassificationHierarchy__c testClassificationHierarchy = APTS_CPQTestUtility.createClassificationHierarchy(testClassification.Id, 'Aircraft Type');
    //populate custom fields value if required
    insert testClassificationHierarchy;
    //system.assert(testClassificationHierarchy.Id != null);
    
    Apttus_Config2__ProductOptionGroup__c testProductOptionGroup = new Apttus_Config2__ProductOptionGroup__c();
    //populate custom fields value if required
    testProductOptionGroup.Apttus_Config2__OptionGroupId__c = testClassificationHierarchy.id;
    testProductOptionGroup.Apttus_Config2__Sequence__c = 1;
    insert testProductOptionGroup;
    
    Apttus_Config2__ProductOptionComponent__c testProductOptionComponent = APTS_CPQTestUtility.createProductOptionComponent(1);
    //populate custom fields value if required
    testProductOptionComponent.Apttus_Config2__ProductOptionGroupId__c = testProductOptionGroup.id;
    insert testProductOptionComponent;
    
    

    Apttus_Config2__PriceList__c testPriceList = APTS_CPQTestUtility.createPriceList('USA', true);
    insert testPriceList;

    list<Product2> lstProducts = new list<Product2>();

    // Insert Card Bundle (US)
    Product2 testCardProduct = APTS_CPQTestUtility.createProduct('Card', 'NJUS_CARD', 'Netjets Product', 'Bundle', true, true, true, true);
    //testCardProduct.Program__c = 'NetJets U.S.';

    // Insert Product Type Option (US)
    Product2 testECProduct = APTS_CPQTestUtility.createProduct('Enhancements', 'Enhancements', 'Product Type', 'Option', true, true, true, true);
    //testECProduct.Program__c = 'NetJets U.S.';
    //testECProduct.Cabin_Class__c = 'Mid-Size';
    
    
    Product2 testQSProduct = APTS_CPQTestUtility.createProduct('QS Executive', 'Executive', 'Product Type', 'Option', true, true, true, true);
    //testECProduct.Program__c = 'NetJets U.S.';
    //testECProduct.Cabin_Class__c = 'Mid-Size';
    
    
     // Insert Aircraft 1 Option (US)
    Product2 testAircraft1Product = APTS_CPQTestUtility.createProduct('Gulfstream 450', 'GULFSTREAM_450_NJUS', 'Aircraft', 'Option', true, true, true, true);
    testAircraft1Product.Program__c = 'NetJets U.S.';
    testAircraft1Product.Cabin_Class__c = 'Large';
    testAircraft1Product.Aircraft_Type_Rank__c =15;

    // Insert Aircraft 2 Option (US)
    Product2 testAircraft2Product = APTS_CPQTestUtility.createProduct('Citation Sovereign', 'CITATION_SOVEREIGN_NJUS', 'Aircraft', 'Option', true, true, true, true);
    testAircraft2Product.Program__c = 'NetJets U.S.';
    testAircraft2Product.Cabin_Class__c = 'Mid-Size';
    testAircraft2Product.Aircraft_Type_Rank__c =5;

    lstProducts.add(testCardProduct);
    lstProducts.add(testECProduct);
    lstProducts.add(testAircraft1Product);
    lstProducts.add(testAircraft2Product);
    insert lstProducts;

    //Create proposal
    Apttus_Proposal__Proposal__c nsProposal = APTS_CPQTestUtility.createProposal('New Sale Test Quote', testAccount.Id, testOpportunity.Id, 'Proposal NJUS', testPriceList.Id);
    insert nsProposal;

    // Add Configuration
    Id configuration = APTS_CPQTestUtility.createConfiguration(nsProposal.Id);

    List<Apttus_Config2__LineItem__c> lineitemList = new List<Apttus_Config2__LineItem__c>();
     //Card PLI
    Apttus_Config2__PriceListItem__c cardPLI = APTS_CPQTestUtility.createPriceListItem(testPriceList.Id, testCardProduct.Id, 0.0, 'Purchase Price', 'One Time', 'Per Unit', 'Each', true);
    

    Apttus_Config2__LineItem__c cardLineItem = APTS_CPQTestUtility.getLineItem(configuration, nsProposal, cardPLI, testCardProduct, testCardProduct.Id, null, 'Product/Service', 1, 1, 1, 0, null, 1);
    lineitemList.add(cardLineItem);
    Apttus_Config2__LineItem__c ECLineItem = APTS_CPQTestUtility.getLineItem(configuration, nsProposal, cardPLI, testCardProduct, testCardProduct.Id, testECProduct.Id, 'Option', 1, 2, 2, 2, 1, 1);
    
    
    //ECLineItem.Line_Status_System__c ='New';
    lineitemList.add(ECLineItem);
    
      Apttus_Config2__LineItem__c AC1LineItem = APTS_CPQTestUtility.getLineItem(configuration, nsProposal, cardPLI, testCardProduct, testCardProduct.Id, testAircraft1Product.Id, 'Option', 1, 2, 3, 2, 1, 1);
      AC1LineItem.Apttus_Config2__ChargeType__c = 'Purchase Price';
      //AC1LineItem.Line_Status_System__c ='New';
      
    lineitemList.add(AC1LineItem);
    Apttus_Config2__LineItem__c AC2LineItem = APTS_CPQTestUtility.getLineItem(configuration, nsProposal, cardPLI, testCardProduct, testCardProduct.Id, testAircraft2Product.Id, 'Option', 1, 2, 4, 2, 1, 1);
     AC2LineItem.Apttus_Config2__ChargeType__c = 'Purchase Price';
     AC2LineItem.Apttus_Config2__ProductOptionId__c = testProductOptionComponent.id;
    lineitemList.add(AC2LineItem);
    Apttus_Config2__LineItem__c QSLineItem = APTS_CPQTestUtility.getLineItem(configuration, nsProposal, cardPLI, testCardProduct, testCardProduct.Id, testQSProduct.Id, 'Option', 1, 2, 4, 2, 1, 1);
    lineitemList.add(QSLineItem);

    insert lineitemList;
        
        
    List<Apttus_Config2__ProductAttributeValue__c> pavList = new List<Apttus_Config2__ProductAttributeValue__c>();
    
    Apttus_Config2__ProductAttributeValue__c pav = new Apttus_Config2__ProductAttributeValue__c();
    pav.Apttus_Config2__LineItemId__c = lineItemList[1].id;
    pav.Death_or_Disability_Clause__c   = true;
    //pav.Business_Development_Partnership__c = 'UBS';
    pav.APTS_BizDev_Partnership_Hours__c = true;
    pav.APTS_Bonus_Hours__c = 2;
    pav.Non_Standard__c = 'test';
    //pav.Amount__c = 2;
    pav.APTS_BizDev_Hours_Partnership_Comments__c = 'test';
    pav.APTS_Override_Requested_Hours__c = 2;
    pav.Preferred_Bank__c = '';
    Insert pav;   

    Apttus_Config2__ProductAttributeValue__c qspav = new Apttus_Config2__ProductAttributeValue__c();
    qspav.Apttus_Config2__LineItemId__c = lineItemList[4].id;
    qspav.Upgrade_To_Aircraft_product__c = testAircraft1Product.id;
    qspav.Preferred_Bank__c = '';
    Insert qspav;    
        
        
        lineItemList[0].Apttus_Config2__AttributeValueId__c = pav.id;
        lineItemList[4].Apttus_Config2__AttributeValueId__c = qspav.id;
        lineItemList[4].Apttus_Config2__Description__c = 'QS Executive';
    
        update lineItemList;
        
        Test.startTest();
        APTS_QuoteProposalUtility.updateChangedEnhancementsOnProposal(configuration,'New Sale');
        APTS_QuoteProposalUtility.updateOverrideHours(configuration);
        APTS_QuoteProposalUtility.checkUpgradeEligible(new List<id>{lineItemList[4].id,lineItemList[3].id},'New Sale');
        
        Test.stopTest();
    }
    
     
    @isTest
    static void QuoteProposalUtilityTest() {

         Account testAccount = APTS_CPQTestUtility.createAccount('Test VCB', 'Other');
    insert testAccount;


    Opportunity testOpportunity = APTS_CPQTestUtility.createOpportunity('New Sale Opportunity', 'New', testAccount.Id, 'Closed Won');
    insert testOpportunity;

    Apttus_Config2__AccountLocation__c testAccLocation = APTS_CPQTestUtility.createAccountLocation('Loc1', testAccount.Id);
    insert testAccLocation;
    
    Apttus_Config2__ClassificationName__c testClassification = APTS_CPQTestUtility.createCategory('test Apttus Classification', 'Aircraft Type', 'Apttus', null, true);
    //populate custom fields value if required
    insert testClassification;
    //system.assert(testClassification.Name == 'test Apttus Classification');
    
    Apttus_Config2__ClassificationHierarchy__c testClassificationHierarchy = APTS_CPQTestUtility.createClassificationHierarchy(testClassification.Id, 'Aircraft Type');
    //populate custom fields value if required
    insert testClassificationHierarchy;
    //system.assert(testClassificationHierarchy.Id != null);
    
    Apttus_Config2__ProductOptionGroup__c testProductOptionGroup = new Apttus_Config2__ProductOptionGroup__c();
    //populate custom fields value if required
    testProductOptionGroup.Apttus_Config2__OptionGroupId__c = testClassificationHierarchy.id;
    testProductOptionGroup.Apttus_Config2__Sequence__c = 1;
    insert testProductOptionGroup;
    
    Apttus_Config2__ProductOptionComponent__c testProductOptionComponent = APTS_CPQTestUtility.createProductOptionComponent(1);
    //populate custom fields value if required
    testProductOptionComponent.Apttus_Config2__ProductOptionGroupId__c = testProductOptionGroup.id;
    insert testProductOptionComponent;
    
    

    Apttus_Config2__PriceList__c testPriceList = APTS_CPQTestUtility.createPriceList('USA', true);
    insert testPriceList;

    list<Product2> lstProducts = new list<Product2>();

    // Insert Card Bundle (US)
    Product2 testCardProduct = APTS_CPQTestUtility.createProduct('Card', 'NJUS_CARD', 'Netjets Product', 'Bundle', true, true, true, true);
    //testCardProduct.Program__c = 'NetJets U.S.';

    // Insert Product Type Option (US)
    Product2 testECProduct = APTS_CPQTestUtility.createProduct('Enhancements', 'Enhancements', 'Product Type', 'Option', true, true, true, true);

    
    
    Product2 testQSProduct = APTS_CPQTestUtility.createProduct('QS Executive', 'Executive', 'Product Type', 'Option', true, true, true, true);

    
    
     // Insert Aircraft 1 Option (US)
    Product2 testAircraft1Product = APTS_CPQTestUtility.createProduct('Gulfstream 450', 'GULFSTREAM_450_NJUS', 'Aircraft', 'Option', true, true, true, true);
    testAircraft1Product.Program__c = 'NetJets U.S.';
    testAircraft1Product.Cabin_Class__c = 'Large';
    testAircraft1Product.Aircraft_Type_Rank__c =15;

    // Insert Aircraft 2 Option (US)
    Product2 testAircraft2Product = APTS_CPQTestUtility.createProduct('Citation Sovereign', 'CITATION_SOVEREIGN_NJUS', 'Aircraft', 'Option', true, true, true, true);
    testAircraft2Product.Program__c = 'NetJets U.S.';
    testAircraft2Product.Cabin_Class__c = 'Mid-Size';
    testAircraft2Product.Aircraft_Type_Rank__c =5;

    lstProducts.add(testCardProduct);
    lstProducts.add(testECProduct);
    lstProducts.add(testAircraft1Product);
    lstProducts.add(testAircraft2Product);
    insert lstProducts;

        //Create proposal
        Apttus_Proposal__Proposal__c nsProposal = APTS_CPQTestUtility.createProposal('New Sale Test Quote', testAccount.Id, testOpportunity.Id, 'Proposal NJUS', testPriceList.Id);
        nsProposal.APTS_Updated_Bonus_Hous__c = 3;
        insert nsProposal;

    // Add Configuration
    Id configuration = APTS_CPQTestUtility.createConfiguration(nsProposal.Id);

    List<Apttus_Config2__LineItem__c> lineitemList = new List<Apttus_Config2__LineItem__c>();
     //Card PLI
    Apttus_Config2__PriceListItem__c cardPLI = APTS_CPQTestUtility.createPriceListItem(testPriceList.Id, testCardProduct.Id, 0.0, 'Purchase Price', 'One Time', 'Per Unit', 'Each', true);
    

    Apttus_Config2__LineItem__c cardLineItem = APTS_CPQTestUtility.getLineItem(configuration, nsProposal, cardPLI, testCardProduct, testCardProduct.Id, null, 'Product/Service', 1, 1, 1, 0, null, 1);
    lineitemList.add(cardLineItem);

    // lineitemList.add(ECLineItem);
    
      Apttus_Config2__LineItem__c AC1LineItem = APTS_CPQTestUtility.getLineItem(configuration, nsProposal, cardPLI, testCardProduct, testCardProduct.Id, testAircraft1Product.Id, 'Option', 1, 2, 3, 2, 1, 1);
      AC1LineItem.Apttus_Config2__ChargeType__c = 'Purchase Price';
      //AC1LineItem.Line_Status_System__c ='New';
      
    lineitemList.add(AC1LineItem);
    Apttus_Config2__LineItem__c AC2LineItem = APTS_CPQTestUtility.getLineItem(configuration, nsProposal, cardPLI, testCardProduct, testCardProduct.Id, testAircraft2Product.Id, 'Option', 1, 2, 4, 2, 1, 1);
     AC2LineItem.Apttus_Config2__ChargeType__c = 'Purchase Price';
     AC2LineItem.Apttus_Config2__ProductOptionId__c = testProductOptionComponent.id;
    lineitemList.add(AC2LineItem);
    Apttus_Config2__LineItem__c QSLineItem = APTS_CPQTestUtility.getLineItem(configuration, nsProposal, cardPLI, testCardProduct, testCardProduct.Id, testQSProduct.Id, 'Option', 1, 2, 4, 2, 1, 1);
    lineitemList.add(QSLineItem);

    insert lineitemList;
        
        
    List<Apttus_Config2__ProductAttributeValue__c> pavList = new List<Apttus_Config2__ProductAttributeValue__c>();
    
    Apttus_Config2__ProductAttributeValue__c pav = new Apttus_Config2__ProductAttributeValue__c();
    pav.Apttus_Config2__LineItemId__c = lineItemList[1].id;
    pav.Death_or_Disability_Clause__c   = true;
    //pav.Business_Development_Partnership__c = 'UBS';
    pav.APTS_BizDev_Partnership_Hours__c = true;
    pav.APTS_Bonus_Hours__c = 2;
    pav.Non_Standard__c = 'test';
    //pav.Amount__c = 2;
    pav.APTS_BizDev_Hours_Partnership_Comments__c = 'test';
    pav.APTS_Override_Requested_Hours__c = 2;
    pav.Preferred_Bank__c = '';
    Insert pav;   

    Apttus_Config2__ProductAttributeValue__c qspav = new Apttus_Config2__ProductAttributeValue__c();
    qspav.Apttus_Config2__LineItemId__c = lineItemList[3].id;
    qspav.Upgrade_To_Aircraft_product__c = testAircraft1Product.id;
    qspav.Preferred_Bank__c = '';
    Insert qspav;    
        
        
        lineItemList[0].Apttus_Config2__AttributeValueId__c = pav.id;

    
        update lineItemList;
        
        Test.startTest();
        APTS_QuoteProposalUtility.updateChangedEnhancementsOnProposal(configuration,'Add Enhancements');
        
         
        
         Apttus_Config2__AssetLineItem__c assetLi = APTS_CPQTestUtility.createAssetLI(testECProduct.Id, 'Option', 'New', null);
        //assetli.Apttus_Config2__AttributeValueId__c = aav.Id;
        insert assetLi;
        
        Apttus_Config2__AssetAttributeValue__c aav = new Apttus_Config2__AssetAttributeValue__c();
        aav.Death_or_Disability_Clause__c   = true;
        aav.Business_Development_Partnership__c = 'UBS';
        aav.APTS_Bonus_Hours__c = 2;
        aav.Non_Standard__c = 'test';
        aav.Amount__c = 2;
        aav.APTS_Override_Requested_Hours__c = 2;
        aav.Apttus_Config2__AssetLineItemId__c = assetLi.Id;
        Insert aav;
        
        assetli.Apttus_Config2__AttributeValueId__c = aav.Id;
        update assetLi;
        
        Apttus_Config2__LineItem__c ECLineItem = APTS_CPQTestUtility.getLineItem(configuration, nsProposal, cardPLI, testCardProduct, testCardProduct.Id, testECProduct.Id, 'Option', 1, 2, 2, 2, 1, 1);
        ECLineItem.Apttus_Config2__AssetLineItemId__c = assetLi.Id;
        ECLineItem.Apttus_Config2__AttributeValueId__c = pav.Id;
        insert ECLineItem;
        
        APTS_QuoteProposalUtility.updateChangedEnhancementsOnProposal(configuration, 'New Card Sale Conversion'); 
       
        
        Test.stopTest();
        
        
       
    
    }
    
      
    @isTest
    static void QuoteProposalUtilityTest3() {

         Account testAccount = APTS_CPQTestUtility.createAccount('Test VCB', 'Other');
    insert testAccount;


    Opportunity testOpportunity = APTS_CPQTestUtility.createOpportunity('New Sale Opportunity', 'New', testAccount.Id, 'Closed Won');
    insert testOpportunity;

    Apttus_Config2__AccountLocation__c testAccLocation = APTS_CPQTestUtility.createAccountLocation('Loc1', testAccount.Id);
    insert testAccLocation;
    
    Apttus_Config2__ClassificationName__c testClassification = APTS_CPQTestUtility.createCategory('test Apttus Classification', 'Aircraft Type', 'Apttus', null, true);
    //populate custom fields value if required
    insert testClassification;
    //system.assert(testClassification.Name == 'test Apttus Classification');
    
    Apttus_Config2__ClassificationHierarchy__c testClassificationHierarchy = APTS_CPQTestUtility.createClassificationHierarchy(testClassification.Id, 'Aircraft Type');
    //populate custom fields value if required
    insert testClassificationHierarchy;
    //system.assert(testClassificationHierarchy.Id != null);
    
    Apttus_Config2__ProductOptionGroup__c testProductOptionGroup = new Apttus_Config2__ProductOptionGroup__c();
    //populate custom fields value if required
    testProductOptionGroup.Apttus_Config2__OptionGroupId__c = testClassificationHierarchy.id;
    testProductOptionGroup.Apttus_Config2__Sequence__c = 1;
    insert testProductOptionGroup;
    
    Apttus_Config2__ProductOptionComponent__c testProductOptionComponent = APTS_CPQTestUtility.createProductOptionComponent(1);
    //populate custom fields value if required
    testProductOptionComponent.Apttus_Config2__ProductOptionGroupId__c = testProductOptionGroup.id;
    insert testProductOptionComponent;
    
    

    Apttus_Config2__PriceList__c testPriceList = APTS_CPQTestUtility.createPriceList('USA', true);
    insert testPriceList;

    list<Product2> lstProducts = new list<Product2>();

    // Insert Card Bundle (US)
    Product2 testCardProduct = APTS_CPQTestUtility.createProduct('Card', 'NJUS_CARD', 'Netjets Product', 'Bundle', true, true, true, true);
    //testCardProduct.Program__c = 'NetJets U.S.';

    // Insert Product Type Option (US)
    Product2 testECProduct = APTS_CPQTestUtility.createProduct('Enhancements', 'Enhancements', 'Product Type', 'Option', true, true, true, true);

    
    
    Product2 testQSProduct = APTS_CPQTestUtility.createProduct('QS Executive', 'Executive', 'Product Type', 'Option', true, true, true, true);

    
    
     // Insert Aircraft 1 Option (US)
    Product2 testAircraft1Product = APTS_CPQTestUtility.createProduct('Gulfstream 450', 'GULFSTREAM_450_NJUS', 'Aircraft', 'Option', true, true, true, true);
    testAircraft1Product.Program__c = 'NetJets U.S.';
    testAircraft1Product.Cabin_Class__c = 'Large';
    testAircraft1Product.Aircraft_Type_Rank__c =15;

    // Insert Aircraft 2 Option (US)
    Product2 testAircraft2Product = APTS_CPQTestUtility.createProduct('Citation Sovereign', 'CITATION_SOVEREIGN_NJUS', 'Aircraft', 'Option', true, true, true, true);
    testAircraft2Product.Program__c = 'NetJets U.S.';
    testAircraft2Product.Cabin_Class__c = 'Mid-Size';
    testAircraft2Product.Aircraft_Type_Rank__c =5;

    lstProducts.add(testCardProduct);
    lstProducts.add(testECProduct);
    lstProducts.add(testAircraft1Product);
    lstProducts.add(testAircraft2Product);
    insert lstProducts;

        //Create proposal
        Apttus_Proposal__Proposal__c nsProposal = APTS_CPQTestUtility.createProposal('New Sale Test Quote', testAccount.Id, testOpportunity.Id, 'Proposal NJUS', testPriceList.Id);
        nsProposal.APTS_Updated_Bonus_Hous__c = 3;
        insert nsProposal;

    // Add Configuration
    Id configuration = APTS_CPQTestUtility.createConfiguration(nsProposal.Id);

    List<Apttus_Config2__LineItem__c> lineitemList = new List<Apttus_Config2__LineItem__c>();
     //Card PLI
    Apttus_Config2__PriceListItem__c cardPLI = APTS_CPQTestUtility.createPriceListItem(testPriceList.Id, testCardProduct.Id, 0.0, 'Purchase Price', 'One Time', 'Per Unit', 'Each', true);
    

    Apttus_Config2__LineItem__c cardLineItem = APTS_CPQTestUtility.getLineItem(configuration, nsProposal, cardPLI, testCardProduct, testCardProduct.Id, null, 'Product/Service', 1, 1, 1, 0, null, 1);
    lineitemList.add(cardLineItem);

    // lineitemList.add(ECLineItem);
    
      Apttus_Config2__LineItem__c AC1LineItem = APTS_CPQTestUtility.getLineItem(configuration, nsProposal, cardPLI, testCardProduct, testCardProduct.Id, testAircraft1Product.Id, 'Option', 1, 2, 3, 2, 1, 1);
      AC1LineItem.Apttus_Config2__ChargeType__c = 'Purchase Price';
      //AC1LineItem.Line_Status_System__c ='New';
      
    lineitemList.add(AC1LineItem);
    Apttus_Config2__LineItem__c AC2LineItem = APTS_CPQTestUtility.getLineItem(configuration, nsProposal, cardPLI, testCardProduct, testCardProduct.Id, testAircraft2Product.Id, 'Option', 1, 2, 4, 2, 1, 1);
     AC2LineItem.Apttus_Config2__ChargeType__c = 'Purchase Price';
     AC2LineItem.Apttus_Config2__ProductOptionId__c = testProductOptionComponent.id;
    lineitemList.add(AC2LineItem);
    Apttus_Config2__LineItem__c QSLineItem = APTS_CPQTestUtility.getLineItem(configuration, nsProposal, cardPLI, testCardProduct, testCardProduct.Id, testQSProduct.Id, 'Option', 1, 2, 4, 2, 1, 1);
    lineitemList.add(QSLineItem);

    insert lineitemList;
        
        
    List<Apttus_Config2__ProductAttributeValue__c> pavList = new List<Apttus_Config2__ProductAttributeValue__c>();
    
    Apttus_Config2__ProductAttributeValue__c pav = new Apttus_Config2__ProductAttributeValue__c();
    pav.Apttus_Config2__LineItemId__c = lineItemList[1].id;
    pav.Death_or_Disability_Clause__c   = true;
    //pav.Business_Development_Partnership__c = 'UBS';
    pav.APTS_BizDev_Partnership_Hours__c = true;
    pav.APTS_Bonus_Hours__c = 2;
    pav.Non_Standard__c = 'test';
    //pav.Amount__c = 2;
    pav.APTS_BizDev_Hours_Partnership_Comments__c = 'test';
    pav.APTS_Override_Requested_Hours__c = 2;
    pav.Preferred_Bank__c = '';
    Insert pav;   

    Apttus_Config2__ProductAttributeValue__c qspav = new Apttus_Config2__ProductAttributeValue__c();
    qspav.Apttus_Config2__LineItemId__c = lineItemList[3].id;
    qspav.Upgrade_To_Aircraft_product__c = testAircraft1Product.id;
    qspav.Preferred_Bank__c = '';
    Insert qspav;    
        
        
        lineItemList[0].Apttus_Config2__AttributeValueId__c = pav.id;

    
        update lineItemList;
        
        Test.startTest();
        APTS_QuoteProposalUtility.updateChangedEnhancementsOnProposal(configuration,'Add Enhancements');
     
        
        Test.stopTest();
        
        
       
    
    }
    
    @isTest
    static void QuoteProposalUtilitySNLTest() {
        
        Account accToInsert = APTS_CPQTestUtility.createAccount('Test VCB', 'Other');
        insert accToInsert ;
        
        Opportunity testOpportunity = APTS_CPQTestUtility.createOpportunity('New Sale Opportunity', 'New', accToInsert.Id, 'Closed Won');
        insert testOpportunity;
        
        Apttus_Config2__AccountLocation__c testAccLocation = APTS_CPQTestUtility.createAccountLocation('Loc1', accToInsert.Id);
        insert testAccLocation;

        Apttus_Config2__ClassificationName__c testClassification = APTS_CPQTestUtility.createCategory('test Apttus Classification', 'Aircraft Type', 'Apttus', null, true);
        //populate custom fields value if required
        insert testClassification;
        //system.assert(testClassification.Name == 'test Apttus Classification');

        Apttus_Config2__ClassificationHierarchy__c testClassificationHierarchy = APTS_CPQTestUtility.createClassificationHierarchy(testClassification.Id, 'Aircraft Type');
        //populate custom fields value if required
        insert testClassificationHierarchy;
        //system.assert(testClassificationHierarchy.Id != null);

        Apttus_Config2__ProductOptionGroup__c testProductOptionGroup = new Apttus_Config2__ProductOptionGroup__c();
        //populate custom fields value if required
        testProductOptionGroup.Apttus_Config2__OptionGroupId__c = testClassificationHierarchy.id;
        testProductOptionGroup.Apttus_Config2__Sequence__c = 1;
        insert testProductOptionGroup;

        Apttus_Config2__ProductOptionComponent__c testProductOptionComponent = APTS_CPQTestUtility.createProductOptionComponent(1);
        //populate custom fields value if required
        testProductOptionComponent.Apttus_Config2__ProductOptionGroupId__c = testProductOptionGroup.id;
        insert testProductOptionComponent;

        Apttus_Config2__PriceList__c testPriceList = APTS_CPQTestUtility.createPriceList('USA', true);
        insert testPriceList;

        //Create Products
        List<Product2> prodList = new List<Product2>();
        Product2 NJUS_DEMO = APTS_CPQTestUtility.createProduct('Demo', 'NJUS_DEMO', 'Apttus', 'Bundle', true, true, true, true);
        prodList.add(NJUS_DEMO);
        
        Product2 NJE_DEMO = APTS_CPQTestUtility.createProduct('Demo', 'NJE_DEMO', 'Apttus', 'Bundle', true, true, true, true);
        prodList.add(NJE_DEMO);
        
        Product2 NJUS_FLIGHT_INFO = APTS_CPQTestUtility.createProduct('Flight Info', 'NJUS_FLIGHT_INFO', 'Apttus', 'Option', true, true, true, true);
        prodList.add(NJUS_FLIGHT_INFO);
        
        Product2 NJUS_RET_FLIGHT_INFO = APTS_CPQTestUtility.createProduct('Flight Return Info', 'NJUS_RET_FLIGHT_INFO', 'Apttus', 'Option', true, true, true, true);
        prodList.add(NJUS_RET_FLIGHT_INFO);
        
        Product2 NJE_CORP = APTS_CPQTestUtility.createProduct('Corporate Trial', 'NJE_Corporate_Trial', 'Apttus', 'Standalone', true, true, true, true);
        prodList.add(NJE_CORP);
        
        insert prodList;

        //Create Quote/Proposals
        List<Apttus_Proposal__Proposal__c> proposalList = new List<Apttus_Proposal__Proposal__c>();
        Apttus_Proposal__Proposal__c demoNJUSQuote = APTS_CPQTestUtility.createProposal('DEMO NJUS Quote', accToInsert.Id, testOpportunity.Id, 'Proposal', testPriceList.Id);
        demoNJUSQuote.APTS_New_Sale_or_Modification__c = APTS_Constants.NEW_SALE;
        demoNJUSQuote.APTPS_Program_Type__c = 'Demo';
        //demoNJUSQuote.CurrencyIsoCode = 'USD';
        proposalList.add(demoNJUSQuote);
        
        Apttus_Proposal__Proposal__c demoNJEQuote = APTS_CPQTestUtility.createProposal('DEMO NJE Quote', accToInsert.Id, testOpportunity.Id, 'Proposal', testPriceList.Id);
        demoNJEQuote.APTS_New_Sale_or_Modification__c = APTS_Constants.NEW_SALE;
        demoNJEQuote.APTPS_Program_Type__c = 'Demo';
        //demoNJEQuote.CurrencyIsoCode = 'EUR';
        proposalList.add(demoNJEQuote);
        
        Apttus_Proposal__Proposal__c corpNJEQuote = APTS_CPQTestUtility.createProposal('Corporate NJE Quote', accToInsert.Id, testOpportunity.Id, 'Proposal', testPriceList.Id);
        corpNJEQuote.APTS_New_Sale_or_Modification__c = APTS_Constants.NEW_SALE;
        corpNJEQuote.APTPS_Program_Type__c = 'Corporate Trial';
        //corpNJEQuote.CurrencyIsoCode = 'EUR';
        proposalList.add(corpNJEQuote);
        
        insert proposalList;

        // Add Configuration
        Id demoNJUSconfig = APTS_CPQTestUtility.createConfiguration(demoNJUSQuote.Id);
        Id demoNJEconfig = APTS_CPQTestUtility.createConfiguration(demoNJEQuote.Id);
        Id corpNJESconfig = APTS_CPQTestUtility.createConfiguration(corpNJEQuote.Id);
        

        List<Apttus_Config2__LineItem__c> lineitemList = new List<Apttus_Config2__LineItem__c>();
        //CT PLI
        Apttus_Config2__PriceListItem__c CTPLI = APTS_CPQTestUtility.createPriceListItem(testPriceList.Id, NJE_CORP.Id, 0.0, 'Purchase Price', 'One Time', 'Per Unit', 'Each', true);
        
        //DEMO NJUS PLI
        Apttus_Config2__PriceListItem__c DemoNJUSPLI = APTS_CPQTestUtility.createPriceListItem(testPriceList.Id, NJUS_DEMO.Id, 0.0, 'Purchase Price', 'One Time', 'Per Unit', 'Each', true);
        
        //DEMO NJE PLI
        Apttus_Config2__PriceListItem__c DemoNJEPLI = APTS_CPQTestUtility.createPriceListItem(testPriceList.Id, NJE_DEMO.Id, 0.0, 'Purchase Price', 'One Time', 'Per Unit', 'Each', true);

        //CT LI
        Apttus_Config2__LineItem__c CTLineItem = APTS_CPQTestUtility.getLineItem(corpNJESconfig, corpNJEQuote, CTPLI, NJE_CORP, NJE_CORP.Id, null, 'Product/Service', 1, 1, 1, 0, null, 1);
        lineitemList.add(CTLineItem);
        
        //DEMO NJUS LI
        Apttus_Config2__LineItem__c demoNJUSLI = APTS_CPQTestUtility.getLineItem(demoNJUSconfig, demoNJUSQuote, DemoNJUSPLI, NJUS_DEMO, NJUS_DEMO.Id, null, 'Product/Service', 1, 1, 1, 0, null, 1);
        lineitemList.add(demoNJUSLI);
        
        Apttus_Config2__LineItem__c flightInfoLI = APTS_CPQTestUtility.getLineItem(demoNJUSconfig, demoNJUSQuote, DemoNJUSPLI, NJUS_FLIGHT_INFO, NJUS_DEMO.Id, NJUS_FLIGHT_INFO.Id, 'Option', 1, 2, 3, 2, 1, 1);
        lineitemList.add(flightInfoLI);
        Apttus_Config2__LineItem__c flightRetInfoLI = APTS_CPQTestUtility.getLineItem(demoNJUSconfig, demoNJUSQuote, DemoNJUSPLI, NJUS_RET_FLIGHT_INFO, NJUS_DEMO.Id, NJUS_RET_FLIGHT_INFO.Id, 'Option', 1, 2, 3, 2, 1, 1);
        lineitemList.add(flightRetInfoLI);
        
        
        //DEMO NJE LI
        Apttus_Config2__LineItem__c demoNJELI = APTS_CPQTestUtility.getLineItem(demoNJEconfig, demoNJEQuote, DemoNJEPLI, NJE_DEMO, NJE_DEMO.Id, null, 'Product/Service', 1, 1, 1, 0, null, 1);
        lineitemList.add(demoNJELI);        
            
        insert lineitemList;
        
        List<Apttus_Config2__ProductAttributeValue__c> pavList = new List<Apttus_Config2__ProductAttributeValue__c>();
        
        //CT PAV
        Apttus_Config2__ProductAttributeValue__c CTPav = new Apttus_Config2__ProductAttributeValue__c();
        CTPav.Apttus_Config2__LineItemId__c = CTLineItem.id;
        CTPav.APTPS_Term_End_Date__c   = Date.today();
        CTPav.APTPS_Term_Months__c   = String.valueOf(3);
        CTPav.APTPS_Override_Deposit__c   = 100.00;
        CTPav.APTPS_Corporate_Trial_Aircraft__c   = 'Citation Latitude';
        CTPav.Preferred_Bank__c = '';
        pavList.add(CTPav);
        
        //DEMO NJUS PAV
        Apttus_Config2__ProductAttributeValue__c demoNJUSPav = new Apttus_Config2__ProductAttributeValue__c();
        demoNJUSPav.Apttus_Config2__LineItemId__c = demoNJUSLI.id;
        demoNJUSPav.APTPS_Type_of_Demo__c = 'Card';
        demoNJUSPav.APTPS_Demo_Aircraft__c = 'Citation XLS';
        demoNJUSPav.APTPS_Rate_Type__c = APTS_Constants.PREMIUM;
        demoNJUSPav.APTPS_Waive_Credit_Card_Authorization__c = true;
        demoNJUSPav.APTPS_Waive_Positioning_Fees__c = true;
        demoNJUSPav.Preferred_Bank__c = '';
        pavList.add(demoNJUSPav);
        
        //DEMO NJE PAV
        Apttus_Config2__ProductAttributeValue__c demoNJEPav = new Apttus_Config2__ProductAttributeValue__c();
        demoNJEPav.Apttus_Config2__LineItemId__c = demoNJELI.id;
        demoNJEPav.APTPS_Type_of_Demo__c = 'Card';
        demoNJEPav.APTPS_Demo_Aircraft__c = 'Phenom 300';
        demoNJEPav.APTPS_NJE_Rate_Type__c = APTS_Constants.PREMIUM;
        demoNJEPav.APTPS_Waive_Positioning_Fees__c = true;
        demoNJEPav.Preferred_Bank__c = '';
        pavList.add(demoNJEPav);
        
        Insert pavList;   
        
        lineItemList[0].Apttus_Config2__AttributeValueId__c = CTPav.id;
        lineItemList[1].Apttus_Config2__AttributeValueId__c = demoNJUSPav.id;
        lineItemList[2].Apttus_Config2__AttributeValueId__c = demoNJEPav.id;


        update lineItemList;
        
        Test.startTest();
        APTS_QuoteProposalUtility.updateCTAttributeFieldsOnProposal(corpNJESconfig);
        APTS_QuoteProposalUtility.updateDemoAttributeFieldsOnProposal(demoNJUSconfig);
        APTS_QuoteProposalUtility.updateDemoAttributeFieldsOnProposal(demoNJEconfig);


        Test.stopTest();


       
    
    }
    
    
    

   
}