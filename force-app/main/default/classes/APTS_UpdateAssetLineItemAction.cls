/*******************************************************************************************************************************************
@Name: APTS_UpdateAssetLineItemAction
@Author: Avinash Bamane
@CreateDate: 11/03/2020
@Description: This invocable method will get triggered by 'Agreement- Chevron Status' process builder. It will execute flow
in async transaction to update parent agreement status.
@Defect Number: GCM-7024
********************************************************************************************************************************************/

global class APTS_UpdateAssetLineItemAction {
    
    public class UpdateAssetLineItemRequest {
        @InvocableVariable(required=true)
        public Id vProposalId;
        @InvocableVariable(required=true)
        public String vNJAssetStatus;
        @InvocableVariable(required=true)
        public String vALIAgreementStatus;
        @InvocableVariable(required=true)
        public String vAgreementStatus;
    }
    
    @InvocableMethod(label='Updates an Asset Line Item' description='Updates an Asset Line Item')
    public static void updateParentAssetAgreements (UpdateAssetLineItemRequest[] updateAssetLineItemRequests) {
        for(UpdateAssetLineItemRequest request : updateAssetLineItemRequests) {
            system.debug('Proposal Id -->'+request.vProposalId+' Asset Status -->'+request.vNJAssetStatus+
                        ' ALI Agreement Status -->'+request.vALIAgreementStatus+
                        ' Agreement Status -->'+request.vAgreementStatus);
            callFlow(request.vProposalId, request.vNJAssetStatus, request.vALIAgreementStatus, request.vAgreementStatus);
        }
    }
    
    @future
    public static void callFlow(Id vProposalId, String vNJAssetStatus, String vALIAgreementStatus, String vAgreementStatus) {
        Flow.Interview flow = new Flow.Interview.APTS_Update_AssetLineItem_NJ_Status(new map<String,Object>
                                                                                     {'vProposalId' => vProposalId,
                                                                                      'vNJAssetStatus' => vNJAssetStatus,
                                                                                      'vALIAgreementStatus' => vALIAgreementStatus,
                                                                                      'vAgreementStatus' => vAgreementStatus});     
        flow.start();
        
    }   
}