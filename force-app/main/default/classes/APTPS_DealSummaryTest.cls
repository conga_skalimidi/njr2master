/************************************************************************************************************************
@Name: APTS_QuoteProposalBaseTriggerHandler
@Author: Conga PS Dev Team
@CreateDate: 18 May 2021
@Description: Test class coverage for Deal Summary implementation
************************************************************************************************************************/
@isTest
public class APTPS_DealSummaryTest {
	@testsetup
    public static void makeData() {
        Test.startTest();
        //Create SNL Custom settings
        APTPS_SNL_Products__c snlProducts = new APTPS_SNL_Products__c();
        snlProducts.Products__c = 'Demo,Corporate Trial';
		insert snlProducts;
        
        Account accToInsert = new Account();
        accToInsert.Name = 'APTS Test Account';
        insert accToInsert;
        
        Apttus_Config2__AccountLocation__c testAccLocation = APTS_CPQTestUtility.createAccountLocation('Loc1', accToInsert.Id);
        insert testAccLocation;
        
        Opportunity testOpportunity = APTS_CPQTestUtility.createOpportunity('test Apttus Opportunity', 'New Customer', accToInsert.Id, 'Propose');
        insert testOpportunity;
        
        Apttus_Config2__PriceList__c testPriceList = APTS_CPQTestUtility.createPriceList('test Apttus Price List', true);
        insert testPriceList;
        
        //Create Products
        List<Product2> prodList = new List<Product2>();
        Product2 NJUS_DEMO = APTS_CPQTestUtility.createProduct('Demo', 'NJUS_DEMO', 'Apttus', 'Bundle', true, true, true, true);
        prodList.add(NJUS_DEMO);
        
        Product2 NJE_DEMO = APTS_CPQTestUtility.createProduct('Demo', 'NJE_DEMO', 'Apttus', 'Bundle', true, true, true, true);
        prodList.add(NJE_DEMO);
        
        Product2 NJE_CORP = APTS_CPQTestUtility.createProduct('Corporate Trial', 'NJE_Corporate_Trial', 'Apttus', 'Standalone', true, true, true, true);
        prodList.add(NJE_CORP);
        insert prodList;
        
        //Create Quote/Proposals
        List<Apttus_Proposal__Proposal__c> proposalList = new List<Apttus_Proposal__Proposal__c>();
        Apttus_Proposal__Proposal__c demoNJUSQuote = APTS_CPQTestUtility.createProposal('DEMO NJUS Quote', accToInsert.Id, testOpportunity.Id, 'Proposal', testPriceList.Id);
        demoNJUSQuote.APTS_New_Sale_or_Modification__c = APTS_Constants.NEW_SALE;
        demoNJUSQuote.APTPS_Program_Type__c = 'Demo';
        //demoNJUSQuote.CurrencyIsoCode = 'USD';
        demoNJUSQuote.Program__c = APTS_ConstantUtil.NETJETS_USD;
        proposalList.add(demoNJUSQuote);
        
        Apttus_Proposal__Proposal__c demoNJEQuote = APTS_CPQTestUtility.createProposal('DEMO NJE Quote', accToInsert.Id, testOpportunity.Id, 'Proposal', testPriceList.Id);
        demoNJEQuote.APTS_New_Sale_or_Modification__c = APTS_Constants.NEW_SALE;
        demoNJEQuote.APTPS_Program_Type__c = 'Demo';
        //demoNJEQuote.CurrencyIsoCode = 'EUR';
        demoNJEQuote.Program__c = APTS_ConstantUtil.NETJETS_EUR;
        proposalList.add(demoNJEQuote);
        
        Apttus_Proposal__Proposal__c corpNJEQuote = APTS_CPQTestUtility.createProposal('Corporate NJE Quote', accToInsert.Id, testOpportunity.Id, 'Proposal', testPriceList.Id);
        corpNJEQuote.APTS_New_Sale_or_Modification__c = APTS_Constants.NEW_SALE;
        corpNJEQuote.APTPS_Program_Type__c = 'Corporate Trial';
        //corpNJEQuote.CurrencyIsoCode = 'EUR';
        corpNJEQuote.Program__c = APTS_ConstantUtil.NETJETS_EUR;
        proposalList.add(corpNJEQuote);
        insert proposalList;
        
        //Create Quote/Proposal Line Items
        List<Apttus_Proposal__Proposal_Line_Item__c> pliList = new List<Apttus_Proposal__Proposal_Line_Item__c>();
        Apttus_Proposal__Proposal_Line_Item__c demoUS = APTS_CPQTestUtility.getPropLI(NJUS_DEMO.Id, 'New', demoNJUSQuote.Id, '', null);
        demoUS.Apttus_QPConfig__LineType__c = 'Product/Service';
        demoUS.Apttus_QPConfig__IsPrimaryLine__c = TRUE;
        demoUS.Apttus_QPConfig__NetPrice__c = 123;
        pliList.add(demoUS);
        
        Apttus_Proposal__Proposal_Line_Item__c demoNJE = APTS_CPQTestUtility.getPropLI(NJE_DEMO.Id, 'New', demoNJEQuote.Id, '', null);
        demoNJE.Apttus_QPConfig__LineType__c = 'Product/Service';
        demoNJE.Apttus_QPConfig__IsPrimaryLine__c = TRUE;
        demoNJE.Apttus_QPConfig__NetPrice__c = 123;
        pliList.add(demoNJE);
        
        Apttus_Proposal__Proposal_Line_Item__c corporateNJE = APTS_CPQTestUtility.getPropLI(NJE_CORP.Id, 'New', corpNJEQuote.Id, '', null);
        corporateNJE.Apttus_QPConfig__LineType__c = 'Product/Service';
        corporateNJE.Apttus_QPConfig__IsPrimaryLine__c = TRUE;
        corporateNJE.Apttus_QPConfig__NetPrice__c = 100;
        pliList.add(corporateNJE);
        insert pliList;
        
        //Create Proposal Attribute Value
        List<Apttus_QPConfig__ProposalProductAttributeValue__c> pavList = new List<Apttus_QPConfig__ProposalProductAttributeValue__c>();
        Apttus_QPConfig__ProposalProductAttributeValue__c demoNJUSPav = new Apttus_QPConfig__ProposalProductAttributeValue__c();
        demoNJUSPav.Apttus_QPConfig__LineItemId__c = demoUS.Id;
        demoNJUSPav.APTPS_Type_of_Demo__c = 'Card';
        demoNJUSPav.APTPS_Demo_Aircraft__c = 'Citation XLS';
        demoNJUSPav.APTPS_Rate_Type__c = 'Non-Premium';
        pavList.add(demoNJUSPav);
        
        Apttus_QPConfig__ProposalProductAttributeValue__c demoNJEPav = new Apttus_QPConfig__ProposalProductAttributeValue__c();
        demoNJEPav.Apttus_QPConfig__LineItemId__c = demoNJE.Id;
        demoNJEPav.APTPS_Type_of_Demo__c = 'Card';
        demoNJEPav.APTPS_Demo_Aircraft__c = 'Phenom 300';
        demoNJEPav.APTPS_Rate_Type__c = 'Non-Premium';
        pavList.add(demoNJEPav);
        
        Apttus_QPConfig__ProposalProductAttributeValue__c corporateNJEPav = new Apttus_QPConfig__ProposalProductAttributeValue__c();
        corporateNJEPav.Apttus_QPConfig__LineItemId__c = corporateNJE.Id;
        corporateNJEPav.APTPS_Corporate_Trial_Aircraft__c = 'Citation Latitude';
        corporateNJEPav.APTPS_Term_Months__c = String.valueOf(3);
        corporateNJEPav.APTPS_Term_End_Date__c = Date.today();
        corporateNJEPav.APTPS_Maximum_Number_of_Trials__c = 2.0;
        corporateNJEPav.APTPS_Override_Deposit__c = 100.00;
        pavList.add(corporateNJEPav);
        insert pavList;
		
        //Link PAVs to respective PLIs
        List<Apttus_Proposal__Proposal_Line_Item__c> pliToUpdate = new List<Apttus_Proposal__Proposal_Line_Item__c>();
        demoUS.Apttus_QPConfig__AttributeValueId__c = demoNJUSPav.Id;
        demoNJE.Apttus_QPConfig__AttributeValueId__c = demoNJEPav.Id;
        corporateNJE.Apttus_QPConfig__AttributeValueId__c = corporateNJEPav.Id;
        pliToUpdate.add(demoUS);
        pliToUpdate.add(demoNJE);
        pliToUpdate.add(corporateNJE);
        update pliToUpdate;
        Test.stopTest();
    }
    
    @isTest
    public static void testDealSummary() {
        Test.startTest();
        List<Apttus_Proposal__Proposal__c> quotesToUpdate = new List<Apttus_Proposal__Proposal__c>();
        Apttus_Proposal__Proposal__c demoNJUSQuote = [SELECT Id, Apttus_QPConfig__ConfigurationFinalizedDate__c FROM Apttus_Proposal__Proposal__c 
                                                      WHERE APTPS_Program_Type__c = 'Demo' AND Program__c =: APTS_ConstantUtil.NETJETS_USD LIMIT 1];
        demoNJUSQuote.Apttus_QPConfig__ConfigurationFinalizedDate__c = Date.today();
        quotesToUpdate.add(demoNJUSQuote);
        
        Apttus_Proposal__Proposal__c demoNJEQuote = [SELECT Id, Apttus_QPConfig__ConfigurationFinalizedDate__c FROM Apttus_Proposal__Proposal__c 
                                                     WHERE APTPS_Program_Type__c = 'Demo' AND Program__c =: APTS_ConstantUtil.NETJETS_EUR LIMIT 1];
        demoNJEQuote.Apttus_QPConfig__ConfigurationFinalizedDate__c = Date.today();
        quotesToUpdate.add(demoNJEQuote);
        
        Apttus_Proposal__Proposal__c corporateNJEQuote = [SELECT Id, Apttus_QPConfig__ConfigurationFinalizedDate__c FROM Apttus_Proposal__Proposal__c 
                                                          WHERE APTPS_Program_Type__c = 'Corporate Trial' AND Program__c =: APTS_ConstantUtil.NETJETS_EUR LIMIT 1];
        corporateNJEQuote.Apttus_QPConfig__ConfigurationFinalizedDate__c = Date.today();
        quotesToUpdate.add(corporateNJEQuote);
        update quotesToUpdate;
        
        APTPS_DealSummary testObj = new APTPS_DealSummary();
        Map<Id, String> quoteProductMap = new Map<Id, String>();
            quoteProductMap.put(demoNJUSQuote.Id, 'NJUS_DEMO');
        try{
            testObj.call(quoteProductMap, null);
        }catch(APTPS_DealSummary.ExtensionMalformedCallException e) {
            system.debug('Test coverage for exception');
        }
        
        try{
       		Map<Id, Map<String, Object>> quoteArgMap = new Map<Id, Map<String, Object>>();
            quoteArgMap.put(demoNJUSQuote.Id, null);
            testObj.call(quoteProductMap, quoteArgMap);
        }catch(APTPS_DealSummary.ExtensionMalformedCallException e) {
            system.debug('Test coverage for exception');
        }
        
        Test.stopTest();
    }
}