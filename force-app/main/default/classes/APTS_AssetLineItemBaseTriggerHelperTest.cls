/***********************************************************************************************************************
@Name: APTS_AssetLineItemBaseTriggerHelperTest
@Author: Ramesh Kumar
@CreateDate: 19 Jan 2021
@Description: APEX class to test classes APTS_AssetLineItemBaseTriggerHelper
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/
@isTest
public with sharing class APTS_AssetLineItemBaseTriggerHelperTest{
    @isTest
   /*  static void updateEHCOnParentAssetPAVTest(){
        List<Account> accList = new List<Account>();
        Id recordTypeIdNJUS = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('NJUS New Card Sale').getRecordTypeId();
        Id recordTypeIdNJE = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('NJE New Card Sale').getRecordTypeId();

        Account accToInsert = new Account();
        accToInsert.Name = 'APTS Test Account';
        accToInsert.APTS_Has_Active_NJUS_Agreement__c = true;
        accToInsert.APTS_Has_Active_NJE_Agreement__c = true;
        insert accToInsert;

        Opportunity testOpportunity = APTS_CPQTestUtility.createOpportunity('test Apttus Opportunity', 'New Customer', accToInsert.Id, 'Propose');
        insert testOpportunity;

        Apttus_Config2__PriceList__c testPriceList = APTS_CPQTestUtility.createPriceList('test Apttus Price List', true);
        insert testPriceList;

        Apttus_Proposal__Proposal__c testProposal = APTS_CPQTestUtility.createProposal('test Apttus Proposal', accToInsert.Id, testOpportunity.Id, 'Proposal', testPriceList.Id);
        //populate custom fields value if required
        testProposal.APTS_New_Sale_or_Modification__c = 'Modification';
        testProposal.APTS_Modification_Type__c = 'Expired Hours Card';
        insert testProposal;            
        
        List<Apttus__APTS_Agreement__c> aggList = new List<Apttus__APTS_Agreement__c>();
        Apttus__APTS_Agreement__c aggToInsert = new Apttus__APTS_Agreement__c();
        aggToInsert.Name = 'APTS Test Agreement 1111';
        aggToInsert.Program__c = 'NetJets U.S.';
        aggToInsert.Apttus__Account__c = accToInsert.Id;
        aggToInsert.Card_Number__c = '123456789';
        aggToInsert.APTS_Modification_Type__c = 'Expired Hours Card';
        aggToInsert.RecordTypeId = recordTypeIdNJUS;
        aggToInsert.Apttus_QPComply__RelatedProposalId__c = testProposal.Id;
        aggList.add(aggToInsert);

        List<Apttus_Config2__AssetLineItem__c> assetALIList = new List<Apttus_Config2__AssetLineItem__c>();
        Apttus_Config2__AssetLineItem__c  assetLi = new Apttus_Config2__AssetLineItem__c ();
        assetLi.Apttus_CMConfig__AgreementId__c = aggList[0].Id;
        assetLi.Apttus_Config2__ChargeType__c = 'Expired Hours Value';
        assetLi.Apttus_Config2__LineType__c = 'Option';
        insert assetLi;

        //Create Asset PAVs
        Apttus_Config2__AssetAttributeValue__c aPAV = new Apttus_Config2__AssetAttributeValue__c();
        aPAV.Apttus_Config2__AssetLineItemId__c = assetLi.Id;
        aPAV.APTS_Aircraft_Expired_Hours__c = 25;
        aPAV.APTS_Overridden_Hours_To_Unexpire__c = 10;
        aPAV.APTPS_Is_Expired_Hours_Reduced__c = false;
        aPAV.APTPS_Stop_Integration_Updates__c = false;

        insert aPAV;
        assetLi.Apttus_Config2__AttributeValueId__c = aPAV.id;
        update assetLi;
        assetALIList.add(assetLi);
        
        
           
       System.debug('PAV APTS_Aircraft_Expired_Hours__c -->'+ assetALIList[0].Apttus_Config2__AttributeValueId__r.APTS_Aircraft_Expired_Hours__c);

        //Start Test
        Test.startTest();

          APTS_AssetLineItemBaseTriggerHelper.updateEHCOnParentAssetPAV(assetALIList);
          system.assert(assetALIList[0].Apttus_Config2__AttributeValueId__r.APTS_Aircraft_Expired_Hours__c!=15);
          System.debug('PAV APTS_Aircraft_Expired_Hours__c -->'+ assetALIList[0].Apttus_Config2__AttributeValueId__r.APTS_Aircraft_Expired_Hours__c);

          //Stop Test
        Test.stopTest();
                
    } */
    
    static void updateEHCOnParentAssetPAVTest(){
      List<Account> accList = new List<Account>();
      Id recordTypeIdNJUS = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('NJUS New Card Sale').getRecordTypeId();
     
      Account accToInsert = new Account();
      accToInsert.Name = 'APTS Test Account';
      accToInsert.APTS_Has_Active_NJUS_Agreement__c = true;
      accToInsert.APTS_Has_Active_NJE_Agreement__c = true;
      insert accToInsert;

      Opportunity testOpportunity = APTS_CPQTestUtility.createOpportunity('test Apttus Opportunity', 'New Customer', accToInsert.Id, 'Propose');
      insert testOpportunity;

      Apttus_Config2__PriceList__c testPriceList = APTS_CPQTestUtility.createPriceList('test Apttus Price List', true);
      insert testPriceList;

      Apttus_Proposal__Proposal__c testProposal = APTS_CPQTestUtility.createProposal('test Apttus Proposal', accToInsert.Id, testOpportunity.Id, 'Proposal', testPriceList.Id);
      //populate custom fields value if required
      testProposal.APTS_New_Sale_or_Modification__c = 'Modification';
      testProposal.APTS_Modification_Type__c = 'Expired Hour Card';
      insert testProposal;
      
            
      ID accID = accToInsert.Id;
      
       //New Sale      
      Apttus__APTS_Agreement__c newSaleAgg = new Apttus__APTS_Agreement__c();
      newSaleAgg.Name = 'APTS Test Agreement 1111';
      newSaleAgg.Program__c = 'NetJets U.S.';
      newSaleAgg.Apttus__Account__c = accToInsert.Id;
      newSaleAgg.Card_Number__c = String.valueof((Math.random() * 100));
      newSaleAgg.APTS_Modification_Type__c = 'Expired Hour Card';
      newSaleAgg.RecordTypeId = recordTypeIdNJUS;
      newSaleAgg.Apttus_QPComply__RelatedProposalId__c = testProposal.Id;
      newSaleAgg.Agreement_Status__c = 'Active';
    
      insert newSaleAgg;    
      
      
      
      List<Apttus_Config2__AssetLineItem__c> assetLIIds = new list<Apttus_Config2__AssetLineItem__c>();
      
      //Create Asset Line Item for New Card Sale Conversion       
      
      Apttus_Config2__AssetLineItem__c  NCSCAseetLI = new Apttus_Config2__AssetLineItem__c ();
      NCSCAseetLI.Apttus_CMConfig__AgreementId__c = newSaleAgg.Id;
      NCSCAseetLI.Apttus_Config2__ChargeType__c = 'Expired Hours Value';
      NCSCAseetLI.Apttus_Config2__LineType__c = 'Option';
      NCSCAseetLI.APTS_Original_Agreement__c = newSaleAgg.Id;
      insert NCSCAseetLI; 
     
      Apttus_Config2__AssetAttributeValue__c termAssetAttri = new Apttus_Config2__AssetAttributeValue__c();
      termAssetAttri.Apttus_Config2__AssetLineItemId__c = NCSCAseetLI.Id;
      termAssetAttri.APTPS_Is_Expired_Hours_Reduced__c = false;
      termAssetAttri.APTS_Aircraft_Expired_Hours__c = 25;
      termAssetAttri.APTS_Overridden_Hours_To_Unexpire__c = 10;
      termAssetAttri.APTS_Hours_To_Unexpire__c = 0;
      termAssetAttri.APTS_Override_Hours_To_Unexpire__c = true;
      termAssetAttri.APTPS_Stop_Integration_Updates__c = false;
      insert termAssetAttri;
      
      NCSCAseetLI.Apttus_Config2__AttributeValueId__c = termAssetAttri.Id;
      update NCSCAseetLI;

      assetLIIds.add(NCSCAseetLI);
      
      //Start Test
      Test.startTest();

      APTS_AssetLineItemBaseTriggerHelper.updateEHCOnParentAssetPAV(assetLIIds);
           
   
      //Stop Test
      Test.stopTest();
              
  }
    
    
}
