/**
 * @description       : Test class for APTPS_CertificateOfInsuranceRequestCtlr
 * @author            : Sagar Solanki
 * @group             : Apttus(Conga) Dev
 * @last modified on  : 01-21-2020
 * @last modified by  : Sagar Solanki
 *
 * Modifications Log
 * Ver   Date         Author          Modification
 * 1.0   01-21-2021   Sagar Solanki   Added test class : GCM-9876
 **/

@isTest
public with sharing class APTPS_CertificateOfInsuranceRequestTest {
	@isTest
	static void checkAccountLocation(){
        
        Account accToInsert = new Account();
		accToInsert.Name = 'APTS Test Account';
		accToInsert.APTS_Has_Active_NJUS_Agreement__c = true;
		accToInsert.APTS_Has_Active_NJE_Agreement__c = true;
		insert accToInsert;
        
    	Apttus_Config2__AccountLocation__c aLocation = new Apttus_Config2__AccountLocation__c();
        aLocation.Name = 'HQ';
        aLocation.Apttus_Config2__Street__c = 'First Street';
        aLocation.Apttus_Config2__AccountId__c = accToInsert.Id;
        insert aLocation;
        
        Apttus__APTS_Agreement__c aggToInsert = new Apttus__APTS_Agreement__c();
		aggToInsert.Name = 'APTS Test Agreement 1';
        aggToInsert.Apttus_CMConfig__LocationId__c = aLocation.Id;
		insert aggToInsert;
        
        Test.startTest();
        APTPS_CertificateOfInsuranceRequestCtlr ctr = new APTPS_CertificateOfInsuranceRequestCtlr();
        ctr.agreementLocationId = aLocation.Id;
        Apttus_Config2__AccountLocation__c addressLocation = ctr.aLocation;
        
        System.assertNotEquals(null, addressLocation);

		Test.stopTest();
    }
}