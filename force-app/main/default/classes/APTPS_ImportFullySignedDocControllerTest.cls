@isTest
private class APTPS_ImportFullySignedDocControllerTest {

    static testMethod void myUnitTest() {
        
        //Create a new Account
        Account testacc = new Account();
        testacc.Name = 'Test Account';
        
        insert testacc;
        
        //Create new agreement
        Apttus__APTS_Agreement__c agreement = new Apttus__APTS_Agreement__c();
        agreement.Apttus__Account__c = testacc.Id;
        
        insert agreement;

        //Start Test
        Test.startTest();
        
        //Initialize the page
        PageReference ImportFullySignedPage = new PageReference('/apex/ImportFullySgnedDocument?id='+agreement.Id);
      	Test.setCurrentPage(ImportFullySignedPage);
      	ApexPages.StandardController sc = new ApexPages.StandardController(agreement);
        APTPS_ImportFullySignedDocController Ctrl = new APTPS_ImportFullySignedDocController(sc);
        
        //execute finalize method
        Ctrl.finalize();
        
        //Stop Test
        Test.stopTest();
        
        //Query agreement record
        Apttus__APTS_Agreement__c agreementResult = [select Id, Apttus__Status_Category__c, Apttus__Status__c from Apttus__APTS_Agreement__c where Id = :agreement.Id];
        
        //Assert that the agreement is in signatures status category and fully signed status
        System.assertEquals(agreementResult.Apttus__Status_Category__c,'In Signatures');
        System.assertEquals(agreementResult.Apttus__Status__c,'Fully Signed');
    }    
}