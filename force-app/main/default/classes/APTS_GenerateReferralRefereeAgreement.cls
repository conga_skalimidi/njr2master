/*******************************************************************************************************************************************
@Name: APTS_GenerateReferralRefereeAgreement
@Author: Avinash Bamane
@CreateDate: 28/11/2019
@Description: In case of Referral and Referee scenario, there is need to generate Agreement with existing Line Items.
This APEX will be called from flow - 'Split_Master_Agreement'. 
********************************************************************************************************************************************
@ModifiedBy: Avinash Bamane
@ModifiedDate: 23/09/2020
@ChangeDescription: GCM-9275 - Added condition to check for Elite Combo and Classic Combo.
********************************************************************************************************************************************
@ModifiedBy: Avinash Bamane
@ModifiedDate: 09/10/2020
@ChangeDescription: GCM-9732 - Stamp Produt Type
********************************************************************************************************************************************
@ModifiedBy: Avinash Bamane
@ModifiedDate: 04/11/2020
@ChangeDescription: GCM-9780 - Updates for test coverage. Restricting Pricing Engine and Cart Finalization API calls.
********************************************************************************************************************************************
@ModifiedBy: Avinash Bamane
@ModifiedDate: 16/02/2021
@ChangeDescription: GCM-10113 - In case of Migrated Agreement, Referrer agreement generation fails.
********************************************************************************************************************************************/

global class APTS_GenerateReferralRefereeAgreement {
    
    public class APTS_GenerateReferralRefereeAgreementRequest{
        @InvocableVariable(required=true)
        public Id cartId;
        
        @InvocableVariable(required=true)
        public String productProgram;
        
        @InvocableVariable(required=true)
        public Id agreementId;
        
        @InvocableVariable(required=true)
        public Id bundleProductId;
        
        @InvocableVariable(required=true)
        public Double aircraftHours;
        
        @InvocableVariable(required=true)
        public Id locationId;
        
        @InvocableVariable(required=false)
        public String modificationType;
        
        @InvocableVariable(required=false)
        public String termProductCode;
        
        @InvocableVariable(required=false)
        public Id referralCardId;
        
        @InvocableVariable(required=false)
        public String legalEntityName;
        
        @InvocableVariable(required=false)
        public String referralName;
        
        //START: GCM-10113
        @InvocableVariable(required=false)
        public boolean isMigrated;
    }
    
    @InvocableMethod(label='Generate Agreement Line Items' description='Generates An Agreement Line Items')
    public static void generateAgreement (APTS_GenerateReferralRefereeAgreementRequest[] generateAgreementRequests) {
        
        for(APTS_GenerateReferralRefereeAgreementRequest input : generateAgreementRequests) {
            try{
                //START: GCM-10113 Pass isMigrated flag to the functions
                String productTypeCode = getProductTypeCode(input.agreementId, input.modificationType, input.isMigrated);
                String aircraftProductCode = getAircraftProductCode(input.agreementId, productTypeCode, input.modificationType, input.isMigrated);
                generateReferralCardAgreement(input.cartId, input.bundleProductId, input.productProgram, productTypeCode, 
                                              aircraftProductCode, input.aircraftHours, input.termProductCode, 
                                              input.legalEntityName, input.referralCardId, input.locationId, input.referralName);
            } catch(Exception e) {
                system.debug('Error while generating referral/referee agreements --> '+e.getMessage());
            }
        }   
        
    } 
    
    private static String getProductTypeCode (Id aggId, String modificationType, boolean isMigrated) {
        system.debug('getProductTypeCode --> '+'aggId '+aggId+' modificationType '+modificationType+' isMigrated '+isMigrated);
        String productCode = '';
        //START: GCM-10113 Condition is updated for migrated agreements.
        if (modificationType != null && modificationType == 'Add Enhancements' && 
           isMigrated != null && !isMigrated) {
            Apttus__AgreementLineItem__c ali = [SELECT Apttus_CMConfig__OptionId__r.ProductCode 
                                                FROM Apttus__AgreementLineItem__c 
                                                WHERE Apttus__AgreementId__r.Id =: aggId 
                                                AND Option_Product_Family_System__c = 'Product Type' 
                                                AND Apttus_CMConfig__LineStatus__c != 'New'
                                                AND Apttus_CMConfig__IsPrimaryLine__c = TRUE];
            productCode = ali.Apttus_CMConfig__OptionId__r.ProductCode; 
        } else {
            Apttus__AgreementLineItem__c ali = [SELECT Apttus_CMConfig__OptionId__r.ProductCode 
                                                FROM Apttus__AgreementLineItem__c 
                                                WHERE Apttus__AgreementId__r.Id =: aggId 
                                                AND Option_Product_Family_System__c = 'Product Type' 
                                                AND Apttus_CMConfig__LineStatus__c = 'New'
                                                AND Apttus_CMConfig__IsPrimaryLine__c = TRUE];
            productCode = ali.Apttus_CMConfig__OptionId__r.ProductCode; 
        }
        return productCode;
    }
    
    private static String getAircraftProductCode (Id aggId, String productTypeCode, String modificationType, boolean isMigrated) {
        system.debug('getAircraftProductCode --> '+'aggId '+aggId+' productTypeCode '+productTypeCode+' modificationType '+modificationType+' isMigrated '+isMigrated);
        String acProductCode = '';
        //START:GCM-10113 Condition is updated to consider migrated agreements.
        if ((modificationType != null && modificationType == 'Add Enhancements') || 
            (isMigrated != null && isMigrated)) {
            if (productTypeCode != null && (productTypeCode == 'COMBO_NJUS' || productTypeCode == 'COMBO_NJE' || productTypeCode == 'CLASSIC_COMBO_NJUS' || productTypeCode == 'ELITE_COMBO_NJUS')) {
                List<Apttus__AgreementLineItem__c> aircraftALIs = [SELECT Apttus_CMConfig__OptionId__r.ProductCode, Apttus_CMConfig__OptionId__r.Cabin_Class_Rank__c 
                                                                   FROM Apttus__AgreementLineItem__c 
                                                                   WHERE Apttus__AgreementId__r.Id =: aggId 
                                                                   AND Option_Product_Family_System__c = 'Aircraft'
                                                                   AND Apttus_CMConfig__LineStatus__c != 'New'
                                                                   AND Apttus_CMConfig__IsPrimaryLine__c = TRUE
                                                                   order by Apttus_CMConfig__OptionId__r.Cabin_Class_Rank__c desc];
                if (!aircraftALIs.isEmpty()) {
                    acProductCode = aircraftALIs[0].Apttus_CMConfig__OptionId__r.ProductCode;
                }
            } else {
                Apttus__AgreementLineItem__c ali = [SELECT Apttus_CMConfig__OptionId__r.ProductCode 
                                                    FROM Apttus__AgreementLineItem__c 
                                                    WHERE Apttus__AgreementId__r.Id =: aggId 
                                                    AND Option_Product_Family_System__c = 'Aircraft'
                                                    AND Apttus_CMConfig__LineStatus__c != 'New'
                                                    AND Apttus_CMConfig__IsPrimaryLine__c = TRUE]; 
                acProductCode = ali.Apttus_CMConfig__OptionId__r.ProductCode;
            }
        } else {
            //GCM-9275: Added condition to check for Elite Combo and Classic Combo
            if (productTypeCode != null && (productTypeCode == 'COMBO_NJUS' || productTypeCode == 'COMBO_NJE' || productTypeCode == 'CLASSIC_COMBO_NJUS' || productTypeCode == 'ELITE_COMBO_NJUS')) {
                List<Apttus__AgreementLineItem__c> aircraftALIs = [SELECT Apttus_CMConfig__OptionId__r.ProductCode, Apttus_CMConfig__OptionId__r.Cabin_Class_Rank__c 
                                                                   FROM Apttus__AgreementLineItem__c 
                                                                   WHERE Apttus__AgreementId__r.Id =: aggId 
                                                                   AND Option_Product_Family_System__c = 'Aircraft' 
                                                                   AND Apttus_CMConfig__LineStatus__c = 'New'
                                                                   AND Apttus_CMConfig__IsPrimaryLine__c = TRUE
                                                                   order by Apttus_CMConfig__OptionId__r.Cabin_Class_Rank__c desc];
                if (!aircraftALIs.isEmpty()) {
                    acProductCode = aircraftALIs[0].Apttus_CMConfig__OptionId__r.ProductCode;
                }
            } else {
                //GCM-9275: In case of non-Combo, ideally there should be one aircraft with existing agreement.
                //Adding 'LIMIT 1' clause to the SOQL query in case if agreement has more than 1 aircraft and Product Type is Non-Combo.
                //This is additional logic/catch to avoid execption/failure.
                Apttus__AgreementLineItem__c ali = [SELECT Apttus_CMConfig__OptionId__r.ProductCode 
                                                    FROM Apttus__AgreementLineItem__c 
                                                    WHERE Apttus__AgreementId__r.Id =: aggId 
                                                    AND Option_Product_Family_System__c = 'Aircraft' 
                                                    AND Apttus_CMConfig__LineStatus__c = 'New'
                                                    AND Apttus_CMConfig__IsPrimaryLine__c = TRUE LIMIT 1]; 
                acProductCode = ali.Apttus_CMConfig__OptionId__r.ProductCode;
            }
        }
        return acProductCode;
    }
    
    @future
    public static void generateReferralCardAgreement(Id cartId, Id bundleProductId, String productProgram, String productTypeCode, 
                                                     String aircraftProductCode, Double aircraftHours, String termProductCode, 
                                                     String legalEntityName, Id referralCardId, Id locationId, String referralName) {
                                                         system.debug('Flow variables --> Cart Id-'+cartId+' bundleProductId-'+bundleProductId+' productProgram-'+productProgram
                                                                     +' productTypeCode-'+productTypeCode+' aircraftProductCode-'+aircraftProductCode
                                                                     +' aircraftHours-'+aircraftHours+' termProductCode-'+termProductCode
                                                                     +' legalEntityName-'+legalEntityName+' referralCardId-'+referralCardId
                                                                     +' locationId-'+locationId+' referralName-'+referralName);                                                         
                                                         String aircraftName = '';
                                                         String productTypeName = '';
                                                         // Add Bundle Product, Options to the Cart

                                                         Apttus_CPQApi.CPQ.AddBundleRequestDO requestBundle = new Apttus_CPQApi.CPQ.AddBundleRequestDO();
                                                         requestBundle.CartId = cartId;
                                                         requestBundle.SelectedBundle = new Apttus_CPQApi.CPQ.SelectedBundleDO();
                                                         requestBundle.SelectedBundle.SelectedProduct = new Apttus_CPQApi.CPQ.SelectedProductDO();
                                                         requestBundle.SelectedBundle.SelectedProduct.ProductId = bundleProductId;
                                                         requestBundle.SelectedBundle.SelectedProduct.Quantity = 1;
                                                         
                                                         // Pass Product Type Option Group ID
                                                         Apttus_Config2__ProductOptionGroup__c prodOpt = [SELECT Id, Name 
                                                                                                          FROM Apttus_Config2__ProductOptionGroup__c 
                                                                                                          WHERE Apttus_Config2__ProductId__r.Id =: bundleProductId 
                                                                                                          AND Apttus_Config2__OptionGroupId__r.Name =: productProgram];
                                                         Apttus_CPQApi.CPQ.ProductOptionGroupSearchResultDO productOptionGroupResult = 
                                                             Apttus_CPQApi.CPQWebService.getOptionGroupsForPriceListProduct(prodOpt.Id, bundleProductId);
                                                         
                                                         if(productOptionGroupResult.HasOptionGroups) {
                                                             requestBundle.SelectedBundle.SelectedOptions = new List<Apttus_CPQApi.CPQ.SelectedOptionDO>();
                                                             for(Apttus_CPQApi.CPQ.ProductOptionGroupDO productOptionGroup : productOptionGroupResult.OptionGroups) {
                                                                 
                                                                 if(productOptionGroup.HasOptionComponents) {
                                                                     for(Apttus_CPQApi.CPQ.ProductOptionComponentDO
                                                                         productOptionComponent : productOptionGroup.OptionComponents) {
                                                                             
                                                                             if(productOptionComponent.ProductCode == productTypeCode) {
                                                                                 Apttus_CPQApi.CPQ.SelectedOptionDO selectedOptionDO = new Apttus_CPQApi.CPQ.SelectedOptionDO();
                                                                                 selectedOptionDO.ComponentId = productOptionComponent.ComponentId;
                                                                                 selectedOptionDO.ComponentProductId = productOptionComponent.ComponentProductId;
                                                                                 //START: GCM-9732
                                                                                 productTypeName = productOptionComponent.Name;
                                                                                 //END: GCM-9732
                                                                                 requestBundle.SelectedBundle.SelectedOptions.add(selectedOptionDO);
                                                                             }
                                                                             
                                                                             if(productOptionComponent.ProductCode == aircraftProductCode) {
                                                                                 Apttus_CPQApi.CPQ.SelectedOptionDO selectedOptionDO = new Apttus_CPQApi.CPQ.SelectedOptionDO();
                                                                                 selectedOptionDO.ComponentId = productOptionComponent.ComponentId;
                                                                                 selectedOptionDO.ComponentProductId = productOptionComponent.ComponentProductId;
                                                                                 selectedOptionDO.Quantity = aircraftHours;
                                                                                 aircraftName = productOptionComponent.Name;
                                                                                 requestBundle.SelectedBundle.SelectedOptions.add(selectedOptionDO);
                                                                             }
                                                                             
                                                                             if(productOptionComponent.ProductCode == termProductCode) {
                                                                                 Apttus_CPQApi.CPQ.SelectedOptionDO selectedOptionDO = new Apttus_CPQApi.CPQ.SelectedOptionDO();
                                                                                 selectedOptionDO.ComponentId = productOptionComponent.ComponentId;
                                                                                 selectedOptionDO.ComponentProductId = productOptionComponent.ComponentProductId;
                                                                                 requestBundle.SelectedBundle.SelectedOptions.add(selectedOptionDO);
                                                                             }
                                                                             
                                                                         }
                                                                 }
                                                                 
                                                             }
                                                         }
                                                         
                                                         Apttus_CPQApi.CPQ.AddBundleResponseDO responseAddBundle = Apttus_CPQApi.CPQWebService.addBundle(requestBundle);
                                                         // End - Add Bundle Product, Options to the Cart
                                                         
                                                         /* REMOVED THIS LOGIC AND ADDED IT AT THE END OF THE EXECUTION - To avoid flow error.
                                                         // Update - Referral Card Agreement Name
                                                         String locationName = legalEntityName != null ? legalEntityName : '';
                                                         String refAgreementName = '';
                                                         refAgreementName = locationName+', '+referralName+'-'+String.valueOf(aircraftHours)+','+aircraftName;
                                                         if(!String.isBlank(refAgreementName)) {
                                                             Apttus__APTS_Agreement__c refAgreement = [SELECT Name
                                                                                                       FROM Apttus__APTS_Agreement__c
                                                                                                       WHERE Id=: referralCardId];
                                                             refAgreement.Name = refAgreementName;
                                                             refAgreement.Document_Name__c = refAgreementName;
                                                             refAgreement.APTS_Total_Aircraft_Hours__c= aircraftHours;
                                                             refAgreement.APTS_New_Sale_or_Modification__c = 'New Sale';
                                                             update refAgreement;
                                                         }
                                                         // End - Referral Card Agreement Name
                                                         */
                                                         // Add Quantity After Pricing
                                                         Apttus_Config2__ProductAttributeValue__c aircraftLineItem = [SELECT Aircraft_Hours__c
                                                                                                                      FROM Apttus_Config2__ProductAttributeValue__c 
                                                                                                                      WHERE Apttus_Config2__LineItemId__r.Apttus_Config2__ConfigurationId__r.Id =: cartId 
                                                                                                                      AND Apttus_Config2__LineItemId__r.Apttus_Config2__IsPrimaryLine__c = TRUE 
                                                                                                                      AND Apttus_Config2__LineItemId__r.Apttus_Config2__OptionId__r.ProductCode =: aircraftProductCode];
                                                         if(aircraftLineItem != null) {
                                                             aircraftLineItem.Aircraft_Hours__c = aircraftHours;
                                                             update aircraftLineItem;
                                                         }
                                                         
                                                         Apttus_Config2__ProductAttributeValue__c termLineItem = [SELECT Initial_Term_Amount_Months__c, Delayed_Start_Amount_Months__c, Grace_Period_Amount_Months__c 
                                                                                                                  FROM Apttus_Config2__ProductAttributeValue__c 
                                                                                                                  WHERE Apttus_Config2__LineItemId__r.Apttus_Config2__ConfigurationId__r.Id =: cartId 
                                                                                                                  AND Apttus_Config2__LineItemId__r.Apttus_Config2__IsPrimaryLine__c = TRUE 
                                                                                                                  AND Apttus_Config2__LineItemId__r.Apttus_Config2__OptionId__r.ProductCode =: termProductCode];
                                                         
                                                         if(termLineItem != null) {
                                                             APTS_Refereer_Referee_Terms__c termValues = APTS_Refereer_Referee_Terms__c.getOrgDefaults();
                                                             termLineItem.Delayed_Start_Amount_Months__c = String.valueOf(termValues.APTS_Delayed_Start_Amount_Months__c);
                                                             termLineItem.Initial_Term_Amount_Months__c = String.valueOf(termValues.APTS_Initial_Term_Amount_Months__c);
                                                             termLineItem.Grace_Period_Amount_Months__c = String.valueOf(termValues.APTS_Grace_Period_Amount_Months__c);
                                                             update termLineItem;
                                                         }
                                                         
                                                         // Stamp Legal Entity on all line items 
                                                         List<Apttus_Config2__LineItem__c> lineItems = [SELECT Apttus_Config2__LocationId__c,APTPS_Referral_Referee_Type__c, APTS_New_Sale_or_Modification__c 
                                                                                                        FROM Apttus_Config2__LineItem__c
                                                                                                        WHERE Apttus_Config2__ConfigurationId__r.Id =: cartId];
                                                         for(Apttus_Config2__LineItem__c li : lineItems) {
                                                             li.Apttus_Config2__LocationId__c = locationId;
                                                             li.APTS_New_Sale_or_Modification__c = 'New Sale';
                                                             li.APTPS_Referral_Referee_Type__c = referralName;
                                                         }
                                                         update lineItems;
                                                         // End - Stamp Legal Entity on all line items
                                                         
                                                         // Do pricing for newly generated cart
                                                         //START: GCM-9780 - Do not call Pricing Engine in testing mode. This will not impact business logic.
                                                         if(!Test.isRunningTest()) {
                                                             boolean hasPendingItems = false;
                                                             do {
                                                                Apttus_CpqApi.CPQ.UpdatePriceRequestDO objUpdatePriceRequestDO = new Apttus_CpqApi.CPQ.UpdatePriceRequestDO();
                                                                objUpdatePriceRequestDO.CartId = cartId;
                                                                Apttus_CpqApi.CPQ.UpdatePriceResponseDO result = Apttus_CpqApi.CPQWebService.updatePriceForCart(objUpdatePriceRequestDO);
                                                                hasPendingItems = result.IsPricePending;
                                                             } while(hasPendingItems);
                                                         }
                                                         //End - Do pricing for newly generated cart
                                                         
                                                         // Finalize the newly generated cart
                                                         //START: GCM-9780 - Do not call Cart Finalization API in testing mode. This will not impact business logic.
                                                         if(!Test.isRunningTest()) {
                                                             Apttus_CpqApi.CPQ.FinalizeCartRequestDO requestFinalize = new Apttus_CpqApi.CPQ.FinalizeCartRequestDO();
                                                             requestFinalize.CartId = cartId;
                                                             Apttus_CpqApi.CPQ.FinalizeCartResponseDO responseFinalize = Apttus_CpqApi.CPQWebService.finalizeCart(requestFinalize);
                                                         }
                                                         //End - Finalize the newly generated cart
                                                         
                                                         // Update - Referral Card Agreement Name
                                                         String locationName = legalEntityName != null ? legalEntityName : '';
                                                         String refAgreementName = '';
                                                         refAgreementName = locationName+', '+referralName+'-'+String.valueOf(aircraftHours)+','+aircraftName;
                                                         if(!String.isBlank(refAgreementName)) {
                                                           /*  Apttus__APTS_Agreement__c refAgreement = [SELECT Name
                                                                                                       FROM Apttus__APTS_Agreement__c
                                                                                                       WHERE Id=: referralCardId];
                                                             refAgreement.Name = refAgreementName;
                                                             refAgreement.Document_Name__c = refAgreementName;
                                                             refAgreement.APTS_Total_Aircraft_Hours__c= aircraftHours;
                                                             refAgreement.APTS_New_Sale_or_Modification__c = 'New Sale';
                                                             update refAgreement;*/
                                                             //START: GCM-9732, new parameter 'productTypeName' is introduced.
                                                             System.enqueueJob(new APTS_UpdateRefAgrQueueble(referralCardId, refAgreementName, productTypeName));
                                                         }
                                                         // End - Referral Card Agreement Name
                                                         
                                                     }
    
}