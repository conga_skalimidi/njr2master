/*
* Class Name: APTS_AccountUpdateQueueable
* Description: 
* 1. Queueable class to link Batch Execution with Account Trigger
* Reason - CDMAccountTriggerHelper has 'future' method implementation. So, updating accoounts from Bacth is giving exception.
* So, implementing this Queuable class to link Batch and Trigger Execution.
* Created By: DEV Team, Apttus

* Modification Log
* ------------------------------------------------------------------------------------------------------------------------------------------------------------
* Developer               Date           US/Defect        Description
* ------------------------------------------------------------------------------------------------------------------------------------------------------------
* Avinash Bamane		 16/12/19		  GCM-6804 			 Added
*/

public class APTS_AccountUpdateQueueable  implements Queueable {
    private List<Account> accList = new List<Account>();
    
    // Constructor: Accept Account List to update
    public APTS_AccountUpdateQueueable(List<Account> listToUpdate) {
        accList = listToUpdate;
    }
    
    public void execute(QueueableContext context) {
        // Update Accounts
        try {
            if(!accList.isEmpty()) {
                update accList;
            }
        } catch(DmlException e) {
            system.debug('Exception while updating Account list '+e);
        } 
    }
}