public with sharing class AcctTeamAgreementPETriggerHelper {
    
    public static void CreateAccTTeamMembersPlatformEvent(Map<Id, AccountTeamMember> newMap){

        // List to hold event objects to be published.
        List<AccountTeamMemberEvent__e> accTeamMemberEventList = new List<AccountTeamMemberEvent__e >();

        for (AccountTeamMember  teamMemberRole: newMap.values()) {

            // Create an instance of the event and store it in the acctTeamEvent variable
            AccountTeamMemberEvent__e acctTeamEvent = new AccountTeamMemberEvent__e(
                AccountId__c = teamMemberRole.AccountId,
                TeamMemberRole__c = teamMemberRole.TeamMemberRole,
                Team_Member_User_ID__c = teamMemberRole.UserId);
                    
            accTeamMemberEventList.add(acctTeamEvent);
                
        }
                    
        // Call method to publish events.
        List<Database.SaveResult> results = EventBus.publish(accTeamMemberEventList);
        // Inspect publishing result for each event
        for (Database.SaveResult sr : results) {
            if (sr.isSuccess()) {
                System.debug('Successfully published accTeamMemberEvents.');
            } else {
                for(Database.Error err : sr.getErrors()) {
                    System.debug('Error returned: ' + err.getStatusCode() + ' - ' + err.getMessage());
                }
            }
        }
        
    }
    
}
