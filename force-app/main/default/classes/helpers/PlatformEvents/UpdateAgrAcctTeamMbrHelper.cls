/*
* Class Name: APTS_AccountUpdateQueueable
* Description: 
* 1. Queueable class is launched from Platform Event to update agreements with Account Team Members

* Modification Log
* ------------------------------------------------------------------------------------------------------------------------------------------------------------
* Developer               Date           US/Defect        Description
* ------------------------------------------------------------------------------------------------------------------------------------------------------------
* Ramesh Kumar		    03/10/21		  GCM-10171 			 Added
*/

public with sharing class UpdateAgrAcctTeamMbrHelper {
    private static Id account_Executive;
    private static Id associated_Sales_Executive;
    private static Id primary_Sales_Executive;
    private static Id sales_Executive;
    private static Id SalesDirector_Rvp;
    private static Id sales_Consultant;
    private static Id secoundary_Sales_Executive;
    private static List<AccountTeamMember> acctTeamMemberNJAList = new List<AccountTeamMember>();
    private static List<AccountTeamMember> acctTeamMemberNJEList = new List<AccountTeamMember>();
    private static List<Apttus__APTS_Agreement__c> agNJAList =  new List<Apttus__APTS_Agreement__c>();
    private static List<Apttus__APTS_Agreement__c> agNJEList =  new List<Apttus__APTS_Agreement__c>();
    private static List<Apttus__APTS_Agreement__c> updateAgList = new List<Apttus__APTS_Agreement__c>();
    private static Set<Id> accountIds = new Set<Id>();

    
    public static void UpdateAgreementsWithAcctTeamMembers(List<AccountTeamMemberEvent__e> acctTeamMbrMapping) {
             
        for(AccountTeamMemberEvent__e teamMember : acctTeamMbrMapping){
            if(teamMember.AccountId__c != null){
                accountIds.add(teamMember.AccountId__c);
            }
        }         

        if(accountIds.size() > 0){
            List<AccountTeamMember> teamMembers = [
                SELECT Id, AccountId, TeamMemberRole, UserID, NetJets_Company__c 
                FROM AccountTeamMember 
                WHERE (NetJets_Company__c = 'NJA' OR NetJets_Company__c = 'NJE') AND AccountId IN :accountIds
            ];
            
            for(AccountTeamMember teamMember : teamMembers){
                if(teamMember.NetJets_Company__c == 'NJA'){
                    acctTeamMemberNJAList.add(teamMember);
                }else if(teamMember.NetJets_Company__c == 'NJE'){
                    acctTeamMemberNJEList.add(teamMember);
                }
            }

            List<Apttus__APTS_Agreement__c> agreements = [
                SELECT Id, Apttus__Account__c, CurrencyIsoCode, Account_Executive__c, APTS_Associate_Sales_Executive__c, 
                Primary_Sales_Executive__c,RVP__c, Sales_Consultant__c, Secondary_Sales_Executive__c, Name 
                FROM Apttus__APTS_Agreement__c 
                WHERE (CurrencyIsoCode = 'USD' OR CurrencyIsoCode = 'EUR') AND Apttus__Account__c IN :accountIds FOR UPDATE
            ];
            
            for(Apttus__APTS_Agreement__c agreement : agreements){
                if(agreement.CurrencyIsoCode == 'USD'){
                    agNJAList.add(agreement);
                }else if(agreement.CurrencyIsoCode == 'EUR'){
                    agNJEList.add(agreement);
                }
            }

            processAgreements(agNJAList, acctTeamMemberNJAList);
            processAgreements(agNJEList, acctTeamMemberNJEList);

            if(updateAgList.size() > 0){
                update updateAgList;
            }

        }

    }

    public static void processAgreements(List<Apttus__APTS_Agreement__c> agList, List<AccountTeamMember> acctTeamMemberList) {
        for(Id accountId : accountIds) {
            if(accountId != null){
                if(acctTeamMemberList.size() > 0){
                    clearTeamRoles();
                    populateAccountTeamRoles(accountId, acctTeamMemberList);

                    if(agList.size() > 0){
                        UpdateAgreements(accountId, agList);
                    }
                }
            }
        }
    }

    public static void populateAccountTeamRoles(Id accountId, List<AccountTeamMember> acctTeamMemberList){

        for(AccountTeamMember acctTeam : acctTeamMemberList){
            if(acctTeam.AccountId != null && acctTeam.AccountId == accountId){

                if(acctTeam.TeamMemberRole == 'Sales Executive - Primary'){
                    primary_Sales_Executive = acctTeam.UserId;
                    
                }if(acctTeam.TeamMemberRole == 'Account Executive - Primary'){
                    account_Executive = acctTeam.UserId;
                    
                }if(acctTeam.TeamMemberRole == 'Associate Sales Executive'){
                    associated_Sales_Executive = acctTeam.UserId;
                    
                }if(acctTeam.TeamMemberRole == 'Sales Director/RVP'){
                    SalesDirector_Rvp = acctTeam.UserId;
                    
                }if(acctTeam.TeamMemberRole == 'Sales Consultant'){
                    sales_Consultant = acctTeam.UserId;
                    
                }if(acctTeam.TeamMemberRole == 'Sales Executive - Secondary'){
                    secoundary_Sales_Executive = acctTeam.UserId;
                }
            }
        }
    }

    public static void UpdateAgreements(Id accountId, List<Apttus__APTS_Agreement__c> agList){

        for(Apttus__APTS_Agreement__c agr : agList){
            if(agr.Apttus__Account__c != null && agr.Apttus__Account__c == accountId){

                Apttus__APTS_Agreement__c agrAdd = new Apttus__APTS_Agreement__c(id = agr.id);

                agrAdd.Account_Executive__c = account_Executive;
                agrAdd.APTS_Associate_Sales_Executive__c = associated_Sales_Executive;
                agrAdd.Primary_Sales_Executive__c = primary_Sales_Executive;
                agrAdd.RVP__c = SalesDirector_Rvp;
                agrAdd.Sales_Consultant__c = sales_Consultant;
                agrAdd.Secondary_Sales_Executive__c = secoundary_Sales_Executive;
                updateAgList.add(agrAdd);
                
            }
        }
        
   }

   public static void clearTeamRoles(){
        account_Executive = null;
        associated_Sales_Executive = null;
        primary_Sales_Executive = null;
        sales_Executive = null;
        SalesDirector_Rvp = null;
        sales_Consultant = null;
        secoundary_Sales_Executive = null;
    }
}