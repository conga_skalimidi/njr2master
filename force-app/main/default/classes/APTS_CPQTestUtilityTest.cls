/*
 * This Apex class is Test Class for apex class - APTS_CPQTestUtility
 * 
 */
@isTest
public class APTS_CPQTestUtilityTest {
    
    static testMethod void testMethod1() {
        try{
            test.StartTest();
            
            Account testAccount = APTS_CPQTestUtility.createAccount('test apttus account', 'Enterprise');
            //populate custom fields value if required
            insert testAccount;
            system.assert(testAccount.Name == 'test apttus account');
            
            Contact testContact = APTS_CPQTestUtility.createContact('ContactFN', 'Apttus', testAccount.Id);
            //populate custom fields value if required
            insert testContact;
            system.assert(testContact.Id != null);
            
            Opportunity testOpportunity = APTS_CPQTestUtility.createOpportunity('test Apttus Opportunity', 'New Customer', testAccount.Id, 'Propose');
            //populate custom fields value if required
            insert testOpportunity;
            system.assert(testOpportunity.Name == 'test Apttus Opportunity');
            
            Apttus_Config2__PriceList__c testPriceList = APTS_CPQTestUtility.createPriceList('test Apttus Price List', true);
            //populate custom fields value if required
            insert testPriceList;
            system.assert(testPriceList.Name == 'test Apttus Price List');
            
            Product2 testProduct = APTS_CPQTestUtility.createProduct('test Apttus Product', 'APTS', 'Apttus', 'Bundle', true, true, true, true);
            //populate custom fields value if required
            insert testProduct;
            system.assert(testProduct.Name == 'test Apttus Product');
            
            PricebookEntry testPBE = APTS_CPQTestUtility.createPricebookEntry(Test.getStandardPricebookId(), testProduct.Id, 100, true);
            //populate custom fields value if required
            insert testPBE;
            system.assert(testPBE.Id != null);
            
            Apttus_Config2__PriceListItem__c testPriceListItem = APTS_CPQTestUtility.createPriceListItem(testPriceList.Id, testProduct.Id, 100, 'Standard Price', 'One Time', 'Per Unit', 'Each', true);
            //populate custom fields value if required
            insert testPriceListItem;
            system.assert(testPriceListItem.Id != null);
            
            Apttus_Proposal__Proposal__c testProposal = APTS_CPQTestUtility.createProposal('test Apttus Proposal', testAccount.Id, testOpportunity.Id, 'Proposal', testPriceList.Id);
            //populate custom fields value if required
            insert testProposal;
            system.assert(testProposal.Id != null);
            
            String testConfigurationId = APTS_CPQTestUtility.createConfiguration(testProposal.Id);
            
            system.assert(testConfigurationId != null);
            
            Apttus_Config2__ClassificationName__c testClassification = APTS_CPQTestUtility.createCategory('test Apttus Classification', 'test Hiearchy Label', 'Apttus', null, true);
            //populate custom fields value if required
            insert testClassification;
            system.assert(testClassification.Name == 'test Apttus Classification');
            
            Apttus_Config2__ClassificationHierarchy__c testClassificationHierarchy = APTS_CPQTestUtility.createClassificationHierarchy(testClassification.Id, 'Classification Hierarchy Label');
            //populate custom fields value if required
            insert testClassificationHierarchy;
            system.assert(testClassificationHierarchy.Id != null);
            
            Apttus_Config2__ProductOptionComponent__c testProductOptionComponent = APTS_CPQTestUtility.createProductOptionComponent(1);
            //populate custom fields value if required
            insert testProductOptionComponent;
            system.assert(testProductOptionComponent.Id != null);
            
            Apttus_Config2__SummaryGroup__c testSummaryGroup = APTS_CPQTestUtility.createSummaryGroup(testConfigurationId, 1, 1);
            //populate custom fields value if required
            insert testSummaryGroup;
            system.assert(testSummaryGroup.Id != null);
            
            APTS_CPQTestUtility.createLineItem(testConfigurationId, testProduct.Id, 1);
            list<Apttus_Config2__LineItem__c> listLineItem = [SELECT Id FROM Apttus_Config2__LineItem__c WHERE Apttus_Config2__ConfigurationId__c = :testConfigurationId];
            system.assert(listLineItem != null);
            
            APTS_CPQTestUtility.createProposalLineItem(testConfigurationId);
            list<Apttus_Proposal__Proposal_Line_Item__c> listProposalLineItem = [SELECT Id FROM Apttus_Proposal__Proposal_Line_Item__c WHERE Apttus_QPConfig__ConfigurationId__c = :testConfigurationId];
            system.assert(listProposalLineItem != null);
            
            Apttus_Config2__ProductAttributeValue__c testProductAttributeValue = APTS_CPQTestUtility.createAttributeValue(listLineItem[0].Id);
            //populate custom fields value if required
            insert testProductAttributeValue;
            system.assert(testProductAttributeValue.Id != null);
            
            Apttus_Config2__ProductAttributeGroup__c testProductAttributeGroup = APTS_CPQTestUtility.createProductAttributeGroup('test Apttus Product Attribute Group', 'Apttus_Config2__ProductConfiguration__c', 'test description', true);
            //populate custom fields value if required
            insert testProductAttributeGroup;
            system.assert(testProductAttributeGroup.Name == 'test Apttus Product Attribute Group');
            
            Apttus_Config2__ProductAttribute__c testProductAttribute = APTS_CPQTestUtility.createProductAttribute(testProductAttributeGroup.Id, 'APTS_Zone__c', false, false, 'APTS_Zone__c', 1);
            //populate custom fields value if required
            insert testProductAttribute;
            system.assert(testProductAttribute.Id != null);
                    
            Apttus_QPConfig__ProposalProductAttributeValue__c testProposalProductAttributeValue = APTS_CPQTestUtility.createProposalProductAttributeValue(listProposalLineItem[0].Id);
            //populate custom fields value if required
            insert testProposalProductAttributeValue;
            system.assert(testProposalProductAttributeValue.Id != null);
            
            Apttus_Config2__PriceDimension__c testPriceDimension = APTS_CPQTestUtility.createPriceDimension('test Price Dimension', 'Product Attribute', 'Apttus_Config2__ProductAttributeValue__c', 'APTS_Zone__c', testProductAttribute.Id);
            //populate custom fields value if required
            insert testPriceDimension;
            system.assert(testPriceDimension.Name == 'test Price Dimension');
            
            Apttus_Config2__PriceMatrix__c testPriceMatrix = APTS_CPQTestUtility.createPriceMatrix(testPriceListItem.Id, testPriceDimension.Id, null, null, 'Discrete', null, null);
            //populate custom fields value if required
            insert testPriceMatrix;
            system.assert(testPriceMatrix.Id != null);
            
            Apttus_Config2__PriceMatrixEntry__c testPriceMatrixEntry = APTS_CPQTestUtility.createPriceMatrixEntry(testPriceMatrix.Id, 1, 'test1', null, null, 100, 100, 100, 'Discount %');
            //populate custom fields value if required
            insert testPriceMatrixEntry;
            system.assert(testPriceMatrixEntry.Id != null);
            
            Apttus_Config2__UsagePriceTier__c testUsagePriceTier = APTS_CPQTestUtility.createUsagePriceTier(listLineItem[0].Id, testPriceMatrix.Id, 1, 'test', null, null, 0, 100, 1000, 1000, 100, 'Discount %');
            //populate custom fields value if required
            insert testUsagePriceTier;
            system.assert(testUsagePriceTier.Id != null);
            
            Apttus_QPConfig__ProposalUsagePriceTier__c testProposalUsagePriceTier = APTS_CPQTestUtility.createProposalUsagePriceTier(listProposalLineItem[0].Id, testPriceMatrix.Id, 1, 'test', null, null, 0, 100, 1000, 1000, 100, 'Discount %');
            //populate custom fields value if required
            insert testProposalUsagePriceTier;
            system.assert(testProposalUsagePriceTier.Id != null);
            
            test.StopTest();
        } catch (Exception e){
			System.debug(e.getStackTraceString());
		}
    }
}