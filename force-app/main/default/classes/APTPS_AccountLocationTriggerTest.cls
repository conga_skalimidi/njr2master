/***********************************************************************************************************************
@Name: APTPS_AccountLocationTriggerTest
@Author: Siva Kumar
@CreateDate: 22 October 2020
@Description: APEX class to to test classes APTPS_AccountLocationTrigger,APTPS_AccountLocationTriggerHandler,APTPS_AccountLocation_Helper,APTPS_AccountLocation_Wrapper
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/
@isTest
public class APTPS_AccountLocationTriggerTest {
    @isTest
     public static void CreateAccountLocation() {
        Test.StartTest(); 
        List<Account> accList = new List<Account>();
        Id recordTypeIdNJUS = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('NJUS New Card Sale').getRecordTypeId();
        Id recordTypeIdNJE = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('NJE New Card Sale').getRecordTypeId();
        Account accToInsert = new Account();
        accToInsert.Name = 'APTS Test Account Name';
        //accToInsert.APTS_Has_Active_Agreement__c = true;
        accToInsert.APTS_Has_Active_NJUS_Agreement__c = true;
        accToInsert.APTS_Has_Active_NJE_Agreement__c = true;
        insert accToInsert; 
        list<Apttus_Config2__AccountLocation__c> locationList = new list<Apttus_Config2__AccountLocation__c>();
        Apttus_Config2__AccountLocation__c loc = new Apttus_Config2__AccountLocation__c();
        loc.Apttus_Config2__AccountId__c = accToInsert.id;
        loc.Name = 'Location1';
        loc.Apttus_Config2__Street__c = 'test';
        loc.Apttus_Config2__Type__c = 'Company';
        loc.Apttus_Config2__Country__c = 'Test';
        locationList.add(loc);
        Apttus_Config2__AccountLocation__c loc2 = new Apttus_Config2__AccountLocation__c();
        loc2.Apttus_Config2__AccountId__c = accToInsert.id;
        loc2.Name = 'Location2';
        loc2.Apttus_Config2__Street__c = 'test';
        loc2.Apttus_Config2__Type__c = 'Company';
        loc2.Apttus_Config2__Country__c = 'Test';
        locationList.add(loc2);
        insert locationList;
        loc2.Name = 'Location3';
        update loc2;
        Test.stopTest();
        
    }   
        
}