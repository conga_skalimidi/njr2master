/*
 * Created by Siva Kumar on 12/09/2019.
 */
global class TerminateAgreementAction {

        public class TerminateAgreementRequest{
            @InvocableVariable(required=true)
            public Id agreementId;
           
        }
        
        @InvocableMethod(label='Terminates an Agreement' description='Terminates an Agreement')
        public static void terminateAgreements (TerminateAgreementRequest[] terminateAgreementRequests) {
            try{
                Apttus__APTS_Agreement__c[] agreements = new Apttus__APTS_Agreement__c[]{};
                List<Apttus_Config2__AssetLineItem__c> updateALIList = new  List<Apttus_Config2__AssetLineItem__c>();
                Set<Id> agreementIds = new Set<Id>();
                Id terminateAgreementId = terminateAgreementRequests[0].agreementId;            
                system.debug('terminateAgreementId-->'+terminateAgreementId);
                // Call the Activate AgreementFlow
                Map<String, Object> params = new Map<String, Object>();
                params.put('agreementId', terminateAgreementId);
                Flow.Interview.Activate_Agreement agreementFlow = new Flow.Interview.Activate_Agreement(params);
                agreementFlow.start();
                Apttus__APTS_Agreement__c activeAgreementObj = [SELECT Id,Apttus__Status__c,Card_Number__c FROM Apttus__APTS_Agreement__c WHERE Id =:terminateAgreementId LIMIT 1];
                //Terminate Current Agreement and All Related 'Related Agreements (Relationship From)' Agreements
                if(activeAgreementObj.Apttus__Status__c == 'Activated') { 
                
                   /* for(Apttus__APTS_Agreement__c terminateAgreementObj : [SELECT Id,Card_Number__c FROM Apttus__APTS_Agreement__c WHERE Id=:activeAgreementObj.Id OR (Card_Number__c!=null AND Card_Number__c =:activeAgreementObj.Card_Number__c)]) {
                        APTS_ExecuteTerminationAPIBatch batch = new APTS_ExecuteTerminationAPIBatch(terminateAgreementObj.id,false);
                        Database.executeBatch( batch,1);
                    }*/
                    APTS_ExecuteTerminationAPIBatch batch = new APTS_ExecuteTerminationAPIBatch(activeAgreementObj.id,false);
                        Database.executeBatch( batch,1);
                     //System.enqueueJob(new APTS_ExecuteTerminationQueueble(activeAgreementObj.Id));
                    
                    
                    
                    
                }
                

            } catch(Exception e){
                System.debug('[TerminateAgreementAction].terminateAgreements Message: ' + e.getMessage());
                System.debug('[TerminateAgreementAction].terminateAgreements StackTrace: ' + e.getStackTraceString());
                //throw e; 
            } 
        }

}