/************************************************************************************************************************
@Name: APTPS_CLMLifeCycle_Demo
@Author: Conga PS Dev Team
@CreateDate: 4 June 2021
@Description: 
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/

public class APTPS_CLMLifeCycle_Demo {    
    
    /**
    @description: Update Status 
    @param: args 
    @return: void
    */
    
    public static void updateCLMStatus(Map<String, Object> args,String currencyCode) {
        system.debug('update Agreement Status Fields');
        List<Apttus__APTS_Agreement__c> newAg = (List<Apttus__APTS_Agreement__c>)args.get('newAgrList');
        Map<Id,SObject> oldAgMap = (Map<Id,SObject>)args.get('oldAgrList');
        for(Apttus__APTS_Agreement__c ag : newAg) {
        
            Apttus__APTS_Agreement__c oldAgObj = (Apttus__APTS_Agreement__c) oldAgMap.get(ag.Id);
            if (oldAgObj != null) {
                String oldStatus = oldAgObj.Apttus__Status__c;
                String oldStatusCat = oldAgObj.Apttus__Status_Category__c;
                String newStatus = ag.Apttus__Status__c;
                String newStatusCat = ag.Apttus__Status_Category__c;
                system.debug('SNL Agreement Status Lifecycle. oldStatus --> '+oldStatus+' oldStatusCat --> '+oldStatusCat+
                            ' newStatus --> '+newStatus+' newStatusCat --> '+newStatusCat);
               //Activate
                    if((APTS_ConstantUtil.FULLY_SIGNED.equalsIgnoreCase(oldStatus) || APTS_ConstantUtil.OTHER_PARTY_SIGN.equalsIgnoreCase(oldStatus)) && APTS_ConstantUtil.IN_SIGN.equalsIgnoreCase(oldStatusCat) 
                       && APTS_ConstantUtil.ACTIVATED.equalsIgnoreCase(newStatus) && APTS_ConstantUtil.IN_EFFECT.equalsIgnoreCase(newStatusCat)) {
                           APTPS_CLMLifeCycle_Corporate.setAgreementStatus(ag, APTS_ConstantUtil.AGREEMENT_ACTIVE, APTS_ConstantUtil.FUNDED_CONTRACT, APTS_ConstantUtil.DOC_SIGNED);
                       }
                
            }
        
        }
    }

    private static void popualateDemoFields(Map<String, Object> args) {
    
    }
    /**
    @description: Reusable method to set Agreement Status
    @param: ag - Agreement record
    @param: agStatus - Agreement Status and Chevron Status
    @param: fundingStatus - Funding Status
    @param: docStatus - Document Status
    @return: void
    */
    public static Apttus__APTS_Agreement__c setAgreementStatus(Apttus__APTS_Agreement__c ag, String agStatus, String fundingStatus, String docStatus) {
        ag.Agreement_Status__c = agStatus;
        ag.Chevron_Status__c = agStatus;
        ag.APTS_Path_Chevron_Status__c = agStatus;
        ag.Funding_Status__c = fundingStatus;
        ag.Document_Status__c = docStatus;
        return ag;
    }
    
     /** 
    @description: NJE Demo implementation
    @param:
    @return: 
    */
    public class NJE_CLMFlow implements APTPS_CLMLifeCycleInterface {
        public void callCLMAction(Map<String, Object> args) {
            
             APTPS_CLMLifeCycle_Demo.updateCLMStatus(args,APTS_ConstantUtil.CUR_EUR);
            
                
            
        }
    }
    
    /** 
    @description: NJA Demo implementation
    @param:
    @return: 
    */
    public class NJA_CLMFlow implements APTPS_CLMLifeCycleInterface {
        public void callCLMAction(Map<String, Object> args) {
            
             APTPS_CLMLifeCycle_Demo.updateCLMStatus(args,APTS_ConstantUtil.CUR_USD);
            
                
            
        }
    }
    
}