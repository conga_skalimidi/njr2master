public class APTPS_ImportFullySignedDocController {
  private Apttus__APTS_Agreement__c agreement;

  public APTPS_ImportFullySignedDocController(ApexPages.StandardController stdController) { 
    
    this.agreement = (Apttus__APTS_Agreement__c)stdController.getRecord();
    
  }

  public PageReference finalize(){
    
    agreement.Apttus__Status_Category__c = 'In Signatures';
    agreement.Apttus__Status__c = 'Fully Signed';
    
    update agreement;
    
    PageReference result = new ApexPages.StandardController(agreement).view();
    
    return result;
    
  }
}