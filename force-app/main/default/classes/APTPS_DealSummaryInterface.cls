/************************************************************************************************************************
@Name: APTPS_DealSummaryInterface
@Author: Conga PS Dev Team
@CreateDate: 17 May 2021
@Description: 
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/

public interface APTPS_DealSummaryInterface {
    Apttus_Proposal__Proposal__c updateSummary(Map<String, Object> args, List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines);
}