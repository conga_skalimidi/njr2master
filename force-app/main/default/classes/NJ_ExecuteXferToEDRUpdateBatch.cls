/*************************************************************
@Name: NJ_ExecuteXferToEDRUpdateBatch
@Author: Ramesh Kumar
@CreateDate: 20 May 2020
@Description : This batch class is used to update Xfer_to_EDR flag once agreement and all its related 
records are created
******************************************************************/

global class NJ_ExecuteXferToEDRUpdateBatch implements Database.Batchable<sObject>,Database.Stateful,Schedulable {
    global Id clsAggreementId;
    global boolean isJobCompleted = false;
    
    global List<Apttus__APTS_Agreement__c> agreementObjList=new List<Apttus__APTS_Agreement__c>();

    global NJ_ExecuteXferToEDRUpdateBatch(Id AgreementId){
        this.clsAggreementId = AgreementId;
    }
    global List<Apttus__APTS_Agreement__c> start(Database.BatchableContext BC){
        //process agreements with Xfer_to_EDR__c as false
        agreementObjList = [SELECT Id, Xfer_to_EDR__c FROM Apttus__APTS_Agreement__c WHERE Id =:clsAggreementId AND APTPS_Program_Type__c = 'Card'] ;
        return this.agreementObjList;
        
    }
    global void execute(Database.BatchableContext BC,List<Apttus__APTS_Agreement__c> agreementObjList){
       
        try {
            
            if(agreementObjList.size() > 0) {
                Integer jobs = [SELECT count() FROM AsyncApexJob where (ApexClass.name='OrderWorkflowQJob' OR ApexClass.name='CreateOrderQJob' OR ApexClass.name='APTS_UpdateAssetLIQueueble') AND  (Status = 'Queued' or Status = 'Processing' or Status = 'Preparing')];
                //Checking for Apttus job completion
                if(jobs == 0 ) {
                    isJobCompleted = true;
                    List<Apttus__APTS_Agreement__c> agreementList = new List<Apttus__APTS_Agreement__c>();

                    for(Apttus__APTS_Agreement__c agr : agreementObjList){
                        agr.Xfer_to_EDR__c = true;
                        agreementList.add(agr);
                    }
                    update agreementList;
                }
                else{
                        //re-execute the batch job
                        isJobCompleted = false;
                    }
                }
                else {
                    //exit the batch job execution when no agreements to be be processed
                    isJobCompleted = true;
 
                }


            }catch (Exception e)  {
                isJobCompleted = true;
                System.debug('@@ exception ' + e.getMessage() + ' line number ' + e.getLineNumber());

        }
        
    }

     global void finish(Database.BatchableContext BC) {
        
         //The batch job will execute recursively at 30 sec interval until the Apttus Jobs are completed
        if (!Test.isRunningTest())
        {
            if(!isJobCompleted){

                Datetime systemTime = DateTime.now();
                systemTime = systemTime.addSeconds(30);
                
                String log_exp = '' + systemTime.second() + ' ' + 
                                    systemTime.minute() + ' ' + 
                                    systemTime.hour() + ' ' + 
                                    systemTime.day() + ' ' + 
                                    systemTime.month() + ' ? ' + 
                                    systemTime.year();     
                System.schedule('Recursive-NJ_ExecuteXferToEDRUpdateBatch-'+log_exp + '--AgreementId-'+clsAggreementId, log_exp, (new NJ_ExecuteXferToEDRUpdateBatch(clsAggreementId)));
            } 

        }
    }
    public void execute(SchedulableContext sc)
    {
        //This method is required to reschedule the batch job
        Database.executeBatch(new NJ_ExecuteXferToEDRUpdateBatch(clsAggreementId), 1);
    }

}