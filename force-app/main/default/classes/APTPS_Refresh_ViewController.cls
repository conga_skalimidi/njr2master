public with sharing class APTPS_Refresh_ViewController {

    @AuraEnabled
    public static String updateRecord(Id recordId, Id recordType){
        system.debug('recordId-->'+recordId);
        Apttus__APTS_Agreement__c agreement = new Apttus__APTS_Agreement__c(Id=recordId, RecordTypeId=recordType);
        upsert agreement;
        return 'SUCCESS';
        
    }
}