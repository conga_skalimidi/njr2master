/*************************************************************
@Name: APTS_UpdateAssetLIQueueble
@Author: Siva Kumar
@CreateDate: 16 January 2020
@Description : This Queueable class is used to update AssetLI Status in Modification Scenarios
*****************************************************************
@ModifiedBy: Avinash Bamane
@ModifiedDate: 7 July 2020
@ChangeDescription: Defect GCM-8282, in case of Termination, Add Enhancements and Term Extension - read card number
from Original Agreement field available on Asset Line Item.
*/
public class APTS_UpdateAssetLIQueueble implements Queueable {
    public List<Id>  assetIds; 
    public APTS_UpdateAssetLIQueueble(List<Id> assetIdList){
        this.assetIds =  assetIdList;  
    }
    public void execute(QueueableContext context) {
        Integer jobs = [SELECT count() FROM AsyncApexJob where (ApexClass.name='OrderWorkflowQJob' OR ApexClass.name='CreateOrderQJob') AND  (Status = 'Queued' or Status = 'Processing' or Status = 'Preparing')];
        if(jobs == 0 ) {
            List<Apttus_Config2__AssetLineItem__c> UpdateALIList = new List<Apttus_Config2__AssetLineItem__c>();
            set<Id> terminateAgreementIds = new set<id>();
            set<Id> activateAgreementIds = new set<id>();
            set<Id> amendedAgreementIds = new set<id>();
            set<Id> inactiveAgreementIds = new set<id>();
            set<Id> AgreementIds = new set<id>();
            Map<id,set<string>> relatedAgreementIdMap = new Map<id,set<string>>();
            set<Id> idsToRemove = new set<id>();
            List<Apttus__APTS_Agreement__c> terminateAgreementList = new List<Apttus__APTS_Agreement__c>();
            List<Apttus__APTS_Agreement__c> ActivateAgreementList = new List<Apttus__APTS_Agreement__c>();
            List<Apttus__APTS_Agreement__c> amendedAgreementList = new List<Apttus__APTS_Agreement__c>();
            List<Apttus__APTS_Agreement__c> inactiveAgreementList = new List<Apttus__APTS_Agreement__c>();
            List<Apttus_Config2__AssetAttributeValue__c> UpdateAssetPAVList = new List<Apttus_Config2__AssetAttributeValue__c>();
            List<Apttus__APTS_Related_Agreement__c> relatedAgreementList = new List<Apttus__APTS_Related_Agreement__c>();
            Map<string,list<id>> activateALIMap = new Map<string,list<id>>();
            Map<string,list<id>> terminateALIMap = new Map<string,list<id>>();
            Map<string,list<id>> inactiveALIMap = new Map<string,list<id>>();
            Map<string,list<id>> amendALIMap = new Map<string,list<id>>();
            //Map<id,double> updatePAVMap = new Map<id,double>();
            String currentAgreementId = '';
            String terminateAgreementId = '';
            String modificationType = '';
            Map<string,String> agreementNameMap = new Map<string,String>();
            system.debug('assetIds-->'+assetIds);
             //START: GCM-8282
            overrideCardNumber();
            //END: GCM-8282
            for(Apttus_Config2__AssetLineItem__c ALIObj : [SELECT Id,APTPS_Agreement_Name__c,Card_Number__c,Apttus_CMConfig__AgreementId__c,APTS_Original_Agreement__c,APTS_Original_Agreement__r.Card_Number__c,Apttus_Config2__AttributeValueId__c,Apttus_Config2__AttributeValueId__r.Requested_Hours__c,Apttus_Config2__AttributeValueId__r.Aircraft_Remaining_Hours__c,Apttus_Config2__AttributeValueId__r.APTS_Override_Requested_Hours__c,Apttus_Config2__AttributeValueId__r.APTS_Aircraft_Expired_Hours__c,Apttus_Config2__AttributeValueId__r.APTS_Hours_To_Unexpire__c,Apttus_Config2__AttributeValueId__r.APTS_Overridden_Hours_To_Unexpire__c,Apttus_Config2__AttributeValueId__r.APTS_Override_Hours_To_Unexpire__c,APTS_NJ_Asset_Status__c,Apttus_CMConfig__AgreementId__r.Apttus_QPComply__RelatedProposalId__r.APTS_Modification_Type__c,APTS_Agreement_Status__c,Apttus_Config2__ChargeType__c  FROM Apttus_Config2__AssetLineItem__c WHERE Id IN:assetIds AND Apttus_Config2__ChargeType__c='Purchase Price' AND Apttus_Config2__LineType__c = 'Option']) {
                List<id> aliIdsList = new List<id>();
                set<string> relatedAgreementIds = new set<string>();
                /*Related To Agreement Logic START*/
                if(ALIObj.Apttus_CMConfig__AgreementId__r.Apttus_QPComply__RelatedProposalId__r.APTS_Modification_Type__c != 'Assignment') {
                    if(relatedAgreementIdMap.containsKey(ALIObj.Apttus_CMConfig__AgreementId__c)) {
                        relatedAgreementIds = relatedAgreementIdMap.get(ALIObj.Apttus_CMConfig__AgreementId__c);
                        relatedAgreementIds.add(ALIObj.APTS_Original_Agreement__r.Card_Number__c);
                        relatedAgreementIdMap.put(ALIObj.Apttus_CMConfig__AgreementId__c,relatedAgreementIds);
                    } else {
                        relatedAgreementIds.add(ALIObj.APTS_Original_Agreement__r.Card_Number__c);
                        relatedAgreementIdMap.put(ALIObj.Apttus_CMConfig__AgreementId__c,relatedAgreementIds);
                    }
                }
                
                /*Related To Agreement Logic END*/
                if(modificationType =='') {
                    modificationType = ALIObj.Apttus_CMConfig__AgreementId__r.Apttus_QPComply__RelatedProposalId__r.APTS_Modification_Type__c;
                }
                if(currentAgreementId =='') {
                    currentAgreementId = ALIObj.Apttus_CMConfig__AgreementId__c;
                }
                system.debug('ALIObj.Apttus_Config2__AttributeValueId__r.Requested_Hours__c-->'+ALIObj.Apttus_Config2__AttributeValueId__r.Requested_Hours__c+'--ALIObj.Apttus_Config2__AttributeValueId__r.Aircraft_Remaining_Hours__c-->'+ALIObj.Apttus_Config2__AttributeValueId__r.Aircraft_Remaining_Hours__c+'ALIObj.Apttus_Config2__AttributeValueId__r.Requested_Hours__c-->'+ALIObj.Apttus_Config2__AttributeValueId__r.Requested_Hours__c);
                //Calculate Requested Hours
                double requestedHours = ALIObj.Apttus_Config2__AttributeValueId__r.Requested_Hours__c;
                if(ALIObj.Apttus_Config2__AttributeValueId__r.APTS_Override_Requested_Hours__c != null && ALIObj.Apttus_Config2__AttributeValueId__r.APTS_Override_Requested_Hours__c != 0) {
                    requestedHours = ALIObj.Apttus_Config2__AttributeValueId__r.APTS_Override_Requested_Hours__c;
                }
                system.debug('requestedHours-->'+requestedHours);
                //For Modifications Add Enhancements, Term Extension and Assignment skip Requested hours check
                if(ALIObj.Apttus_CMConfig__AgreementId__r.Apttus_QPComply__RelatedProposalId__r.APTS_Modification_Type__c == 'Add Enhancements' || ALIObj.Apttus_CMConfig__AgreementId__r.Apttus_QPComply__RelatedProposalId__r.APTS_Modification_Type__c == 'Term Extension' || ALIObj.Apttus_CMConfig__AgreementId__r.Apttus_QPComply__RelatedProposalId__r.APTS_Modification_Type__c == 'Assignment') {
                    // Updated for GCM-7029 for Assignemt Parent Agreement ststus should be Inactive and for Add Enhancements, Term Extension status is Amended
                    if(ALIObj.Apttus_CMConfig__AgreementId__r.Apttus_QPComply__RelatedProposalId__r.APTS_Modification_Type__c == 'Assignment') {
                        //inactiveAgreementIds.add(ALIObj.APTS_Original_Agreement__c);
                        if(currentAgreementId =='') {
                            currentAgreementId = ALIObj.Apttus_CMConfig__AgreementId__c;
                        }
                        if(inactiveALIMap.containsKey(ALIObj.APTS_Original_Agreement__r.Card_Number__c)) {
                            aliIdsList = inactiveALIMap.get(ALIObj.APTS_Original_Agreement__r.Card_Number__c);
                            aliIdsList.add(ALIObj.id);
                            inactiveALIMap.put(ALIObj.APTS_Original_Agreement__r.Card_Number__c,aliIdsList);
                        } else {
                            aliIdsList.add(ALIObj.id);
                            inactiveALIMap.put(ALIObj.APTS_Original_Agreement__r.Card_Number__c,aliIdsList);
                        }
                    } else  {
                        //amendedAgreementIds.add(ALIObj.APTS_Original_Agreement__c);
                        if(currentAgreementId =='') {
                            currentAgreementId = ALIObj.Apttus_CMConfig__AgreementId__c;
                        }
                        if(amendALIMap.containsKey(ALIObj.APTS_Original_Agreement__r.Card_Number__c)) {
                            aliIdsList = amendALIMap.get(ALIObj.APTS_Original_Agreement__r.Card_Number__c);
                            aliIdsList.add(ALIObj.id);
                            amendALIMap.put(ALIObj.APTS_Original_Agreement__r.Card_Number__c,aliIdsList);
                        } else {
                            aliIdsList.add(ALIObj.id);
                            amendALIMap.put(ALIObj.APTS_Original_Agreement__r.Card_Number__c,aliIdsList);
                        }
                    }
                    
                } else if(ALIObj.Apttus_CMConfig__AgreementId__r.Apttus_QPComply__RelatedProposalId__r.APTS_Modification_Type__c != 'Termination') {                 
                    
                    if(modificationType == 'Expired Hours Card') {
                        //Calculate UnExpired Hours
                        /*double unExpiredHours = ALIObj.Apttus_Config2__AttributeValueId__r.APTS_Hours_To_Unexpire__c;
                        if(ALIObj.Apttus_Config2__AttributeValueId__r.APTS_Override_Hours_To_Unexpire__c && ALIObj.Apttus_Config2__AttributeValueId__r.APTS_Overridden_Hours_To_Unexpire__c != null && ALIObj.Apttus_Config2__AttributeValueId__r.APTS_Overridden_Hours_To_Unexpire__c != 0) {
                            unExpiredHours = ALIObj.Apttus_Config2__AttributeValueId__r.APTS_Overridden_Hours_To_Unexpire__c;
                        }*/
                        //if(unExpiredHours >= ALIObj.Apttus_Config2__AttributeValueId__r.APTS_Aircraft_Expired_Hours__c) {
                        if(ALIObj.Apttus_Config2__AttributeValueId__r.APTS_Aircraft_Expired_Hours__c <= 0) {
                            if(terminateALIMap.containsKey(ALIObj.APTS_Original_Agreement__r.Card_Number__c)) {
                                aliIdsList = terminateALIMap.get(ALIObj.APTS_Original_Agreement__r.Card_Number__c);
                                aliIdsList.add(ALIObj.id);
                                terminateALIMap.put(ALIObj.APTS_Original_Agreement__r.Card_Number__c,aliIdsList);
                            } else {
                                aliIdsList.add(ALIObj.id);
                                terminateALIMap.put(ALIObj.APTS_Original_Agreement__r.Card_Number__c,aliIdsList);
                            }
                          
                        } 
                        //else if(unExpiredHours < ALIObj.Apttus_Config2__AttributeValueId__r.APTS_Aircraft_Expired_Hours__c) {
                        else if(ALIObj.Apttus_Config2__AttributeValueId__r.APTS_Aircraft_Expired_Hours__c > 0) {
                            //system.debug('elseIf unExpiredHours-->'+unExpiredHours);
                            
                            if(activateALIMap.containsKey(ALIObj.APTS_Original_Agreement__r.Card_Number__c)) {
                                aliIdsList = activateALIMap.get(ALIObj.APTS_Original_Agreement__r.Card_Number__c);
                                aliIdsList.add(ALIObj.id);
                                activateALIMap.put(ALIObj.APTS_Original_Agreement__r.Card_Number__c,aliIdsList);
                            } else {
                                aliIdsList.add(ALIObj.id);
                                activateALIMap.put(ALIObj.APTS_Original_Agreement__r.Card_Number__c,aliIdsList);
                            }
                            
                        }
                        if(ALIObj.Apttus_Config2__AttributeValueId__r.APTS_Override_Hours_To_Unexpire__c){
                            UpdateAssetPAVList.add(new Apttus_Config2__AssetAttributeValue__c(id=ALIObj.Apttus_Config2__AttributeValueId__c,APTS_Override_Hours_To_Unexpire__c =false,APTS_Overridden_Hours_To_Unexpire__c = null));
                        }
                        
                       /* Double dExpiredHrs = 0.0;
                        Double aExpiredHrs = ALIObj.Apttus_Config2__AttributeValueId__r.APTS_Aircraft_Expired_Hours__c;
                        dExpiredHrs  = aExpiredHrs - unExpiredHours;
                        updatePAVMap.put(ALIObj.Apttus_Config2__AttributeValueId__c,dExpiredHrs);*/

                        
                    } else {
                    
                        if(requestedHours >= ALIObj.Apttus_Config2__AttributeValueId__r.Aircraft_Remaining_Hours__c) {
                            if(terminateALIMap.containsKey(ALIObj.APTS_Original_Agreement__r.Card_Number__c)) {
                                aliIdsList = terminateALIMap.get(ALIObj.APTS_Original_Agreement__r.Card_Number__c);
                                aliIdsList.add(ALIObj.id);
                                terminateALIMap.put(ALIObj.APTS_Original_Agreement__r.Card_Number__c,aliIdsList);
                            } else {
                                aliIdsList.add(ALIObj.id);
                                terminateALIMap.put(ALIObj.APTS_Original_Agreement__r.Card_Number__c,aliIdsList);
                            }
                          
                        } else if(requestedHours < ALIObj.Apttus_Config2__AttributeValueId__r.Aircraft_Remaining_Hours__c) {
                            system.debug('elseIf-->'+requestedHours);
                            
                            if(activateALIMap.containsKey(ALIObj.APTS_Original_Agreement__r.Card_Number__c)) {
                                aliIdsList = activateALIMap.get(ALIObj.APTS_Original_Agreement__r.Card_Number__c);
                                aliIdsList.add(ALIObj.id);
                                activateALIMap.put(ALIObj.APTS_Original_Agreement__r.Card_Number__c,aliIdsList);
                            } else {
                                aliIdsList.add(ALIObj.id);
                                activateALIMap.put(ALIObj.APTS_Original_Agreement__r.Card_Number__c,aliIdsList);
                            }
                            
                        }
                    }
                }
                else if(ALIObj.Apttus_CMConfig__AgreementId__r.Apttus_QPComply__RelatedProposalId__r.APTS_Modification_Type__c == 'Termination') {
                    if(terminateAgreementId =='') {
                            terminateAgreementId = ALIObj.Apttus_CMConfig__AgreementId__c;
                    }
                    
                }
            }
            system.debug('activateALIMap-->'+activateALIMap);
            system.debug('terminateALIMap-->'+terminateALIMap);
            system.debug('inactiveALIMap-->'+inactiveALIMap);
            system.debug('amendALIMap-->'+amendALIMap);
            system.debug('currentAgreementId-->'+currentAgreementId);
            system.debug('relatedAgreementIdMap-->'+relatedAgreementIdMap);
            
            
            if(!activateALIMap.isEmpty()) {
                for(Apttus__APTS_Agreement__c activateAgreementObj : [SELECT Id,Name FROM Apttus__APTS_Agreement__c WHERE Card_Number__c IN:activateALIMap.keySet()]) {
                    activateAgreementIds.add(activateAgreementObj.Id);
                    agreementNameMap.put(activateAgreementObj.Id,activateAgreementObj.Name);
                }
            }
            if(!terminateALIMap.isEmpty()) {
                for(Apttus__APTS_Agreement__c terminateAgreementObj : [SELECT Id,Name FROM Apttus__APTS_Agreement__c WHERE Card_Number__c IN:terminateALIMap.keySet()]) {
                    terminateAgreementIds.add(terminateAgreementObj.Id);
                    
                }
            }
            if(!inactiveALIMap.isEmpty()) {
                for(Apttus__APTS_Agreement__c inactiveAgreementObj : [SELECT Id,Name FROM Apttus__APTS_Agreement__c WHERE id!=:currentAgreementId AND Agreement_Status__c ='In Modification'  AND Card_Number__c IN:inactiveALIMap.keySet()]) {
                    inactiveAgreementIds.add(inactiveAgreementObj.Id);
                    
                    
                }
            }
            if(!amendALIMap.isEmpty()) {
                for(Apttus__APTS_Agreement__c amendAgreementObj : [SELECT Id,Name FROM Apttus__APTS_Agreement__c WHERE id!=:currentAgreementId AND Card_Number__c IN:amendALIMap.keySet()]) {
                    amendedAgreementIds.add(amendAgreementObj.Id);
                    
                }
            }
            if(!relatedAgreementIdMap.isEmpty()) {
                
                Map<id,set<Id>> relatedFromAgreementMap = new Map<id,set<Id>>();
                set<id> relatedToAgreementIds = new set<id>();
                for(Apttus__APTS_Related_Agreement__c relatedFromAgreement : [SELECT id,Apttus__APTS_Contract_From__c,Apttus__APTS_Contract_To__c FROM Apttus__APTS_Related_Agreement__c WHERE Apttus__APTS_Contract_From__c IN :relatedAgreementIdMap.keyset() ]) {
                    if(relatedFromAgreementMap.containsKey(relatedFromAgreement.Apttus__APTS_Contract_From__c)) {
                        relatedToAgreementIds = relatedFromAgreementMap.get(relatedFromAgreement.Apttus__APTS_Contract_From__c);
                        relatedToAgreementIds.add(relatedFromAgreement.Apttus__APTS_Contract_To__c);
                        relatedFromAgreementMap.put(relatedFromAgreement.Apttus__APTS_Contract_From__c,relatedToAgreementIds);
                    } else {
                        relatedToAgreementIds.add(relatedFromAgreement.Apttus__APTS_Contract_To__c);
                        relatedFromAgreementMap.put(relatedFromAgreement.Apttus__APTS_Contract_From__c,relatedToAgreementIds);
                    }
                }
                if((modificationType == 'Add Enhancements' || modificationType == 'Term Extension') && !amendALIMap.isEmpty()){
                    set<string> relatedToIds = new set<string>();
                    for(Apttus__APTS_Agreement__c amendAgreementObj : [SELECT Id,Name,Card_Number__c FROM Apttus__APTS_Agreement__c WHERE id!=:currentAgreementId AND Agreement_Status__c != 'Amended' AND Card_Number__c IN:amendALIMap.keySet()]) {
                        relatedToIds.add(amendAgreementObj.Card_Number__c);
                    
                    }
                    if(!relatedToIds.isEmpty()){
                        relatedAgreementIdMap.put(currentAgreementId,relatedToIds);
                    }
                    
                }
                if(!relatedAgreementIdMap.isEmpty()) {
                    map<string,id> agreementCardMap = new map<string,id>();
                    set<string> relatedAgreementValues = new set<string>();
                    for(Id currentId  : relatedAgreementIdMap.keyset()) {
                        relatedAgreementValues.addAll(relatedAgreementIdMap.get(currentId));
                    }
                    for(Apttus__APTS_Agreement__c agmtObj : [SELECT Id,Name,Card_Number__c FROM Apttus__APTS_Agreement__c WHERE id!=:currentAgreementId AND Agreement_Status__c != 'Amended' AND Card_Number__c IN :relatedAgreementValues]) {
                        agreementCardMap.put(agmtObj.Card_Number__c,agmtObj.id);
                    }
                    for(Id fromId  : relatedAgreementIdMap.keyset()) {
                        set<id> toIds = new set<id>();
                         if(!relatedFromAgreementMap.isEmpty()) {
                              if(relatedFromAgreementMap.containsKey(fromId)) {
                                toIds = relatedFromAgreementMap.get(fromId);  
                              }                    
                                
                            }
                       for(String toId : relatedAgreementIdMap.get(fromId)) {
                            if(!toIds.contains(agreementCardMap.get(toId))) {
                                relatedAgreementList.add(new Apttus__APTS_Related_Agreement__c(Name='Modification',APTS_Business_Object_Type__c='Agreement Life Cycle Flow',Apttus__APTS_Contract_From__c=fromId,Apttus__APTS_Contract_To__c=agreementCardMap.get(toId)));
                            }
                            
                        }
                    }
                }
            }
            system.debug('activateAgreementIds-->'+activateAgreementIds);
            system.debug('amendedAgreementIds-->'+amendedAgreementIds);
            system.debug('inactiveAgreementIds-->'+inactiveAgreementIds);
            system.debug('terminateAgreementIds-->'+terminateAgreementIds);
            system.debug('relatedAgreementList-->'+relatedAgreementList);
            system.debug('relatedAgreementIdMap-->'+relatedAgreementIdMap);
            system.debug('modificationType-->'+modificationType);
           
           for(id aggId : activateAgreementIds) {
                if(terminateAgreementIds.contains(aggId)) {
                    idsToRemove.add(aggId);
                }
            }
            if(!idsToRemove.isEmpty()) {
                terminateAgreementIds.removeAll(idsToRemove);
            }
            try{
               
                if(!activateAgreementIds.isEmpty()) {
                    for(id activateAgreementId : activateAgreementIds){
                        String agreementStatus = 'Active';
                        if(modificationType == 'Expired Hours Card') {
                            agreementStatus = 'Inactive';
                        }
                         ActivateAgreementList.add(new Apttus__APTS_Agreement__c(Id=activateAgreementId,Agreement_Status__c = agreementStatus,Chevron_Status__c=agreementStatus));
                    }
                    for(Apttus_Config2__AssetLineItem__c updateALI :[SELECT Id,APTS_Original_Agreement__c FROM  Apttus_Config2__AssetLineItem__c WHERE APTS_Original_Agreement__c IN :activateAgreementIds]) {                     
                        UpdateALIList.add(new Apttus_Config2__AssetLineItem__c(Id=updateALI.Id, Apttus_Config2__AssetStatus__c='Activated',APTS_Agreement_Status__c='Active',APTPS_Agreement_Name__c=agreementNameMap.get(updateALI.APTS_Original_Agreement__c)));
                    }     
                    system.debug('ActivateAgreementList-->'+ActivateAgreementList);
                    update ActivateAgreementList;
                }
                if(!amendedAgreementIds.isEmpty()) {
                   //for(Apttus__APTS_Related_Agreement__c relAgreementObj : [SELECT Apttus__APTS_Contract_To__c FROM  Apttus__APTS_Related_Agreement__c WHERE Apttus__APTS_Contract_From__c IN:amendedAgreementIds]){
                    for(Id amendedAgreementId : amendedAgreementIds) {
                         amendedAgreementList.add(new Apttus__APTS_Agreement__c(Id=amendedAgreementId,Agreement_Status__c = 'Amended'));
                    }  
                    for(Apttus_Config2__AssetLineItem__c amendALI :[SELECT Id,Apttus_CMConfig__AgreementId__r.Name FROM      Apttus_Config2__AssetLineItem__c WHERE APTS_Original_Agreement__c IN :amendedAgreementIds]) {                     
                        UpdateALIList.add(new Apttus_Config2__AssetLineItem__c(Id=amendALI.Id, Apttus_Config2__AssetStatus__c='Activated',APTS_Agreement_Status__c='Active',APTPS_Agreement_Name__c=amendALI.Apttus_CMConfig__AgreementId__r.Name));
                    }  
                                       
                    update amendedAgreementList;
                }
                
                if(!inactiveAgreementIds.isEmpty()) {
                   // for(Apttus__APTS_Related_Agreement__c relAgreementObj : [SELECT Apttus__APTS_Contract_To__c FROM  Apttus__APTS_Related_Agreement__c WHERE Apttus__APTS_Contract_From__c IN:inactiveAgreementIds]){
                    for(Id inactiveAgreementId : inactiveAgreementIds) {
                         inactiveAgreementList.add(new Apttus__APTS_Agreement__c(Id=inactiveAgreementId,Agreement_Status__c = 'Inactive'));
                    }               
                    for(Apttus_Config2__AssetLineItem__c inactiveALI :[SELECT Id,Apttus_CMConfig__AgreementId__r.Name FROM      Apttus_Config2__AssetLineItem__c WHERE Apttus_CMConfig__AgreementId__c =:currentAgreementId]) {                     
                        UpdateALIList.add(new Apttus_Config2__AssetLineItem__c(Id=inactiveALI.Id, APTS_NJ_Asset_Status__c='Activated',APTS_Agreement_Status__c='Active',APTPS_Agreement_Name__c=inactiveALI.Apttus_CMConfig__AgreementId__r.Name,APTS_Original_Agreement__c=inactiveALI.Apttus_CMConfig__AgreementId__c));
                    }                     
                    update inactiveAgreementList;
                }
                
                
                if(!relatedAgreementList.isEmpty()) {
                    list<Apttus__APTS_Related_Agreement__c> realtedList = [SELECT id, Apttus__APTS_Contract_From__c FROM Apttus__APTS_Related_Agreement__c WHERE Apttus__APTS_Contract_From__c =:currentAgreementId];
                    if(!realtedList.isEmpty()) {
                        delete realtedList;
                    }
                    insert relatedAgreementList;
                }
                Boolean setAgreementStatusToInactive = true;
                if(!terminateAgreementIds.isEmpty()){
                    for(Id inActiveAgreementId : terminateAgreementIds) {
                         terminateAgreementList.add(new Apttus__APTS_Agreement__c(Id=inActiveAgreementId,Agreement_Status__c = 'Inactive'));
                    } 
                     
                    for(Apttus_Config2__AssetLineItem__c terminateALI :[SELECT Id,Apttus_CMConfig__AgreementId__c,Apttus_Config2__AssetStatus__c FROM      Apttus_Config2__AssetLineItem__c WHERE APTS_Original_Agreement__c IN:terminateAgreementIds]) {
                        terminateALI.Apttus_Config2__AssetStatus__c = 'Cancelled';
                        terminateALI.APTS_Agreement_Status__c = 'Inactive';
                        terminateALI.APTS_NJ_Asset_Status__c='Terminated';
                        UpdateALIList.add(terminateALI);
                    }
                    update terminateAgreementList;
                    /*for(id terminateAgreementId : terminateAgreementIds){
                         APTS_ExecuteTerminationAPIBatch batch = new APTS_ExecuteTerminationAPIBatch(terminateAgreementId,setAgreementStatusToInactive );
                         Database.executeBatch( batch, 1 );
                    }*/
                }
                if(terminateAgreementId!='') {
                    Id agrId = terminateAgreementId;
                    APTS_ExecuteTerminationAPIBatch batch = new APTS_ExecuteTerminationAPIBatch(agrId,false );
                    Database.executeBatch( batch, 1 );
                }
                system.debug('UpdateALIList-->'+UpdateALIList);
                if(!UpdateALIList.isEmpty()) {
                    update UpdateALIList;
                }
              /*  if(!updatePAVMap.isEmpty()) {
                    for(Id pavId: updatePAVMap.keySet()) {
                        UpdateAssetPAVList.add(new Apttus_Config2__AssetAttributeValue__c(id=pavId,APTS_Aircraft_Expired_Hours__c =updatePAVMap.get(pavId),APTPS_Is_Expired_Hours_Reduced__c = true,APTPS_Stop_Integration_Updates__c = true));
                    }
                }*/
                if(!UpdateAssetPAVList.isEmpty()){
                    update UpdateAssetPAVList;
                }
                /**For GCM-8518 defect fix**/
                list<Apttus_Config2__AssetLineItem__c> updateNewALIList = new List<Apttus_Config2__AssetLineItem__c>(); 
                if(modificationType!='') {
                    for(Apttus_Config2__AssetLineItem__c newALIObj : [SELECT ID,APTPS_Asset_Modification_Type__c FROM Apttus_Config2__AssetLineItem__c WHERE Apttus_CMConfig__AgreementId__c =:currentAgreementId AND Apttus_CMConfig__AgreementLineItemId__r.Apttus_CMConfig__LineStatus__c = 'New']) {
                        updateNewALIList.add(new Apttus_Config2__AssetLineItem__c(id=newALIObj.id,APTPS_Asset_Modification_Type__c = modificationType));
                    }
                    system.debug('updateNewALIList-->'+updateNewALIList);
                    if(!updateNewALIList.isEmpty()) {
                        update updateNewALIList;
                    }
                }
                system.debug('Before Add enh, term ext logic');
                // Updating Original Agreement with current agreement for Term Ext, Assignment Addl Enhancements
                list<Apttus_Config2__AssetLineItem__c> updateOrigAgrALIList = new List<Apttus_Config2__AssetLineItem__c>(); 
                if((modificationType == 'Add Enhancements' || modificationType == 'Term Extension' || modificationType == 'Assignment')){
                    for(Apttus_Config2__AssetLineItem__c orgALIObj : [SELECT ID,APTS_Original_Agreement__c FROM Apttus_Config2__AssetLineItem__c WHERE Apttus_CMConfig__AgreementId__c =:currentAgreementId]) {
                        system.debug('Into for loop');
                        updateOrigAgrALIList.add(new Apttus_Config2__AssetLineItem__c(id=orgALIObj.id, APTS_Original_Agreement__c = currentAgreementId));
                    }
                    system.debug('updateOrigAgrALIList-->'+updateOrigAgrALIList);
                    if(!updateOrigAgrALIList.isEmpty()) {
                        system.debug('Into Update');
                        update updateOrigAgrALIList;
                    }
                    
                }

            } catch(Exception e){
                    System.debug('[APTS_UpdateAssetLIStatusAction].UpdateAssetLIStatus Message: ' + e.getMessage());
                    System.debug('[APTS_UpdateAssetLIStatusAction].UpdateAssetLIStatus StackTrace: ' + e.getStackTraceString());
                    //throw e; 
            }
            
           
        } else{
            System.enqueueJob(new APTS_UpdateAssetLIQueueble(assetIds));
        }
    }
    
    //START: GCM-8282
    public void overrideCardNumber() {
        system.debug('overrideCardNumber, assetIds -->'+assetIds);
        try {
            List<Apttus__APTS_Agreement__c> aggToUpdate = new List<Apttus__APTS_Agreement__c>();
            for(Apttus_Config2__AssetLineItem__c aliObj : [SELECT Id, APTS_Original_Agreement__r.Card_Number__c, Apttus_CMConfig__AgreementId__r.Id 
                                                          FROM Apttus_Config2__AssetLineItem__c 
                                                          WHERE Id IN :assetIds AND APTS_Original_Agreement__c != NULL 
                                                          AND Apttus_Config2__LineType__c = 'Product/Service' AND Apttus_Config2__IsPrimaryLine__c = TRUE 
                                                          AND Apttus_CMConfig__AgreementId__c != NULL 
                                                          AND (Apttus_CMConfig__AgreementId__r.APTS_Modification_Type__c = 'Termination' OR Apttus_CMConfig__AgreementId__r.APTS_Modification_Type__c = 'Term Extension' 
                                                              OR Apttus_CMConfig__AgreementId__r.APTS_Modification_Type__c = 'Add Enhancements')]) {
                system.debug('Asset Line Item to copy Card Number --> '+aliObj.Id);
                Apttus__APTS_Agreement__c aggRecord = new Apttus__APTS_Agreement__c(Id = aliObj.Apttus_CMConfig__AgreementId__r.Id, Card_Number__c = aliObj.APTS_Original_Agreement__r.Card_Number__c);
                aggToUpdate.add(aggRecord);
            }
            
            if(!aggToUpdate.isEmpty())
                update aggToUpdate;
        } catch(Exception e) {
            System.debug('[APTS_UpdateAssetLIStatusAction].overrideCardNumber Message: ' + e.getMessage());
            System.debug('[APTS_UpdateAssetLIStatusAction].overrideCardNumber StackTrace: ' + e.getStackTraceString());
        }
    }
    //END: GCM-8282  
      
}