/*********************************************************************************************************************************
@Name: APTS_UpdateRefAgrQueueble
@Author: Siva Kumar
@CreateDate: 10 June 2020
@Description : This Queueable class is used to update Referee and Referral Agreement name
**********************************************************************************************************************************
@ModifiedBy: Avinash Bamane
@ModifiedDate: 29 June 2020
@ChangeDescription: GCM-7631 Reset Additional Enhancement field value and 
set Total Contract Value to zero on Referral/Referee agreements.
**********************************************************************************************************************************
@ModifiedBy: Avinash Bamane
@ModifiedDate: 9 Oct 2020
@ChangeDescription: GCM-9732, stamp Product Type
**********************************************************************************************************************************/

public class APTS_UpdateRefAgrQueueble implements Queueable {
    public Id  agreementId; 
    public String agreementName;
    public String productTypeName = '';
    public APTS_UpdateRefAgrQueueble(Id agrId, String agrName, String productTypeName){
        this.agreementId=  agrId; 
        this.agreementName=  agrName;
        this.productTypeName = productTypeName;
    }
    public void execute(QueueableContext context) {
         Integer jobs = [SELECT count() FROM AsyncApexJob where (ApexClass.name='SyncCartSecondaryQJob' OR ApexClass.name='SyncCartAttributeQJob' OR ApexClass.name='SyncCartUsageTierQJob' OR ApexClass.name='PostSyncCartQJob') AND  (Status = 'Queued' or Status = 'Processing' or Status = 'Preparing')];
        if(jobs == 0 ) {
            if(!String.isBlank(agreementName)) {
                List<Apttus__AgreementLineItem__c> updateAgreementLIs = new List<Apttus__AgreementLineItem__c>();
                set<String> airCraftTypesList = new set<String>();
                for(Apttus__AgreementLineItem__c agrLI : [SELECT id,APTS_New_Sale_or_Modification__c,Option_Product_Family_System__c,Option_Product_Name__c FROM Apttus__AgreementLineItem__c WHERE Apttus__AgreementId__c =:agreementId]) {
                    agrLI.APTS_New_Sale_or_Modification__c = 'New Sale';
                    if(agrLI.Option_Product_Family_System__c == 'Aircraft') {
                        airCraftTypesList.add(agrLI.Option_Product_Name__c);
                    }
                    updateAgreementLIs.add(agrLI);
                }
                if(!updateAgreementLIs.isEmpty()){
                    update updateAgreementLIs;
                }
                String airCraftTypes = '';
                if(!airCraftTypesList.isEmpty()){
                    for(String ac : airCraftTypesList ) {
                        airCraftTypes+= ac+'/';
                    }
                }
                Apttus__APTS_Agreement__c refAgreement = [SELECT Name FROM Apttus__APTS_Agreement__c WHERE Id=:agreementId];
                refAgreement.Name = agreementName;
                refAgreement.Document_Name__c = agreementName;
                refAgreement.APTS_New_Sale_or_Modification__c = 'New Sale';
                refAgreement.APTS_Modification_Type__c = '';
                refAgreement.Apttus__Total_Contract_Value__c = 0;
                //GCM-9732 : Stamp Product Type on Agreement.
                refAgreement.Product_Type__c = productTypeName;
                refAgreement.Exclude_from_Active_Until_Calculation__c = true;
                refAgreement.Delayed_Start_Amount_Months__c = '0';
                refAgreement.Initial_Term_Amount_Months__c = '12';
                refAgreement.Grace_Period_Amount_Months__c = '0';
                if(airCraftTypes!='') {
                    refAgreement.Aircraft_Types__c = airCraftTypes.removeEnd('/');
                }
                
                update refAgreement;
            }
        } else{
            System.enqueueJob(new APTS_UpdateRefAgrQueueble(agreementId, agreementName, productTypeName));
        }
    }   
    
}