public class APTS_ProposalDeletionScheduler implements Schedulable {
    public void execute(SchedulableContext sc) {
        APTS_ProposalDeletionBatch prDelBatch = new APTS_ProposalDeletionBatch(); 
        Database.executebatch(prDelBatch);
    }
}