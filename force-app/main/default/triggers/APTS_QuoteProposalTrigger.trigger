/************************************************************************************************************************
@ModifiedBy: Avinash Bamane
@ModifiedDate: 18/02/2021
@ChangeDescription: GCM-10120 Added before delete and after delete trigger events.
*************************************************************************************************************************/
trigger APTS_QuoteProposalTrigger on Apttus_Proposal__Proposal__c (before insert, before update, before delete, after delete, after update) {
	system.debug('>>>>>>>>>>Before dispatching to trigger handler ');
	TriggerHandlerDispatcher.execute(Apttus_Proposal__Proposal__c.getSObjectType());
}