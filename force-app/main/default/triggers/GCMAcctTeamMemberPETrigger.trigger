trigger GCMAcctTeamMemberPETrigger on AccountTeamMemberEvent__e (after insert) {
    TriggerHandlerDispatcher.execute(AccountTeamMemberEvent__e.getSObjectType());
}