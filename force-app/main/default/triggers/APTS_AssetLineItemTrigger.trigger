trigger APTS_AssetLineItemTrigger on Apttus_Config2__AssetLineItem__c (after update) {
	system.debug('>>>>>>>>>>Before dispatching to trigger handler ');
	TriggerHandlerDispatcher.execute(Apttus_Config2__AssetLineItem__c.getSObjectType());
}